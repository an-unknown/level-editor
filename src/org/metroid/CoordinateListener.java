package org.metroid;

public interface CoordinateListener {
	public void changed(int x, int y);
}
