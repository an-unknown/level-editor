package org.metroid;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.metroid.pak.Pak;
import org.metroid.project.Project;

public class ProjectTool {
	public static void main(String[] args) throws IOException {
		if(args.length != 2) {
			System.out.printf("usage: %s in.xml out.pak\n", ProjectTool.class.getSimpleName());
			System.exit(1);
		}
		String infile = args[0];
		String outfile = args[1];
		Project prj = Project.read(new File(infile));
		Pak pak = prj.getPak();
		pak.write(new FileOutputStream(outfile));
	}
}
