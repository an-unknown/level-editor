package org.metroid.pak;

import java.io.IOException;
import java.io.OutputStream;

public class BEOutputStream extends WordOutputStream {
	public BEOutputStream(OutputStream parent) {
		super(parent);
	}

	@Override
	public void write8bit(byte value) throws IOException {
		write(value);
	}

	@Override
	public void write16bit(short value) throws IOException {
		write(Endianess.set16bit_BE(value, new byte[2]));
	}

	@Override
	public void write32bit(int value) throws IOException {
		write(Endianess.set32bit_BE(value, new byte[4]));
	}

	@Override
	public void write32bit(float value) throws IOException {
		write(Endianess.set32bit_BE(value, new byte[4]));
	}

	@Override
	public void write64bit(long value) throws IOException {
		write(Endianess.set64bit_BE(value, new byte[8]));
	}

	@Override
	public void write64bit(double value) throws IOException {
		write(Endianess.set64bit_BE(value, new byte[8]));
	}
}
