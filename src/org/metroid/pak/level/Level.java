package org.metroid.pak.level;

import java.awt.Rectangle;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.metroid.pak.Resource;
import org.metroid.pak.WordInputStream;
import org.metroid.pak.WordOutputStream;
import org.metroid.xml.dom.Element;

public class Level extends Resource {
	public static final int FOURCC = 0x4D4C564C;

	private int[] roomIDs;
	private List<RoomInfo> rooms;

	private int spawnX;
	private int spawnY;

	public Level() {
		super(FOURCC);
		rooms = new ArrayList<RoomInfo>();
	}

	public int addRoom(Room room, int x, int y) {
		int id = rooms.size();
		rooms.add(new RoomInfo(room, x, y));
		return id;
	}

	public Room getRoom(int i) {
		return rooms.get(i).getRoom();
	}

	public RoomInfo getRoomInfo(int i) {
		return rooms.get(i);
	}

	public int getRoomX(int i) {
		return rooms.get(i).getX();
	}

	public int getRoomY(int i) {
		return rooms.get(i).getY();
	}

	public void setRoomX(int i, int x) {
		rooms.get(i).setX(x);
	}

	public void setRoomY(int i, int y) {
		rooms.get(i).setY(y);
	}

	public int rooms() {
		return rooms.size();
	}

	public void removeRoom(int i) {
		rooms.remove(i);
	}

	public void removeRoom(RoomInfo room) {
		Iterator<RoomInfo> i = rooms.iterator();
		while(i.hasNext()) {
			RoomInfo info = i.next();
			if(info == room) {
				i.remove();
				return;
			}
		}
	}

	public RoomInfo[] getRooms() {
		return rooms.toArray(new RoomInfo[rooms.size()]);
	}

	public Rectangle getArea() {
		int minX = rooms.get(0).getX();
		int minY = rooms.get(0).getY();
		int maxX = minX;
		int maxY = minY;
		for(RoomInfo room : rooms) {
			if(room.getX() < minX)
				minX = room.getX();
			if(room.getY() < minY)
				minY = room.getY();
			int x = room.getX() + room.getWidth();
			int y = room.getY() + room.getHeight();
			if(x > maxX)
				maxX = x;
			if(y > maxY)
				maxY = y;
		}
		return new Rectangle(minX, minY, maxX - minX, maxY - minY);
	}

	public int getSpawnX() {
		return spawnX;
	}

	public int getSpawnY() {
		return spawnY;
	}

	public void setSpawnX(int x) {
		spawnX = x;
	}

	public void setSpawnY(int y) {
		spawnY = y;
	}

	public void setSpawn(int x, int y) {
		spawnX = x;
		spawnY = y;
	}

	@Override
	public void delayedLoad() {
		if(roomIDs == null)
			return;
		for(int i = 0; i < roomIDs.length; i++)
			rooms.get(i).setRoom(getResource(roomIDs[i], Room.FOURCC));
		roomIDs = null;
	}

	@Override
	public void read(WordInputStream in) throws IOException {
		spawnX = in.read32bit();
		spawnY = in.read32bit();
		roomIDs = new int[in.read32bit()];
		rooms = new ArrayList<RoomInfo>(roomIDs.length);
		for(int i = 0; i < roomIDs.length; i++) {
			roomIDs[i] = in.read32bit();
			int x = in.read32bit();
			int y = in.read32bit();
			in.read32bit(); // skip width
			in.read32bit(); // skip height
			rooms.add(new RoomInfo(x, y));
		}
		Rectangle area = getArea();
		for(RoomInfo info : rooms)
			info.setY(getPointY(area, info.getY()));
	}

	public int getX(int x) {
		return x;
	}

	public int getY(int y) {
		return getPointY(getArea(), y);
	}

	private int getPointY(Rectangle area, int y) {
		return area.height - (y - area.y) + area.y;
	}

	private int getRoomY(Rectangle area, RoomInfo room) {
		int base = room.getY() + room.getHeight();
		return getPointY(area, base);
	}

	@Override
	public void write(WordOutputStream out) throws IOException {
		Rectangle area = getArea();
		out.write32bit(spawnX);
		out.write32bit(getPointY(area, spawnY));
		out.write32bit(rooms.size());
		for(RoomInfo room : rooms) {
			out.write32bit(room.getRoom().getID());
			out.write32bit(room.getX());
			out.write32bit(getRoomY(area, room));
			out.write32bit(room.getWidth());
			out.write32bit(room.getHeight());
		}
	}

	@Override
	public Resource[] getDependencies() {
		Room[] deps = new Room[rooms.size()];
		int i = 0;
		for(RoomInfo room : rooms)
			deps[i++] = room.getRoom();
		return deps;
	}

	@Override
	public Element write() {
		Element root = new Element("level");
		Element spawnPoint = new Element("spawn-point");
		spawnPoint.addAttribute("x", Integer.toString(spawnX));
		spawnPoint.addAttribute("y", Integer.toString(spawnY));
		root.addChild(spawnPoint);
		for(RoomInfo info : getRooms()) {
			Room room = info.getRoom();
			Element r = room.write();
			r.addAttribute("pos-x", Integer.toString(info.getX()));
			r.addAttribute("pos-y", Integer.toString(info.getY()));
			root.addChild(r);
		}
		return root;
	}

	@Override
	public void read(Element root) throws IOException {
		if(!root.name.equals("level")) {
			throw new IllegalArgumentException("not a level");
		}
		Element[] spawnPoints = root.getElementsByTagName("spawn-point");
		if(spawnPoints.length > 0) {
			Element spawnPoint = spawnPoints[0];
			spawnX = Integer.parseInt(spawnPoint.getAttribute("x"));
			spawnY = Integer.parseInt(spawnPoint.getAttribute("y"));
		}
		for(Element info : root.getElementsByTagName("room")) {
			Room room = new Room();
			room.setPak(getPak());
			room.read(info);
			int x = Integer.parseInt(info.getAttribute("pos-x"));
			int y = Integer.parseInt(info.getAttribute("pos-y"));
			addRoom(room, x, y);
		}
	}
}
