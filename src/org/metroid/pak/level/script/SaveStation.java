package org.metroid.pak.level.script;

import java.io.IOException;

import org.metroid.pak.WordInputStream;
import org.metroid.pak.level.script.object.Instance;
import org.metroid.pak.level.script.object.Position;

public class SaveStation extends Instance {
	public static final int FOURCC = 0x53415645; // SAVE

	private int x;
	private int y;

	public SaveStation() {
		super(FOURCC);
		x = 0;
		y = 0;
		set();
	}

	public SaveStation(WordInputStream in) throws IOException {
		super(in);
		Position pos = getProperty(Position.FOURCC);
		x = pos.getX();
		y = pos.getY();
	}

	private void set() {
		Position pos = new Position(x, y);
		setProperty(pos);
	}

	public void setX(int x) {
		this.x = x;
		set();
	}

	public void setY(int y) {
		this.y = y;
		set();
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}
}
