package org.metroid.pak.level.script;

import java.io.IOException;

import org.metroid.pak.Resource;
import org.metroid.pak.WordInputStream;
import org.metroid.pak.WordOutputStream;
import org.metroid.xml.dom.Element;

public class Script extends Resource {
	public static final int FOURCC = 0x464C4F57;

	public Script() {
		super(FOURCC);
	}

	@Override
	public void read(WordInputStream in) throws IOException {
	}

	@Override
	public void write(WordOutputStream out) throws IOException {
	}

	@Override
	public Element write() {
		return null;
	}

	@Override
	public void read(Element root) throws IOException {
	}
}
