package org.metroid.pak.level.script;

import org.metroid.pak.Utils;

public class State {
	public static final int NonZero = Utils.fourCC("!ZER");
	public static final int Acquired = Utils.fourCC("ACQU");
	public static final int Active = Utils.fourCC("ACTV");
	public static final int Approach = Utils.fourCC("APRC");
	public static final int Arrived = Utils.fourCC("ARRV");
	public static final int Attack = Utils.fourCC("ATTK");
	public static final int Closed = Utils.fourCC("CLOS");
	public static final int Connect = Utils.fourCC("CONN");
	public static final int Dead = Utils.fourCC("DEAD");
	public static final int Entered = Utils.fourCC("ENTR");
	public static final int Exited = Utils.fourCC("EXIT");
	public static final int Freeze = Utils.fourCC("FREZ");
	public static final int Inactive = Utils.fourCC("ICTV");
	public static final int Inside = Utils.fourCC("INSD");
	public static final int Locked = Utils.fourCC("LOCK");
	public static final int Modify = Utils.fourCC("MDFY");
	public static final int Open = Utils.fourCC("OPEN");
	public static final int Zero = Utils.fourCC("ZERO");
}
