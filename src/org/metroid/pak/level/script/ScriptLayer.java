package org.metroid.pak.level.script;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.metroid.pak.Resource;
import org.metroid.pak.WordInputStream;
import org.metroid.pak.WordOutputStream;
import org.metroid.pak.level.script.object.Instance;
import org.metroid.xml.dom.Element;

public class ScriptLayer extends Resource {
	public static final int FOURCC = 0x53434C59;

	private List<Instance> instances;

	public ScriptLayer() {
		super(FOURCC);
		instances = new ArrayList<Instance>();
	}

	public ScriptLayer(WordInputStream in) throws IOException {
		this();
		read(in);
	}

	@Override
	public void read(WordInputStream in) throws IOException {
		instances.clear();
		int magic = in.read32bit();
		if(magic != FOURCC)
			throw new IOException("Invalid magic");
		int instanceCount = in.read32bit();
		for(int i = 0; i < instanceCount; i++) {
			instances.add(Instance.parse(in));
		}
	}

	@Override
	public void write(WordOutputStream out) throws IOException {
		out.write32bit(FOURCC);
		out.write32bit(instances.size());
		for(Instance instance : instances)
			instance.write(out);
	}

	@Override
	public void read(Element root) throws IOException {
		// TODO Auto-generated method stub
	}

	@Override
	public Element write() {
		Element root = new Element("script-layer");
		return root;
	}
}
