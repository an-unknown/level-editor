package org.metroid.pak.level.script.object;

import java.io.IOException;

import org.metroid.pak.WordInputStream;
import org.metroid.pak.WordOutputStream;

public class Property {
	protected int id;
	private byte[] data;

	public Property(int id) {
		this.id = id;
	}

	public Property(int id, byte[] data) {
		this.id = id;
		this.data = data;
	}

	public Property(WordInputStream in) throws IOException {
		read(in);
	}

	public int getId() {
		return id;
	}

	public byte[] getData() {
		return data;
	}

	public int size() {
		return getData().length;
	}

	public void setData(byte[] data) {
		this.data = data;
	}

	// comparison is only based on ID
	@Override
	public int hashCode() {
		return id;
	}

	@Override
	public boolean equals(Object o) {
		if(!(o instanceof Property))
			return false;
		Property p = (Property) o;
		return p.id == id;
	}

	public void read(WordInputStream in) throws IOException {
		id = in.read32bit();
		data = new byte[in.read16bit()];
		in.read(data);
	}

	public void write(WordOutputStream out) throws IOException {
		out.write32bit(getId());
		out.write16bit((short) size());
		out.write(getData());
	}

	public static Property parse(WordInputStream in) throws IOException {
		Property p = new Property(in);
		switch(p.getId()) {
		case Position.FOURCC:
			return new Position(p.data);
		default:
			return p;
		}
	}
}
