package org.metroid.pak.level.script.object;

import org.metroid.pak.Utils;

public class Door extends Instance {
	public static final int FOURCC = Utils.fourCC("DOOR");

	public Door() {
		super(FOURCC);
	}
}
