package org.metroid.pak.level.script.object;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.metroid.pak.BEInputStream;
import org.metroid.pak.Endianess;
import org.metroid.pak.WordInputStream;
import org.metroid.pak.WordOutputStream;
import org.metroid.pak.level.script.SaveStation;

public class Instance {
	private int type;
	private int id;
	private PropertyStruct properties;
	private Set<Connection> connections;

	public Instance(int type) {
		this.type = type;
		properties = new PropertyStruct(0xFFFFFFFF);
		connections = new HashSet<Connection>();
	}

	public Instance(WordInputStream in) throws IOException {
		read(in);
	}

	public int getType() {
		return type;
	}

	@SuppressWarnings("unchecked")
	public <T extends Property> T getProperty(int id) {
		return (T) properties.get(id);
	}

	public void setProperty(Property p) {
		properties.set(p);
	}

	public int size() {
		return connections.size() * Connection.SIZE + properties.size() + 6;
	}

	public void read(WordInputStream in) throws IOException {
		type = in.read32bit();
		@SuppressWarnings("unused")
		int size = in.read16bit();
		id = in.read32bit();
		int numConnections = in.read16bit();
		for(int i = 0; i < numConnections; i++)
			connections.add(new Connection(in));
		properties.read(in);
	}

	public void write(WordOutputStream out) throws IOException {
		out.write32bit(type);
		out.write16bit((short) size());
		out.write32bit(id);
		out.write16bit((short) connections.size());
		for(Connection c : connections)
			c.write(out);
		properties.write(out);
	}

	public static Instance parse(WordInputStream in) throws IOException {
		int type = in.read32bit();
		int size = in.read16bit();
		byte[] data = new byte[size];
		in.read(data);
		byte[] raw = new byte[size + 6];
		System.arraycopy(data, 0, raw, 6, data.length);
		Endianess.set32bit_BE(type, raw);
		Endianess.set16bit_BE(size, raw, 4);

		try(WordInputStream src = new BEInputStream(new ByteArrayInputStream(raw))) {
			switch(type) {
			case SaveStation.FOURCC:
				return new SaveStation(src);
			default:
				return new Instance(src);
			}
		}
	}
}
