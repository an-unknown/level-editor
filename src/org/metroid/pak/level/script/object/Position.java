package org.metroid.pak.level.script.object;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.metroid.pak.BEInputStream;
import org.metroid.pak.BEOutputStream;
import org.metroid.pak.WordInputStream;
import org.metroid.pak.WordOutputStream;

public class Position extends Property {
	public static final int FOURCC = 0x504F5332; // POS2

	private int x;
	private int y;

	public Position(int x, int y) {
		super(FOURCC);
		this.x = x;
		this.y = y;
	}

	public Position(byte[] data) throws IOException {
		super(FOURCC);
		try(WordInputStream in = new BEInputStream(new ByteArrayInputStream(data))) {
			x = in.read32bit();
			y = in.read32bit();
		}
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public void setX(int x) {
		this.x = x;
	}

	public void setY(int y) {
		this.y = y;
	}

	@Override
	public byte[] getData() {
		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		try {
			WordOutputStream out = new BEOutputStream(bytes);
			out.write32bit(x);
			out.write32bit(y);
			out.flush();
			out.close();
		} catch(IOException e) {
			e.printStackTrace();
		}
		try {
			bytes.close();
		} catch(IOException e) {
			e.printStackTrace();
		}
		return bytes.toByteArray();
	}
}
