package org.metroid.pak.level.script.object;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.metroid.pak.WordInputStream;
import org.metroid.pak.WordOutputStream;

public class PropertyStruct extends Property {
	private Set<Property> subProperties;

	public PropertyStruct(int id) {
		super(id);
		subProperties = new HashSet<Property>();
	}

	public Property get(int id) {
		for(Property p : subProperties)
			if(p.getId() == id)
				return p;
		return null;
	}

	public void set(Property p) {
		subProperties.add(p);
	}

	@Override
	public int size() {
		int size = 2;
		for(Property p : subProperties)
			size += p.size();
		return size;
	}

	@Override
	public void read(WordInputStream in) throws IOException {
		id = in.read32bit();
		@SuppressWarnings("unused")
		int size = in.read16bit();
		int numProperties = in.read16bit();
		for(int i = 0; i < numProperties; i++)
			subProperties.add(Property.parse(in));
	}

	@Override
	public void write(WordOutputStream out) throws IOException {
		out.write32bit(getId());
		out.write16bit((short) size());
		for(Property p : subProperties)
			p.write(out);
	}
}
