package org.metroid.pak.level.script.object;

import java.io.IOException;

import org.metroid.pak.WordInputStream;
import org.metroid.pak.WordOutputStream;

public class Connection {
	public static final int SIZE = 12;

	private int state;
	private int message;
	private int target;

	public Connection(int state, int message, int target) {
		this.state = state;
		this.message = message;
		this.target = target;
	}

	public Connection(WordInputStream in) throws IOException {
		read(in);
	}

	public void write(WordOutputStream out) throws IOException {
		out.write32bit(state);
		out.write32bit(message);
		out.write32bit(target);
	}

	public void read(WordInputStream in) throws IOException {
		state = in.read32bit();
		message = in.read32bit();
		target = in.read32bit();
	}
}
