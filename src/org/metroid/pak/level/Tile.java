package org.metroid.pak.level;

import java.awt.image.BufferedImage;
import java.io.Serializable;

import org.metroid.pak.txtr.Texture;

public class Tile implements Serializable {
	private static final long serialVersionUID = 1L;

	private static short globalId = 0;

	private short id;
	private Texture texture;
	public short s;
	public short t;

	private BufferedImage cache;

	public Tile(short s, short t, Texture texture) {
		this.id = globalId++;
		this.s = s;
		this.t = t;
		this.texture = texture;
		this.cache = null;
	}

	public short getS() {
		return s;
	}

	public short getT() {
		return t;
	}

	public Texture getTexture() {
		return texture;
	}

	public void setTexture(Texture texture) {
		this.texture = texture;
	}

	public void setId(short id) {
		this.id = id;
	}

	public short getId() {
		return id;
	}

	public BufferedImage getCache() {
		return cache;
	}

	public void setCache(BufferedImage cache) {
		this.cache = cache;
	}

	@Override
	public boolean equals(Object o) {
		if(o == null)
			return false;
		if(!(o instanceof Tile))
			return false;
		Tile t = (Tile) o;
		return t.s == s && t.t == this.t && t.texture == texture;
	}

	@Override
	public int hashCode() {
		return (s + (t << 11) * 3) ^ texture.hashCode();
	}

	@Override
	public String toString() {
		return "Tile[id=" + id + ";s=" + s + ";t=" + t + ";texture=" + texture + "]";
	}
}
