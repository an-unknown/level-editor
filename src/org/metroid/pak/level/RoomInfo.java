package org.metroid.pak.level;

public class RoomInfo {
	private Room room;
	private int x;
	private int y;

	public RoomInfo(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public RoomInfo(Room room, int x, int y) {
		this.room = room;
		this.x = x;
		this.y = y;
	}

	public Room getRoom() {
		return room;
	}

	public void setRoom(Room room) {
		this.room = room;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public void setX(int x) {
		this.x = x;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getWidth() {
		return room.getWidth();
	}

	public int getHeight() {
		return room.getHeight();
	}
}
