package org.metroid.pak.level;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import org.metroid.logging.Levels;
import org.metroid.logging.Trace;
import org.metroid.pak.Resource;
import org.metroid.pak.WordInputStream;
import org.metroid.pak.WordOutputStream;
import org.metroid.pak.txtr.Texture;
import org.metroid.project.Project;
import org.metroid.xml.dom.Element;
import org.metroid.xml.dom.Text;

public class Tileset extends Resource {
	private static final Logger log = Trace.create(Tileset.class);

	public static final int FOURCC = 0x54494C45;

	private Texture[] textures;
	private List<Tile> tiles;

	private int tileWidth;
	private int tileHeight;

	private Tile emptyTile;

	public Tileset(Project project) {
		super(FOURCC);
		tiles = new ArrayList<Tile>();
		tileWidth = project.getTileWidth();
		tileHeight = project.getTileHeight();
		emptyTile = project.getEmptyTile();
	}

	public Tileset(int width, int height, Tile emptyTile) {
		super(FOURCC);
		tiles = new ArrayList<Tile>();
		tileWidth = width;
		tileHeight = height;
		this.emptyTile = emptyTile;
	}

	public Tile getEmptyTile() {
		return emptyTile;
	}

	public void setEmptyTile(Tile tile) {
		emptyTile = tile;
	}

	public void setTextures() {
		Set<Texture> textures = new HashSet<Texture>();
		for(Tile tile : tiles)
			textures.add(tile.getTexture());
		this.textures = textures.toArray(new Texture[textures.size()]);
	}

	public Texture[] getTextures() {
		setTextures();
		return textures;
	}

	public void setTiles(Tile[] tiles) {
		this.tiles.clear();
		this.tiles.addAll(Arrays.asList(tiles));
		renumber();
	}

	public void addTile(Tile tile) {
		tiles.add(tile);
	}

	public void removeTile(Tile tile) {
		tiles.remove(tile);
	}

	public Tile[] getTiles() {
		return tiles.toArray(new Tile[tiles.size()]);
	}

	public Tile getTile(int id) {
		return tiles.get(id);
	}

	public Tile createTile(short s, short t, Texture texture) {
		Tile tile = new Tile(s, t, texture);
		if(tiles.contains(tile))
			for(Tile ti : tiles)
				if(ti.equals(tile))
					return ti;
		addTile(tile);
		return tile;
	}

	public int getTileWidth() {
		return tileWidth;
	}

	public int getTileHeight() {
		return tileHeight;
	}

	public BufferedImage get(Tile tile) {
		BufferedImage img = tile.getCache();
		if(img == null) {
			img = tile.getTexture().getTexture().getSubimage(tile.s * tileWidth, tile.t * tileHeight,
					tileWidth, tileHeight);
			tile.setCache(img);
		}
		return img;
	}

	public void renumber() {
		short id = 0;
		for(Tile tile : tiles)
			tile.setId(id++);
		setTextures();
	}

	@Override
	public void read(WordInputStream in) throws IOException {
		tileWidth = in.read32bit();
		tileHeight = in.read32bit();
		textures = new Texture[in.read32bit()];
		for(int i = 0; i < textures.length; i++)
			textures[i] = getResource(in.read32bit(), Texture.FOURCC);
		tiles.clear();
		int count = in.read32bit();
		for(int i = 0; i < count; i++) {
			int txtr = in.read32bit();
			short s = in.read16bit();
			short t = in.read16bit();
			Tile tile = new Tile(s, t, getResource(txtr, Texture.FOURCC));
			tile.setId((short) i);
			tiles.add(tile);
		}
	}

	@Override
	public void write(WordOutputStream out) throws IOException {
		renumber();
		out.write32bit(tileWidth);
		out.write32bit(tileHeight);
		out.write32bit(textures.length);
		for(Texture texture : textures)
			out.write32bit(texture.getID());
		out.write32bit(tiles.size());
		for(Tile tile : tiles) {
			out.write32bit(tile.getTexture().getID());
			out.write16bit(tile.getS());
			out.write16bit(tile.getT());
		}
	}

	@Override
	public Resource[] getDependencies() {
		Set<Texture> txtr = new HashSet<Texture>();
		for(Tile tile : tiles)
			txtr.add(tile.getTexture());
		return txtr.toArray(new Texture[txtr.size()]);
	}

	@Override
	public Element write() {
		log.log(Levels.DEBUG, "Writing tileset");
		Element root = new Element("tileset");
		root.addAttribute("tileWidth", Integer.toString(getTileWidth()));
		root.addAttribute("tileHeight", Integer.toString(getTileHeight()));
		Element emptyTile = new Element("empty-tile");
		emptyTile.addAttribute("tex", getEmptyTile().getTexture().getName());
		emptyTile.addAttribute("s", Short.toString(getEmptyTile().getS()));
		emptyTile.addAttribute("t", Short.toString(getEmptyTile().getT()));
		root.addChild(emptyTile);
		for(Tile tile : getTiles()) {
			Element t = new Element("tile");
			t.addAttribute("id", Short.toString(tile.getId()));
			t.addAttribute("tex", tile.getTexture().getName());
			t.addAttribute("s", Short.toString(tile.getS()));
			t.addAttribute("t", Short.toString(tile.getT()));
			root.addChild(t);
		}
		return root;
	}

	@Override
	public void read(Element in) {
		if(!in.name.equals("tileset")) {
			throw new IllegalArgumentException("not a tileset");
		}
		log.log(Levels.DEBUG, "Reading tileset");
		tiles = new ArrayList<>();
		tileWidth = Integer.parseInt(in.getAttribute("tileWidth"));
		tileHeight = Integer.parseInt(in.getAttribute("tileHeight"));
		emptyTile = null;
		for(Element e : in.getChildren()) {
			if(e.name.equals("tile")) {
				short s = Short.parseShort(e.getAttribute("s"));
				short t = Short.parseShort(e.getAttribute("t"));
				short id = Short.parseShort(e.getAttribute("id"));
				if(id != tiles.size()) {
					throw new RuntimeException("tile ID is wrong! (" + id + ")");
				}
				Texture tex = getPak().getProject().getTexture(e.getAttribute("tex"));
				Tile tile = new Tile(s, t, tex);
				tiles.add(tile);
			} else if(e.name.equals("empty-tile")) {
				short s = Short.parseShort(e.getAttribute("s"));
				short t = Short.parseShort(e.getAttribute("t"));
				Texture tex = getPak().getProject().getTexture(e.getAttribute("tex"));
				emptyTile = new Tile(s, t, tex);
			} else if(e instanceof Text && ((Text) e).value.trim().length() > 0) {
				throw new IllegalArgumentException("unknown element: " + e.name);
			}
		}
		setTextures();
	}
}
