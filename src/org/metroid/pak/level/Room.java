package org.metroid.pak.level;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import org.metroid.bts.TileBts;
import org.metroid.logging.Levels;
import org.metroid.logging.Trace;
import org.metroid.pak.Resource;
import org.metroid.pak.Utils;
import org.metroid.pak.WordInputStream;
import org.metroid.pak.WordOutputStream;
import org.metroid.pak.level.script.ScriptLayer;
import org.metroid.pak.txtr.ComparableTexture;
import org.metroid.util.Base64;
import org.metroid.util.Endianess;
import org.metroid.xml.dom.Element;

public class Room extends Resource {
	public static final Logger log = Trace.create(Room.class);

	public static final boolean MERGE_TEXTURES = false;

	public static final int FOURCC = 0x524F4F4D; // ROOM

	private short width;
	private short height;
	private int layerWidth;
	private int layerHeight;
	private Tileset tileset;
	private TileBts[] bts;
	private Tile[] hidden;
	private Tile[] fg1;
	private Tile[] fg2;
	private Tile[] bg1;
	private Tile[] bg2;
	private List<ScriptLayer> layers;

	public Room() {
		super(FOURCC);
	}

	public Room(Tileset tileset, int width, int height) {
		this();
		this.tileset = tileset;
		this.width = (short) width;
		this.height = (short) height;
		init();
	}

	private void init() {
		this.layers = new ArrayList<>();
		this.layerWidth = Utils.round2(width);
		this.layerHeight = Utils.round2(height);
		bts = new TileBts[width * height];
		hidden = new Tile[layerWidth * layerHeight];
		fg1 = new Tile[layerWidth * layerHeight];
		fg2 = new Tile[layerWidth * layerHeight];
		bg1 = new Tile[layerWidth * layerHeight];
		bg2 = new Tile[layerWidth * layerHeight];
		Tile emptyTile = tileset.getEmptyTile();
		for(int i = 0; i < bts.length; i++)
			bts[i] = new TileBts();
		for(int i = 0; i < hidden.length; i++)
			hidden[i] = emptyTile;
		for(int i = 0; i < fg1.length; i++)
			fg1[i] = emptyTile;
		for(int i = 0; i < fg2.length; i++)
			fg2[i] = emptyTile;
		for(int i = 0; i < bg1.length; i++)
			bg1[i] = emptyTile;
		for(int i = 0; i < bg2.length; i++)
			bg2[i] = emptyTile;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public Tileset getTileset() {
		return tileset;
	}

	public TileBts getBTS(int x, int y) {
		return bts[y * width + x];
	}

	public Tile getHidden(int x, int y) {
		return hidden[y * layerWidth + x];
	}

	public Tile getFG1(int x, int y) {
		return fg1[y * layerWidth + x];
	}

	public Tile getFG2(int x, int y) {
		return fg2[y * layerWidth + x];
	}

	public Tile getBG1(int x, int y) {
		return bg1[y * layerWidth + x];
	}

	public Tile getBG2(int x, int y) {
		return bg2[y * layerWidth + x];
	}

	public void setBTS(int x, int y, int bts) {
		this.bts[y * width + x] = new TileBts(bts);
	}

	public void setBTS(int x, int y, TileBts bts) {
		this.bts[y * width + x] = bts;
	}

	public void setHidden(int x, int y, Tile tile) {
		hidden[y * layerWidth + x] = tile;
	}

	public void setFG1(int x, int y, Tile tile) {
		fg1[y * layerWidth + x] = tile;
	}

	public void setFG2(int x, int y, Tile tile) {
		fg2[y * layerWidth + x] = tile;
	}

	public void setBG1(int x, int y, Tile tile) {
		bg1[y * layerWidth + x] = tile;
	}

	public void setBG2(int x, int y, Tile tile) {
		bg2[y * layerWidth + x] = tile;
	}

	public int getLayerCount() {
		return layers.size();
	}

	public ScriptLayer getLayer(int layer) {
		return layers.get(layer);
	}

	public int addLayer(ScriptLayer layer) {
		int id = getLayerCount();
		layers.add(layer);
		return id;
	}

	public void removeLayer(int layer) {
		layers.remove(layer);
	}

	public void removeLayer(ScriptLayer layer) {
		layers.remove(layer);
	}

	public void resize(int width, int height) {
		if(this.width == width && this.height == height)
			return;
		int layerWidth = Utils.round2(width);
		int layerHeight = Utils.round2(height);
		TileBts[] bts = new TileBts[width * height];
		int mw = this.width < width ? this.width : width;
		int mh = this.height < height ? this.height : height;
		for(int i = 0; i < bts.length; i++)
			bts[i] = new TileBts();
		for(int y = 0; y < mh; y++)
			for(int x = 0; x < mw; x++)
				bts[y * width + x] = getBTS(x, y);
		this.bts = bts;

		if(this.layerWidth == layerWidth && this.layerHeight == layerHeight) {
			this.width = (short) width;
			this.height = (short) height;
			return;
		}

		Tile[] hidden = new Tile[layerWidth * layerHeight];
		Tile[] fg1 = new Tile[layerWidth * layerHeight];
		Tile[] fg2 = new Tile[layerWidth * layerHeight];
		Tile[] bg1 = new Tile[layerWidth * layerHeight];
		Tile[] bg2 = new Tile[layerWidth * layerHeight];
		Tile emptyTile = tileset.getEmptyTile();
		for(int i = 0; i < hidden.length; i++)
			hidden[i] = emptyTile;
		for(int i = 0; i < fg1.length; i++)
			fg1[i] = emptyTile;
		for(int i = 0; i < fg2.length; i++)
			fg2[i] = emptyTile;
		for(int i = 0; i < bg1.length; i++)
			bg1[i] = emptyTile;
		for(int i = 0; i < bg2.length; i++)
			bg2[i] = emptyTile;
		for(int y = 0; y < mh; y++) {
			for(int x = 0; x < mw; x++) {
				hidden[y * layerWidth + x] = getHidden(x, y);
				fg1[y * layerWidth + x] = getFG1(x, y);
				fg2[y * layerWidth + x] = getFG2(x, y);
				bg1[y * layerWidth + x] = getBG1(x, y);
				bg2[y * layerWidth + x] = getBG2(x, y);
			}
		}
		this.width = (short) width;
		this.height = (short) height;
		this.layerWidth = layerWidth;
		this.layerHeight = layerHeight;
		this.hidden = hidden;
		this.fg1 = fg1;
		this.fg2 = fg2;
		this.bg1 = bg1;
		this.bg2 = bg2;
	}

	@Override
	public void read(WordInputStream in) throws IOException {
		width = in.read16bit();
		height = in.read16bit();
		tileset = getResource(in.read32bit(), Tileset.FOURCC);
		layers = new ArrayList<ScriptLayer>();
		int numLayers = in.read32bit();
		for(int i = 0; i < numLayers; i++)
			layers.add(new ScriptLayer(in));
		layerWidth = Utils.round2(width);
		layerHeight = Utils.round2(height);
		bts = new TileBts[width * height];
		hidden = new Tile[layerWidth * layerHeight];
		fg1 = new Tile[layerWidth * layerHeight];
		fg2 = new Tile[layerWidth * layerHeight];
		bg1 = new Tile[layerWidth * layerHeight];
		bg2 = new Tile[layerWidth * layerHeight];
		for(int i = 0; i < hidden.length; i++)
			hidden[i] = tileset.getTile(in.read16bit());
		for(int i = 0; i < fg1.length; i++)
			fg1[i] = tileset.getTile(in.read16bit());
		for(int i = 0; i < fg2.length; i++)
			fg2[i] = tileset.getTile(in.read16bit());
		for(int i = 0; i < bg1.length; i++)
			bg1[i] = tileset.getTile(in.read16bit());
		for(int i = 0; i < bg2.length; i++)
			bg2[i] = tileset.getTile(in.read16bit());
		for(int i = 0; i < bts.length; i++)
			bts[i] = new TileBts(in.read32bit(), tileset);
	}

	@Override
	public void write(WordOutputStream out) throws IOException {
		out.write16bit(width);
		out.write16bit(height);
		out.write32bit(tileset.getID());
		out.write32bit(layers.size());
		for(ScriptLayer layer : layers)
			layer.write(out);
		for(int i = 0; i < hidden.length; i++)
			out.write16bit(hidden[i].getId());
		for(int i = 0; i < fg1.length; i++)
			out.write16bit(fg1[i].getId());
		for(int i = 0; i < fg2.length; i++)
			out.write16bit(fg2[i].getId());
		for(int i = 0; i < bg1.length; i++)
			out.write16bit(bg1[i].getId());
		for(int i = 0; i < bg2.length; i++)
			out.write16bit(bg2[i].getId());
		for(int i = 0; i < bts.length; i++)
			out.write32bit(bts[i].getData());
	}

	@Override
	public void prepareForWrite() {
		// cleanup tileset
		Set<Tile> tiles = new HashSet<Tile>();
		Set<ComparableTexture> textures = new HashSet<ComparableTexture>();
		for(int i = 0; i < hidden.length; i++)
			tiles.add(hidden[i]);
		for(int i = 0; i < fg2.length; i++)
			tiles.add(fg1[i]);
		for(int i = 0; i < fg2.length; i++)
			tiles.add(fg2[i]);
		for(int i = 0; i < bg1.length; i++)
			tiles.add(bg1[i]);
		for(int i = 0; i < bg2.length; i++)
			tiles.add(bg2[i]);
		for(int i = 0; i < bts.length; i++)
			if(bts[i].hasTile())
				tiles.add(bts[i].getTile());
		Tile[] t = tiles.toArray(new Tile[tiles.size()]);
		// reduce number of textures: merge duplicates
		if(MERGE_TEXTURES) {
			for(Tile tile : tiles)
				textures.add(new ComparableTexture(tile.getTexture()));
			for(Tile tile : t) {
				for(ComparableTexture tex : textures) {
					if(tex.getTexture().equals(tile.getTexture())) {
						tile.setTexture(tex.getTexture());
						break;
					}
				}
			}
			tiles.clear();
			for(Tile tile : t)
				tiles.add(tile);
		}
		// remove redundant tiles
		t = tiles.toArray(new Tile[tiles.size()]);
		for(int i = 0; i < bts.length; i++) {
			if(!bts[i].hasTile())
				continue;
			for(Tile tile : t) {
				if(bts[i].getTile().equals(tile)) {
					bts[i].setTile(tile);
					break;
				}
			}
		}
		for(int i = 0; i < hidden.length; i++) {
			for(Tile tile : t) {
				if(hidden[i].equals(tile)) {
					hidden[i] = tile;
					break;
				}
			}
		}
		for(int i = 0; i < fg1.length; i++) {
			for(Tile tile : t) {
				if(fg1[i].equals(tile)) {
					fg1[i] = tile;
					break;
				}
			}
		}
		for(int i = 0; i < fg2.length; i++) {
			for(Tile tile : t) {
				if(fg2[i].equals(tile)) {
					fg2[i] = tile;
					break;
				}
			}
		}
		for(int i = 0; i < bg1.length; i++) {
			for(Tile tile : t) {
				if(bg1[i].equals(tile)) {
					bg1[i] = tile;
					break;
				}
			}
		}
		for(int i = 0; i < bg2.length; i++) {
			for(Tile tile : t) {
				if(bg2[i].equals(tile)) {
					bg2[i] = tile;
					break;
				}
			}
		}
		tileset.setTiles(tiles.toArray(new Tile[tiles.size()]));
		System.out.printf("%d tiles\n", tileset.getTiles().length);
	}

	private void prepareForXmlWrite() {
		// remove redundant tiles
		Set<Tile> tiles = new HashSet<>();
		for(int i = 0; i < hidden.length; i++)
			tiles.add(hidden[i]);
		for(int i = 0; i < fg1.length; i++)
			tiles.add(fg1[i]);
		for(int i = 0; i < fg2.length; i++)
			tiles.add(fg2[i]);
		for(int i = 0; i < bg1.length; i++)
			tiles.add(bg1[i]);
		for(int i = 0; i < bg2.length; i++)
			tiles.add(bg2[i]);
		for(int i = 0; i < bts.length; i++)
			if(bts[i].hasTile())
				tiles.add(bts[i].getTile());
		Tile[] t = tiles.toArray(new Tile[tiles.size()]);
		for(Tile tile : t)
			tiles.add(tile);
		t = tiles.toArray(new Tile[tiles.size()]);
		for(int i = 0; i < bts.length; i++) {
			if(!bts[i].hasTile())
				continue;
			for(Tile tile : t) {
				if(bts[i].getTile().equals(tile)) {
					bts[i].setTile(tile);
					break;
				}
			}
		}
		for(int i = 0; i < hidden.length; i++) {
			for(Tile tile : t) {
				if(hidden[i].equals(tile)) {
					hidden[i] = tile;
					break;
				}
			}
		}
		for(int i = 0; i < fg1.length; i++) {
			for(Tile tile : t) {
				if(fg1[i].equals(tile)) {
					fg1[i] = tile;
					break;
				}
			}
		}
		for(int i = 0; i < fg2.length; i++) {
			for(Tile tile : t) {
				if(fg2[i].equals(tile)) {
					fg2[i] = tile;
					break;
				}
			}
		}
		for(int i = 0; i < bg1.length; i++) {
			for(Tile tile : t) {
				if(bg1[i].equals(tile)) {
					bg1[i] = tile;
					break;
				}
			}
		}
		for(int i = 0; i < bg2.length; i++) {
			for(Tile tile : t) {
				if(bg2[i].equals(tile)) {
					bg2[i] = tile;
					break;
				}
			}
		}
		tileset.setTiles(tiles.toArray(new Tile[tiles.size()]));
		System.out.printf("%d tiles\n", tileset.getTiles().length);
	}

	@Override
	public Resource[] getDependencies() {
		List<Resource> dependencies = new ArrayList<Resource>();
		dependencies.add(tileset);
		return dependencies.toArray(new Resource[dependencies.size()]);
	}

	private interface LayerReader {
		public int get(int x, int y);
	}

	private interface LayerWriter {
		public void set(int x, int y, int id);
	}

	@Override
	public Element write() {
		log.log(Levels.DEBUG, "Writing room data");
		Element root = new Element("room");
		prepareForXmlWrite();
		log.log(Levels.DEBUG, "Room prepared for writing");
		root.addAttribute("width", Integer.toString(getWidth()));
		root.addAttribute("height", Integer.toString(getHeight()));
		root.addChild(getTileset().write());
		root.addChild(xmlLayer("hidden", getWidth(), getHeight(), (x, y) -> getHidden(x, y).getId()));
		root.addChild(xmlLayer("fg1", getWidth(), getHeight(), (x, y) -> getFG1(x, y).getId()));
		root.addChild(xmlLayer("fg2", getWidth(), getHeight(), (x, y) -> getFG2(x, y).getId()));
		root.addChild(xmlLayer("bg1", getWidth(), getHeight(), (x, y) -> getBG1(x, y).getId()));
		root.addChild(xmlLayer("bg2", getWidth(), getHeight(), (x, y) -> getBG2(x, y).getId()));
		root.addChild(xmlBtsLayer("bts", getWidth(), getHeight(), (x, y) -> getBTS(x, y).getData()));
		return root;
	}

	@Override
	public void read(Element root) throws IOException {
		if(!root.name.equals("room")) {
			throw new IllegalArgumentException("not a room");
		}
		log.log(Levels.DEBUG, "Reading room data");
		width = Short.parseShort(root.getAttribute("width"));
		height = Short.parseShort(root.getAttribute("height"));
		Element[] xmlTileset = root.getElementsByTagName("tileset");
		if(xmlTileset.length != 1) {
			throw new IllegalArgumentException("not exactly 1 tileset");
		}
		tileset = new Tileset(getProject());
		tileset.setPak(getPak());
		tileset.read(xmlTileset[0]);
		init();
		for(Element layer : root.getElementsByTagName("layer")) {
			xmlReadLayer(this, layer);
		}
	}

	private static Element xmlLayer(String name, int width, int height, LayerReader reader) {
		log.log(Levels.DEBUG, "Writing layer " + name);
		byte[] data = new byte[width * height * 2];
		for(int y = 0; y < height; y++) {
			for(int x = 0; x < width; x++) {
				int offset = (y * width + x) * 2;
				short id = (short) reader.get(x, y);
				Endianess.set16bit_BE(id, data, offset);
			}
		}
		Element layer = new Element("layer", Base64.encode(data));
		layer.addAttribute("name", name);
		return layer;
	}

	private static Element xmlBtsLayer(String name, int width, int height, LayerReader reader) {
		byte[] data = new byte[width * height * 4];
		for(int y = 0; y < height; y++) {
			for(int x = 0; x < width; x++) {
				int offset = (y * width + x) * 4;
				int id = reader.get(x, y);
				Endianess.set32bit_BE(id, data, offset);
			}
		}
		Element layer = new Element("layer", Base64.encode(data));
		layer.addAttribute("name", name);
		return layer;
	}

	private static void xmlReadLayer(Room room, Element layer) {
		if(!layer.name.equals("layer")) {
			throw new IllegalArgumentException("not a layer");
		}
		String name = layer.getAttribute("name");
		log.log(Levels.DEBUG, "Reading layer " + name);
		switch(name) {
		case "hidden":
			xmlReadLayer(room.getWidth(), room.getHeight(), layer.value,
					(x, y, id) -> room.setHidden(x, y, room.getTileset().getTile(id)));
			break;
		case "fg":
		case "fg1":
			xmlReadLayer(room.getWidth(), room.getHeight(), layer.value,
					(x, y, id) -> room.setFG1(x, y, room.getTileset().getTile(id)));
			break;
		case "fg2":
			xmlReadLayer(room.getWidth(), room.getHeight(), layer.value,
					(x, y, id) -> room.setFG2(x, y, room.getTileset().getTile(id)));
			break;
		case "bg1":
			xmlReadLayer(room.getWidth(), room.getHeight(), layer.value,
					(x, y, id) -> room.setBG1(x, y, room.getTileset().getTile(id)));
			break;
		case "bg2":
			xmlReadLayer(room.getWidth(), room.getHeight(), layer.value,
					(x, y, id) -> room.setBG2(x, y, room.getTileset().getTile(id)));
			break;
		case "bts":
			xmlReadBtsLayer(room.getWidth(), room.getHeight(), layer.value,
					(x, y, data) -> room.setBTS(x, y, new TileBts(data)));
			break;
		}
	}

	private static void xmlReadLayer(int width, int height, String data, LayerWriter writer) {
		byte[] binary = Base64.decode(data);
		for(int y = 0; y < height; y++) {
			for(int x = 0; x < width; x++) {
				int offset = (y * width + x) * 2;
				int id = Endianess.get16bit_BE(binary, offset);
				writer.set(x, y, id);
			}
		}
	}

	private static void xmlReadBtsLayer(int width, int height, String data, LayerWriter writer) {
		byte[] binary = Base64.decode(data);
		for(int y = 0; y < height; y++) {
			for(int x = 0; x < width; x++) {
				int offset = (y * width + x) * 4;
				int id = Endianess.get32bit_BE(binary, offset);
				writer.set(x, y, id);
			}
		}
	}
}
