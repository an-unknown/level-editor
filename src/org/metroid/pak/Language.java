package org.metroid.pak;

public class Language {
	public static final int ENGLISH = Utils.fourCC("ENGL");
	public static final int FRENCH = Utils.fourCC("FREN");
	public static final int GERMAN = Utils.fourCC("GERM");
	public static final int SPANISH = Utils.fourCC("SPAN");
	public static final int ITALIAN = Utils.fourCC("ITAL");
	public static final int DUTCH = Utils.fourCC("DUTC");
	public static final int JAPANESE = Utils.fourCC("JAPN");
}
