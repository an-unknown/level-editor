package org.metroid.pak;

import java.io.IOException;

import org.metroid.pak.txtr.Texture;
import org.metroid.xml.dom.Element;

public class Sprite extends Resource {
	public static final int FOURCC = 0x53505254; // SPRT

	private Texture texture;
	private int delayedTxtr = -1;

	private int count;
	private int frames;
	private int width;
	private int height;
	private int paddingL;
	private int paddingR;
	private int paddingT;
	private int paddingB;
	private int[] offsetX;
	private int[] offsetY;
	private boolean loop;
	private boolean reverse;

	public Sprite() {
		super(FOURCC);
	}

	public Sprite(Texture texture, int width, int height, int count, int frames, int[] offsetX, int[] offsetY,
			int paddingL, int paddingR, int paddingT, int paddingB, boolean loop, boolean reverse) {
		super(FOURCC);
		if(offsetX.length != count || offsetY.length != count)
			throw new IllegalArgumentException(String.format("count=%d, offsetX.length=%d, offsetY=%d",
					count, offsetX.length, offsetY.length));
		this.texture = texture;
		this.width = width;
		this.height = height;
		this.count = count;
		this.frames = frames;
		this.offsetX = offsetX;
		this.offsetY = offsetY;
		this.loop = loop;
		this.reverse = reverse;
		this.paddingL = paddingL;
		this.paddingR = paddingR;
		this.paddingT = paddingT;
		this.paddingB = paddingB;
	}

	public Texture getTexture() {
		return texture;
	}

	public void setTexture(Texture texture) {
		this.texture = texture;
	}

	public int getFrameCount() {
		return count;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public int getFrames() {
		return frames;
	}

	public int[] getOffsetX() {
		return offsetX;
	}

	public int[] getOffsetY() {
		return offsetY;
	}

	public int getPaddingL() {
		return paddingL;
	}

	public int getPaddingR() {
		return paddingR;
	}

	public int getPaddingT() {
		return paddingT;
	}

	public int getPaddingB() {
		return paddingB;
	}

	public boolean isLoop() {
		return loop;
	}

	public boolean isReverse() {
		return reverse;
	}

	@Override
	public void delayedLoad() {
		if(delayedTxtr != -1) {
			texture = getResource(delayedTxtr, Texture.FOURCC);
			delayedTxtr = -1;
		}
	}

	@Override
	public void read(WordInputStream in) throws IOException {
		delayedTxtr = in.read32bit();
		width = in.read16bit();
		height = in.read16bit();
		paddingL = in.read16bit();
		paddingR = in.read16bit();
		paddingT = in.read16bit();
		paddingB = in.read16bit();
		loop = in.read8bit() != 0;
		reverse = in.read8bit() != 0;
		frames = in.read16bit();
		count = in.read16bit();
		for(int i = 0; i < count; i++) {
			offsetX[i] = in.read16bit();
			offsetY[i] = in.read16bit();
		}
	}

	@Override
	public void write(WordOutputStream out) throws IOException {
		short count = (short) getFrameCount();
		out.write32bit(texture.getID());
		out.write16bit((short) width);
		out.write16bit((short) height);
		out.write16bit((short) paddingL);
		out.write16bit((short) paddingR);
		out.write16bit((short) paddingT);
		out.write16bit((short) paddingB);
		out.write8bit((byte) (loop ? 1 : 0));
		out.write8bit((byte) (reverse ? 1 : 0));
		out.write16bit((short) frames);
		out.write16bit(count);
		for(int i = 0; i < count; i++) {
			out.write16bit((short) offsetX[i]);
			out.write16bit((short) offsetY[i]);
		}
	}

	@Override
	public Resource[] getDependencies() {
		return new Resource[] { texture };
	}

	@Override
	public Element write() {
		Element root = new Element("sprite");
		root.addAttribute("tex", getTexture().getName());
		root.addAttribute("width", Integer.toString(getWidth()));
		root.addAttribute("height", Integer.toString(getHeight()));
		root.addAttribute("frames", Integer.toString(getFrames()));
		root.addAttribute("padding-l", Integer.toString(getPaddingL()));
		root.addAttribute("padding-r", Integer.toString(getPaddingR()));
		root.addAttribute("padding-t", Integer.toString(getPaddingT()));
		root.addAttribute("padding-b", Integer.toString(getPaddingB()));
		root.addAttribute("loop", isLoop() ? "1" : "0");
		root.addAttribute("reverse", isReverse() ? "1" : "0");
		for(int i = 0; i < getFrameCount(); i++) {
			Element offset = new Element("offset");
			offset.addAttribute("x", Integer.toString(getOffsetX()[i]));
			offset.addAttribute("y", Integer.toString(getOffsetY()[i]));
			root.addChild(offset);
		}
		return root;
	}

	@Override
	public void read(Element root) {
		if(!root.name.equals("sprite")) {
			throw new IllegalArgumentException("not a sprite");
		}
		texture = getPak().getProject().getTexture(root.getAttribute("tex"));
		width = Integer.parseInt(root.getAttribute("width"));
		height = Integer.parseInt(root.getAttribute("height"));
		frames = Integer.parseInt(root.getAttribute("frames"));
		paddingL = get(root, "padding-l", 0);
		paddingR = get(root, "padding-r", 0);
		paddingT = get(root, "padding-t", 0);
		paddingB = get(root, "padding-b", 0);
		loop = get(root, "loop", false);
		reverse = get(root, "reverse", false);
		Element[] frameOffsets = root.getElementsByTagName("offset");
		if(frameOffsets.length == 0) {
			throw new IllegalArgumentException("invalid frame count");
		}
		count = frameOffsets.length;
		offsetX = new int[count];
		offsetY = new int[count];
		for(int i = 0; i < count; i++) {
			Element offset = frameOffsets[i];
			offsetX[i] = Integer.parseInt(offset.getAttribute("x"));
			offsetY[i] = Integer.parseInt(offset.getAttribute("y"));
		}
	}

	private static int get(Element e, String name, int def) {
		String a = e.getAttribute(name);
		if(a == null) {
			return def;
		} else {
			return Integer.parseInt(a);
		}
	}

	private static boolean get(Element e, String name, boolean def) {
		String a = e.getAttribute(name);
		if(a == null) {
			return def;
		} else {
			return Integer.parseInt(a) != 0;
		}
	}
}
