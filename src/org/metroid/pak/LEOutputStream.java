package org.metroid.pak;

import java.io.IOException;
import java.io.OutputStream;

public class LEOutputStream extends WordOutputStream {
	public LEOutputStream(OutputStream parent) {
		super(parent);
	}

	public void write8bit(byte value) throws IOException {
		write(value);
	}

	public void write16bit(short value) throws IOException {
		write(Endianess.set16bit_LE(value, new byte[2]));
	}

	public void write32bit(int value) throws IOException {
		write(Endianess.set32bit_LE(value, new byte[4]));
	}

	public void write32bit(float value) throws IOException {
		write(Endianess.set32bit_LE(value, new byte[4]));
	}

	public void write64bit(long value) throws IOException {
		write(Endianess.set64bit_LE(value, new byte[8]));
	}

	public void write64bit(double value) throws IOException {
		write(Endianess.set64bit_LE(value, new byte[8]));
	}
}
