package org.metroid.pak;

public abstract class Utils {
	public static int log2(int i) {
		int n = 0;
		while(i > 0) {
			i >>= 1;
			n++;
		}
		return n - 1;
	}

	public static int round2(int x) {
		int log = log2(x);
		if(1 << log < x)
			return 1 << (log + 1);
		return x;
	}

	public static String fourCC(int fourCC) {
		char[] chars = new char[4];
		chars[0] = (char) ((fourCC >> 24) & 0xFF);
		chars[1] = (char) ((fourCC >> 16) & 0xFF);
		chars[2] = (char) ((fourCC >>  8) & 0xFF);
		chars[3] = (char) ( fourCC        & 0xFF);
		return new String(chars);
	}

	public static int fourCC(String fourCC) {
		int result = 0;
		for(char c : fourCC.toCharArray())
			result = (result << 8) | (c & 0xFF);
		return result;
	}
}
