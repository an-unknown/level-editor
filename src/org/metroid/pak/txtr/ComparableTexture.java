package org.metroid.pak.txtr;

public class ComparableTexture {
	private Texture tex;

	public ComparableTexture(Texture tex) {
		this.tex = tex;
	}

	public Texture getTexture() {
		return tex;
	}

	@Override
	public int hashCode() {
		return tex.getMipmapCount() + tex.getWidth() * 31 + tex.getHeight() * 63;
	}

	@Override
	public boolean equals(Object o) {
		if(!(o instanceof ComparableTexture))
			return false;
		return tex.equals(((ComparableTexture) o).getTexture());
	}

	@Override
	public String toString() {
		return tex.toString();
	}
}
