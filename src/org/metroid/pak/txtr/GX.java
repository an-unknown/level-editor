package org.metroid.pak.txtr;

import java.awt.image.BufferedImage;
import java.util.HashSet;
import java.util.Set;

public class GX {
	public static final int I4 = 0x00;
	public static final int I8 = 0x01;
	public static final int IA4 = 0x02;
	public static final int IA8 = 0x03;
	public static final int C4 = 0x04;
	public static final int C8 = 0x05;
	public static final int C14x2 = 0x6;
	public static final int RGB565 = 0x7;
	public static final int RGB5A3 = 0x08;
	public static final int RGBA8 = 0x09;
	public static final int CMPR = 0x0A;

	public static final int[] BLOCK_WIDTH = { 8, 8, 8, 4, 8, 8, 4, 4, 4, 4, 8 };
	public static final int[] BLOCK_HEIGHT = { 8, 4, 4, 4, 8, 4, 4, 4, 4, 4, 8 };

	public static final String[] FORMATS = { "I4", "I8", "IA4", "IA8", "C4", "C8", "C14x2", "RGB565", "RGB5A3",
			"RGBA8", "CMPR" };

	public static int get(int x, int y, int width, int height, int format, int[] data) {
		int w = BLOCK_WIDTH[format];
		int h = BLOCK_HEIGHT[format];
		int bx = width / w;
		if(width % w != 0)
			bx++;
		int blockX = x / w;
		int blockY = y / h;
		int px = x % w;
		int py = y % h;
		int block = blockY * (w * h * bx) + blockX * (w * h);
		int pixel = block + (py * w + px);
		return data[pixel];
	}

	public static void set(int color, int x, int y, int width, int height, int format, int[] data) {
		int w = BLOCK_WIDTH[format];
		int h = BLOCK_HEIGHT[format];
		int bx = width / w;
		if(width % w != 0)
			bx++;
		int blockX = x / w;
		int blockY = y / h;
		int px = x % w;
		int py = y % h;
		int block = blockY * (w * h * bx) + blockX * (w * h);
		int pixel = block + (py * w + px);
		data[pixel] = color;
	}

	public static int round(int value, int block) {
		if(value % block != 0)
			value = (value - (value % block) + block);
		return value;
	}

	public static int roundWidth(int format, int width) {
		return round(width, BLOCK_WIDTH[format]);
	}

	public static int roundHeight(int format, int height) {
		return round(height, BLOCK_HEIGHT[format]);
	}

	public static int getIA8(int ia8) {
		int a = (ia8 >> 8) & 0xFF;
		int i = ia8 & 0xFF;
		return (a << 24) | (i << 16) | (i << 8) | i;
	}

	public static int getRGB565(int rgb565) {
		int r = (rgb565 >> 8) & 0xF8;
		int g = (rgb565 >> 3) & 0xFC;
		int b = (rgb565 << 3) & 0xF8;
		return 0xFF000000 | (r << 16) | (g << 8) | b;
	}

	public static int getRGB5A3(int rgb5a3) {
		if((rgb5a3 & 0x8000) != 0) {
			// alpha = 0xff, 1RRRRRGGGGGBBBBB
			int r = (rgb5a3 >> 7) & 0xF8;
			int g = (rgb5a3 >> 2) & 0xF8;
			int b = (rgb5a3 << 3) & 0xF8;
			int a = 0xFF;
			return (a << 24) | (r << 16) | (g << 8) | b;
		} else {
			// 0AAARRRRGGGGBBBB
			int a = (rgb5a3 >> 7) & 0xE0;
			int r = (rgb5a3 >> 4) & 0xF0;
			int g = rgb5a3 & 0xF0;
			int b = (rgb5a3 << 4) & 0xF0;
			return (a << 24) | (r << 16) | (g << 8) | b;
		}
	}

	public static int toRGB565(int rgb) {
		int r = (rgb >> 8) & 0xF800;
		int g = (rgb >> 5) & 0x07E0;
		int b = (rgb >> 3) & 0x001F;
		return r | g | b;
	}

	public static int toRGB5A3(int rgba) {
		if((rgba & 0xFF000000) != 0xFF000000) { // has alpha
			int a = (rgba >> 17) & 0x7000;
			int r = (rgba >> 12) & 0x0F00;
			int g = (rgba >> 8) & 0x00F0;
			int b = (rgba >> 4) & 0x000F;
			return a | r | g | b;
		} else { // no alpha
			int r = (rgba >> 9) & 0x7C00;
			int g = (rgba >> 6) & 0x03E0;
			int b = (rgba >> 3) & 0x001F;
			return 0x8000 | r | g | b;
		}
	}

	public static int toIA4(int rgba) {
		int a = (rgba >> 24) & 0xFF;
		int r = (rgba >> 16) & 0xFF;
		int g = (rgba >>  8) & 0xFF;
		int b =  rgba        & 0xFF;
		int intensity = (r + g + b) / 3;
		int a4 = a & 0xF0;
		int i4 = (intensity >> 4) & 0x0F;
		// round
		if((a4 < 0xF0) && (a & 0x80) != 0)
			a4 += 0x10;
		if((i4 < 0x0F) && (intensity & 0x80) != 0)
			i4++;
		return a4 | i4;
	}

	public static int toIA8(int rgba) {
		int a = (rgba >> 24) & 0xFF;
		int r = (rgba >> 16) & 0xFF;
		int g = (rgba >>  8) & 0xFF;
		int b =  rgba        & 0xFF;
		int intensity = (r + g + b) / 3;
		return (a << 8) | (intensity & 0xFF);
	}

	public static int getColorCount(BufferedImage img) {
		Set<Integer> colors = new HashSet<Integer>();
		for(int y = 0; y < img.getHeight(); y++)
			for(int x = 0; x < img.getWidth(); x++)
				colors.add(img.getRGB(x, y));
		return colors.size();
	}

	public static int getColorCountRGB565(BufferedImage img) {
		Set<Integer> colors = new HashSet<Integer>();
		for(int y = 0; y < img.getHeight(); y++)
			for(int x = 0; x < img.getWidth(); x++)
				colors.add(toRGB565(img.getRGB(x, y)));
		return colors.size();
	}

	public static int getColorCountRGB5A3(BufferedImage img) {
		Set<Integer> colors = new HashSet<Integer>();
		for(int y = 0; y < img.getHeight(); y++)
			for(int x = 0; x < img.getWidth(); x++)
				colors.add(toRGB5A3(img.getRGB(x, y)));
		return colors.size();
	}

	public static int getColorCountIA4(BufferedImage img) {
		Set<Integer> colors = new HashSet<Integer>();
		for(int y = 0; y < img.getHeight(); y++) {
			for(int x = 0; x < img.getWidth(); x++) {
				int rgba = img.getRGB(x, y);
				colors.add(toIA4(rgba));
			}
		}
		return colors.size();
	}

	public static boolean isGrayscale(BufferedImage img) {
		for(int y = 0; y < img.getHeight(); y++) {
			for(int x = 0; x < img.getWidth(); x++) {
				int rgba = img.getRGB(x, y);
				int r = (rgba >> 16) & 0xFF;
				int g = (rgba >>  8) & 0xFF;
				int b =  rgba        & 0xFF;
				if(r != g || g != b || r != b)
					return false;
			}
		}
		return true;
	}

	public static boolean hasAlpha(BufferedImage img) {
		for(int y = 0; y < img.getHeight(); y++)
			for(int x = 0; x < img.getWidth(); x++)
				if((img.getRGB(x, y) & 0xFF000000) != 0xFF000000)
					return true;
		return false;
	}
}
