package org.metroid.pak.txtr;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Set;
import java.util.TreeSet;

import org.metroid.pak.Endianess;
import org.metroid.pak.WordInputStream;
import org.metroid.pak.WordOutputStream;

public class Palette {
	public static final int IA8 = 0x00;
	public static final int RGB565 = 0x01;
	public static final int RGB5A3 = 0x02;

	public static final String[] FORMATS = { "IA8", "RGB565", "RGB5A3" };

	private int format;
	private int[] colors;
	private int used;

	public Palette() {
		format = -1;
		colors = null;
		used = 0;
	}

	public Palette(BufferedImage image, int format, int colorCount) {
		this.format = format;
		this.colors = new int[colorCount];
		createPalette(image);
	}

	private void createPalette(BufferedImage image) {
		used = 0;
		add(image);
		for(int i = used; i < this.colors.length; i++)
			this.colors[i] = 0xFFFFFFFF;
	}

	public void add(BufferedImage image) {
		Set<Integer> colors = new TreeSet<Integer>();
		switch(format) {
		case IA8:
			for(int y = 0; y < image.getHeight(); y++) {
				for(int x = 0; x < image.getWidth(); x++) {
					int color = image.getRGB(x, y);
					int a = (color >> 24) & 0xFF;
					int i = (color >> 16) & 0xFF;
					colors.add(a << 8 | i);
				}
			}
			break;
		case RGB565:
			for(int y = 0; y < image.getHeight(); y++) {
				for(int x = 0; x < image.getWidth(); x++) {
					int color = image.getRGB(x, y);
					colors.add(GX.toRGB565(color));
				}
			}
			break;
		case RGB5A3:
			for(int y = 0; y < image.getHeight(); y++) {
				for(int x = 0; x < image.getWidth(); x++) {
					int color = image.getRGB(x, y);
					colors.add(GX.toRGB5A3(color));
				}
			}
			break;
		default:
			throw new IllegalStateException(String.format("invalid format: %02X", format));
		}

		for(int color : colors) {
			if(used >= this.colors.length)
				break;
			this.colors[used++] = color;
		}
	}

	public int getIndex(int color) {
		int value;
		switch(format) {
		case IA8:
			value = GX.toIA8(color);
			break;
		case RGB565:
			value = GX.toRGB565(color);
			break;
		case RGB5A3:
			value = GX.toRGB5A3(color);
			break;
		default:
			throw new IllegalStateException(String.format("invalid format: %02X", format));
		}
		for(int i = 0; i < colors.length; i++)
			if(colors[i] == value)
				return i;
		return -1;
	}

	public void setColors(int[] colors) {
		this.colors = colors;
	}

	public int getCount() {
		return colors.length;
	}

	public int getFormat() {
		return format;
	}

	public int[] getColors() {
		return colors;
	}

	public int getColor(int i) {
		switch(format) {
		case IA8:
			return GX.getIA8(colors[i]);
		case RGB565:
			return GX.getRGB565(colors[i]);
		case RGB5A3:
			return GX.getRGB5A3(colors[i]);
		default:
			return 0;
		}
	}

	public void read(WordInputStream in) throws IOException {
		format = in.read32bit();
		short width = in.read16bit();
		short height = in.read16bit();
		colors = new int[width * height];

		byte[] buffer;
		switch(format) {
		case IA8:
			buffer = new byte[colors.length * 2];
			in.read(buffer);
			for(int i = 0; i < colors.length; i++)
				colors[i] = Endianess.get16bit_BE(buffer, i * 2) & 0xFFFF;
			break;
		case RGB565:
			buffer = new byte[colors.length * 2];
			in.read(buffer);
			for(int i = 0; i < colors.length; i++)
				colors[i] = Endianess.get16bit_BE(buffer, i * 2) & 0xFFFF;
			break;
		case RGB5A3:
			buffer = new byte[colors.length * 2];
			in.read(buffer);
			for(int i = 0; i < colors.length; i++)
				colors[i] = Endianess.get16bit_BE(buffer, i * 2) & 0xFFFF;
			break;
		}
	}

	public void write(WordOutputStream out) throws IOException {
		out.write32bit(format);
		if(colors.length == 16) { // C4
			out.write16bit((short) 1); // width
			out.write16bit((short) 16); // height
		} else { // C8
			out.write16bit((short) 256); // width
			out.write16bit((short) 1); // height
		}

		byte[] buffer = new byte[colors.length * 2];
		for(int i = 0; i < colors.length; i++)
			Endianess.set16bit_BE(colors[i], buffer, 2 * i);
		out.write(buffer);
	}

	@Override
	public String toString() {
		return "Palette[format=" + format + ";" + colors.length + "colors]";
	}
}
