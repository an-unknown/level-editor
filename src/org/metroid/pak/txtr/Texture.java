package org.metroid.pak.txtr;

import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.metroid.pak.BEInputStream;
import org.metroid.pak.Endianess;
import org.metroid.pak.Resource;
import org.metroid.pak.WordInputStream;
import org.metroid.pak.WordOutputStream;
import org.metroid.project.Project;
import org.metroid.xml.dom.Element;

public class Texture extends Resource {
	public static final int FOURCC = 0x54585452;

	private BufferedImage[] image;
	private int format;
	private short width;
	private short height;
	private int mipmapCount;

	private String filename;

	public Texture() {
		super(FOURCC);
		this.image = null;
		this.format = -1;
		this.width = -1;
		this.height = -1;
		this.mipmapCount = -1;
	}

	public Texture(BufferedImage image) {
		this(image, false);
	}

	public Texture(BufferedImage image, boolean reload) {
		super(FOURCC);
		read(image, reload);
	}

	public Texture(WordInputStream in) throws IOException {
		super(FOURCC);
		read(in);
	}

	public Texture(InputStream in) throws IOException {
		super(FOURCC);
		read(new BEInputStream(in));
	}

	protected void read(BufferedImage image, boolean reload) {
		this.image = new BufferedImage[] { image };
		this.format = getBestFormat();
		this.width = (short) image.getWidth();
		this.height = (short) image.getHeight();
		this.mipmapCount = 1;
		if(!reload)
			return;
		// reload to get correct colors
		try {
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			write(out);
			out.flush();
			out.close();
			WordInputStream in = new BEInputStream(new ByteArrayInputStream(out.toByteArray()));
			read(in);
			in.close();
		} catch(IOException e) {
			e.printStackTrace();
			this.image = new BufferedImage[] { image };
			this.format = getBestFormat();
			this.width = (short) image.getWidth();
			this.height = (short) image.getHeight();
			this.mipmapCount = 1;
		}
	}

	public BufferedImage getTexture() {
		return image[0];
	}

	public BufferedImage getTexture(int mipmap) {
		return image[mipmap];
	}

	public BufferedImage getSubimage(int x, int y, int w, int h) {
		return getTexture().getSubimage(x, y, w, h);
	}

	public BufferedImage getSubimage(int mipmap, int x, int y, int w, int h) {
		return getTexture(mipmap).getSubimage(x, y, w, h);
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public int getMipmapCount() {
		return mipmapCount;
	}

	public int getFormat() {
		return format;
	}

	private static Dimension[] getMipmapSizes(int width, int height) {
		List<Dimension> sizes = new ArrayList<Dimension>();
		while(width > 1 && height > 1) {
			sizes.add(new Dimension(width, height));
			if(width > 1)
				width /= 2;
			if(height > 1)
				height /= 2;
		}
		return sizes.toArray(new Dimension[sizes.size()]);
	}

	@Override
	public void read(WordInputStream in) throws IOException {
		format = in.read32bit();
		width = in.read16bit();
		height = in.read16bit();
		mipmapCount = in.read32bit();

		image = new BufferedImage[mipmapCount];

		int dataWidth = GX.roundWidth(format, width);
		int dataHeight = GX.roundHeight(format, height);

		// read palette and pixel data
		Palette pal = null;
		if(format == GX.C4 || format == GX.C8) {
			pal = new Palette();
			pal.read(in);
		}

		byte[] buffer;
		int[] pixels = new int[dataWidth * dataHeight];
		NibbleInputStream nibbles;

		if(mipmapCount == 0)
			throw new IOException("at least 1 mipmap required");

		Dimension[] sizes = getMipmapSizes(width, height);

		if(sizes.length != (mipmapCount + 1) && (mipmapCount != 1))
			throw new IOException("invalid mipmap count! " + sizes.length + " vs " + mipmapCount);

		for(int mipmap = 0; mipmap < mipmapCount; mipmap++) {
			int width = sizes[mipmap].width;
			int height = sizes[mipmap].height;
			dataWidth = GX.roundWidth(format, width);
			dataHeight = GX.roundHeight(format, height);
			pixels = new int[dataWidth * dataHeight];
			switch(format) {
			case GX.I4:
				buffer = new byte[pixels.length / 2];
				in.read(buffer);
				nibbles = new NibbleInputStream(buffer);
				for(int i = 0; i < pixels.length; i++) {
					int intensity = (nibbles.read4bit() & 0xFF) << 4;
					pixels[i] = 0xFF000000 | (intensity << 16) | (intensity << 8) | intensity;
				}
				break;
			case GX.I8:
				buffer = new byte[pixels.length];
				in.read(buffer);
				for(int i = 0; i < pixels.length; i++) {
					int intensity = buffer[i] & 0xFF;
					pixels[i] = 0xFF000000 | (intensity << 16) | (intensity << 8) | intensity;
				}
				break;
			case GX.C4:
				buffer = new byte[pixels.length / 2];
				in.read(buffer);
				nibbles = new NibbleInputStream(buffer);
				for(int i = 0; i < pixels.length; i++)
					pixels[i] = pal.getColor(nibbles.read4bit() & 0x0F);
				break;
			case GX.C8:
				buffer = new byte[pixels.length];
				in.read(buffer);
				for(int i = 0; i < pixels.length; i++)
					pixels[i] = pal.getColor(buffer[i] & 0xFF);
				break;
			case GX.RGB565:
				buffer = new byte[pixels.length * 2];
				in.read(buffer);
				for(int i = 0; i < pixels.length; i++)
					pixels[i] = GX.getRGB565(Endianess.get16bit_BE(buffer, i * 2) & 0xFFFF);
				break;
			case GX.RGB5A3:
				buffer = new byte[pixels.length * 2];
				in.read(buffer);
				for(int i = 0; i < pixels.length; i++)
					pixels[i] = GX.getRGB5A3(Endianess.get16bit_BE(buffer, i * 2) & 0xFFFF);
				break;
			default:
				throw new IOException(String.format("unknown format: %02X", format));
			}

			image[mipmap] = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
			// parse blocks
			for(int y = 0; y < height; y++)
				for(int x = 0; x < width; x++)
					image[mipmap].setRGB(x, y, GX.get(x, y, width, height, format, pixels));
		}
	}

	public int getBestFormat() {
		int colors = GX.getColorCount(image[0]);
		int rgb565 = GX.getColorCountRGB565(image[0]);
		int rgb5A3 = GX.getColorCountRGB5A3(image[0]);
		int ia4 = GX.getColorCountIA4(image[0]);
		boolean hasAlpha = GX.hasAlpha(image[0]);
		boolean isIntensity = GX.isGrayscale(image[0]);
		if(hasAlpha) {
			if(isIntensity) {
				if(colors <= 16)
					return GX.C4;
				else if(colors <= 256)
					return GX.C8;
				else if(ia4 == colors)
					return GX.IA4;
				else if(colors == rgb5A3) {
					if(colors <= 16)
						return GX.C4;
					else if(colors <= 256)
						return GX.C8;
					else
						return GX.RGB5A3;
				} else
					return GX.IA8;
			} else if(colors - rgb5A3 > 256)
				return GX.RGB5A3;
			// return GX.RGBA8;
			else if(rgb5A3 <= 16)
				return GX.C4;
			else if(rgb5A3 <= 256)
				return GX.C8;
			else
				return GX.RGB5A3;
		} else {
			if(isIntensity) {
				if(ia4 == colors)
					return GX.I4;
				else if(colors <= 16)
					return GX.C4;
				else if(colors <= 256)
					return GX.C8;
				else if(colors == rgb5A3) {
					if(colors <= 16)
						return GX.C4;
					else if(colors <= 256)
						return GX.C8;
					else
						return GX.RGB5A3;
				} else if(colors == rgb565) {
					if(colors <= 16)
						return GX.C4;
					else if(colors <= 256)
						return GX.C8;
					else
						return GX.RGB565;
				} else
					return GX.I8;
			} else if(colors - rgb565 > 256)
				// return GX.RGBA8;
				return GX.RGB565;
			else if(rgb565 <= 16)
				return GX.C4;
			else if(rgb565 <= 256)
				return GX.C8;
			else if(rgb565 > rgb5A3)
				return GX.RGB565;
			else
				return GX.RGB5A3;
		}
	}

	public int getBestPaletteFormat() {
		int colors = GX.getColorCount(image[0]);
		int rgb565 = GX.getColorCountRGB565(image[0]);
		int rgb5A3 = GX.getColorCountRGB5A3(image[0]);
		boolean hasAlpha = GX.hasAlpha(image[0]);
		boolean isIntensity = GX.isGrayscale(image[0]);
		if(hasAlpha) {
			if(isIntensity) {
				if(colors <= 256)
					return Palette.IA8;
				else if(colors == rgb5A3)
					return Palette.RGB5A3;
			} else if(rgb5A3 <= 16)
				return Palette.RGB5A3;
			else if(rgb5A3 <= 256)
				return Palette.RGB5A3;
		} else {
			if(isIntensity) {
				if(colors <= 16)
					return Palette.IA8;
				else if(colors <= 256)
					return Palette.IA8;
				else if(colors == rgb5A3) {
					if(colors <= 16)
						return Palette.RGB5A3;
					else if(colors <= 256)
						return Palette.RGB5A3;
				} else if(colors == rgb565) {
					if(colors <= 16)
						return Palette.RGB565;
					else if(colors <= 256)
						return Palette.RGB565;
				}
			} else if(colors - rgb565 > 256)
				return Palette.RGB565;
			else if(rgb565 <= 16)
				return Palette.RGB565;
			else if(rgb565 <= 256)
				return Palette.RGB565;
		}
		throw new IllegalStateException("unknown palette format");
	}

	@Override
	public void write(WordOutputStream out) throws IOException {
		out.write32bit(format);
		out.write16bit(width);
		out.write16bit(height);
		out.write32bit(mipmapCount);

		Palette pal = null;
		if(format == GX.C4 || format == GX.C8) {
			pal = new Palette(image[0], getBestPaletteFormat(), format == GX.C4 ? 16 : 256);
			for(int i = 1; i < mipmapCount; i++)
				pal.add(image[i]);
			pal.write(out);
		}

		Dimension[] sizes = getMipmapSizes(width, height);

		if(sizes.length != (mipmapCount + 1) && (mipmapCount != 1))
			throw new IOException("invalid mipmap count! " + sizes.length + " vs " + mipmapCount);

		for(int mipmap = 0; mipmap < mipmapCount; mipmap++) {
			int width = sizes[mipmap].width;
			int height = sizes[mipmap].height;

			int dataWidth = GX.roundWidth(format, width);
			int dataHeight = GX.roundHeight(format, height);

			byte[] buffer;
			NibbleOutputStream nibbles = new NibbleOutputStream();
			int[] pixels = new int[dataWidth * dataHeight];
			for(int i = 0; i < pixels.length; i++)
				pixels[i] = 0xFFFFFFFF;
			for(int y = 0; y < height; y++)
				for(int x = 0; x < width; x++)
					GX.set(image[mipmap].getRGB(x, y), x, y, width, height, format, pixels);

			switch(format) {
			case GX.I4:
				for(int i = 0; i < pixels.length; i++)
					nibbles.write4bit((byte) (pixels[i] >> 4));
				buffer = nibbles.getBytes();
				break;
			case GX.I8:
				buffer = new byte[pixels.length];
				for(int i = 0; i < pixels.length; i++)
					buffer[i] = (byte) pixels[i];
				break;
			case GX.C4:
				for(int i = 0; i < pixels.length; i++)
					nibbles.write4bit((byte) pal.getIndex(pixels[i]));
				buffer = nibbles.getBytes();
				break;
			case GX.C8:
				buffer = new byte[pixels.length];
				for(int i = 0; i < pixels.length; i++)
					buffer[i] = (byte) pal.getIndex(pixels[i]);
				break;
			case GX.RGB565:
				buffer = new byte[pixels.length * 2];
				for(int i = 0; i < pixels.length; i++)
					Endianess.set16bit_BE(GX.toRGB565(pixels[i]), buffer, 2 * i);
				break;
			case GX.RGB5A3:
				buffer = new byte[pixels.length * 2];
				for(int i = 0; i < pixels.length; i++)
					Endianess.set16bit_BE(GX.toRGB5A3(pixels[i]), buffer, 2 * i);
				break;
			default:
				throw new IOException(String.format("unknown format: %02X", format));
			}
			out.write(buffer);
		}
	}

	public boolean equals(Texture t) {
		if(t.mipmapCount != mipmapCount)
			return false;
		for(int i = 0; i < t.mipmapCount; i++) {
			BufferedImage a = t.image[i];
			BufferedImage b = image[i];
			if(a.getWidth() != b.getWidth() || a.getHeight() != b.getHeight())
				return false;
			for(int y = 0; y < a.getHeight(); y++)
				for(int x = 0; x < a.getHeight(); x++)
					if(a.getRGB(x, y) != b.getRGB(x, y))
						return false;
		}
		return true;
	}

	public void setFileName(String name) {
		this.filename = name;
	}

	public String getFileName() {
		return filename;
	}

	@Override
	public Element write() {
		Element root = new Element("texture", getFileName());
		return root;
	}

	@Override
	public void read(Element root) throws IOException {
		if(!root.name.equals("texture"))
			throw new IOException("not a texture");
		Project prj = getPak().getProject();
		BufferedImage img = prj.loadTexture(root.value);
		read(img, true);
	}
}
