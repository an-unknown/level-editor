package org.metroid.pak;

// @formatter:off
public class Endianess {
	public static final int get16bit_BE(byte buffer[]) {
		return(((buffer[0] & 0xFF)<< 8) | (buffer[1] & 0xFF));
	}

	public static final int get16bit_BE(byte buffer[], int offset) {
		return(((buffer[offset +0] & 0xFF)<< 8) | (buffer[offset +1] & 0xFF));
	}

	public static final int get32bit_BE(byte buffer[]) {
		return(
			((buffer[0] & 0xFF) << 24) |
			((buffer[1] & 0xFF) << 16) |
			((buffer[2] & 0xFF) << 8) |
			 (buffer[3] & 0xFF));
	}

	public static final int get32bit_BE(byte buffer[], int offset) {
		return(
			((buffer[offset +0] & 0xFF) << 24) |
			((buffer[offset +1] & 0xFF) << 16) |
			((buffer[offset +2] & 0xFF) << 8) |
			 (buffer[offset +3] & 0xFF));
	}

	public static final long get64bit_BE(byte buffer[]) {
		return(
			((buffer[0] & 0xFF) << 56) |
			((buffer[1] & 0xFF) << 48) |
			((buffer[2] & 0xFF) << 40) |
			((buffer[3] & 0xFF) << 32) |
			((buffer[4] & 0xFF) << 24) |
			((buffer[5] & 0xFF) << 16) |
			((buffer[6] & 0xFF) <<  8) |
			 (buffer[7] & 0xFF));
	}

	public static final int get16bit_LE(byte buffer[]) {
		return((buffer[0] & 0xFF) | ((buffer[1] & 0xFF) << 8));
	}

	public static final int get16bit_LE(byte buffer[], int offset) {
		return((buffer[offset] & 0xFF) | ((buffer[offset + 1] & 0xFF) << 8));
	}

	public static final int get32bit_LE(byte buffer[]) {
		return(
			 (buffer[0] & 0xFF) |
			((buffer[1] & 0xFF) << 8) |
			((buffer[2] & 0xFF) << 16) |
			((buffer[3] & 0xFF) << 24));
	}

	public static final int get32bit_LE(byte buffer[], int offset) {
		return(
			 (buffer[offset + 0] & 0xFF) |
			((buffer[offset + 1] & 0xFF) << 8) |
			((buffer[offset + 2] & 0xFF) << 16) |
			((buffer[offset + 3] & 0xFF) << 24));
	}

	public static final long get64bit_LE(byte buffer[]) {
		return(
			 buffer[0] & 0xFF |
			((long)(buffer[1] & 0xFF) <<  8) |
			((long)(buffer[2] & 0xFF) << 16) |
			((long)(buffer[3] & 0xFF) << 24) |
			((long)(buffer[4] & 0xFF) << 32) |
			((long)(buffer[5] & 0xFF) << 40) |
			((long)(buffer[6] & 0xFF) << 48) |
			((long)(buffer[7] & 0xFF) << 56)
		);
	}

	public static final long get64bit_LE(byte buffer[], int offset) {
		return(
			 buffer[offset + 0] & 0xFF |
			((long)(buffer[offset + 1] & 0xFF) <<  8) |
			((long)(buffer[offset + 2] & 0xFF) << 16) |
			((long)(buffer[offset + 3] & 0xFF) << 24) |
			((long)(buffer[offset + 4] & 0xFF) << 32) |
			((long)(buffer[offset + 5] & 0xFF) << 40) |
			((long)(buffer[offset + 6] & 0xFF) << 48) |
			((long)(buffer[offset + 7] & 0xFF) << 56)
		);
	}

	public static final byte[] set16bit_BE(int num, byte buffer[], int offset) {
		buffer[offset + 0] = (byte) ((num >> 8) & 0xFF);
		buffer[offset + 1] = (byte) (num & 0xFF);
		return buffer;
	}

	public static final byte[] set32bit_BE(int num, byte buffer[], int offset) {
		buffer[offset + 0] = (byte) ((num >> 24) & 0xFF);
		buffer[offset + 1] = (byte) ((num >> 16) & 0xFF);
		buffer[offset + 2] = (byte) ((num >>  8) & 0xFF);
		buffer[offset + 3] = (byte) (num & 0xFF);
		return buffer;
	}

	public static final byte[] set32bit_BE(long num, byte buffer[], int offset) {
		buffer[offset + 0] = (byte) ((num >> 24) & 0xFF);
		buffer[offset + 1] = (byte) ((num >> 16) & 0xFF);
		buffer[offset + 2] = (byte) ((num >>  8) & 0xFF);
		buffer[offset + 3] = (byte) (num & 0xFF);
		return buffer;
	}

	public static final byte[] set64bit_BE(long num, byte buffer[], int offset) {
		buffer[offset + 0] = (byte) ((num >> 56) & 0xFF);
		buffer[offset + 1] = (byte) ((num >> 48) & 0xFF);
		buffer[offset + 2] = (byte) ((num >> 40) & 0xFF);
		buffer[offset + 3] = (byte) ((num >> 32) & 0xFF);
		buffer[offset + 4] = (byte) ((num >> 24) & 0xFF);
		buffer[offset + 5] = (byte) ((num >> 16) & 0xFF);
		buffer[offset + 6] = (byte) ((num >>  8) & 0xFF);
		buffer[offset + 7] = (byte) (num & 0xFF);
		return buffer;
	}

	public static final byte[] set32bit_BE(float f, byte buffer[], int offset) {
		int num = Float.floatToRawIntBits(f);
		return set32bit_BE(num, buffer, offset);
	}

	public static final byte[] set64bit_BE(double f, byte buffer[], int offset) {
		long num = Double.doubleToRawLongBits(f);
		return set64bit_BE(num, buffer, offset);
	}

	public static final byte[] set16bit_BE(int num, byte[] buffer) {
		return set16bit_BE(num, buffer, 0);
	}

	public static final byte[] set32bit_BE(int num, byte[] buffer) {
		return set32bit_BE(num, buffer, 0);
	}

	public static final byte[] set32bit_BE(float f, byte[] buffer) {
		return set32bit_BE(f, buffer, 0);
	}

	public static final byte[] set64bit_BE(double f, byte[] buffer) {
		return set64bit_BE(f, buffer, 0);
	}

	public static final byte[] set16bit_LE(short value, byte[] buffer, int offset) {
		buffer[offset    ] = (byte) value;
		buffer[offset + 1] = (byte) (value >> 8);
		return buffer;
	}

	public static final byte[] set32bit_LE(int value, byte[] buffer, int offset) {
		buffer[offset    ] = (byte) value;
		buffer[offset + 1] = (byte) (value >> 8);
		buffer[offset + 2] = (byte) (value >> 16);
		buffer[offset + 3] = (byte) (value >> 24);
		return buffer;
	}

	public static final byte[] set64bit_LE(long value, byte[] buffer, int offset) {
		buffer[offset    ] = (byte) value;
		buffer[offset + 1] = (byte) (value >> 8);
		buffer[offset + 2] = (byte) (value >> 16);
		buffer[offset + 3] = (byte) (value >> 24);
		buffer[offset + 4] = (byte) (value >> 32);
		buffer[offset + 5] = (byte) (value >> 40);
		buffer[offset + 6] = (byte) (value >> 48);
		buffer[offset + 7] = (byte) (value >> 56);
		return buffer;
	}

	public static final byte[] set32bit_LE(float f, byte buffer[], int offset) {
		int num = Float.floatToRawIntBits(f);
		return set32bit_LE(num, buffer, offset);
	}

	public static final byte[] set64bit_LE(double d, byte buffer[], int offset) {
		long num = Double.doubleToRawLongBits(d);
		return set64bit_LE(num, buffer, offset);
	}

	public static final byte[] set16bit_LE(short value, byte[] buffer) {
		return set16bit_LE(value, buffer, 0);
	}

	public static final byte[] set32bit_LE(int value, byte[] buffer) {
		return set32bit_LE(value, buffer, 0);
	}

	public static final byte[] set32bit_LE(float value, byte[] buffer) {
		return set32bit_LE(value, buffer, 0);
	}

	public static final byte[] set64bit_LE(long value, byte[] buffer) {
		return set64bit_LE(value, buffer, 0);
	}

	public static final byte[] set64bit_LE(double value, byte[] buffer) {
		return set64bit_LE(value, buffer, 0);
	}
}
