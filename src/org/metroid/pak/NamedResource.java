package org.metroid.pak;

public class NamedResource {
	private Pak pak;
	private String name;
	private final Resource resource;

	public NamedResource(String name, Resource resource) {
		this.name = name;
		this.resource = resource;
	}

	public NamedResource(Pak pak, String name, Resource resource) {
		this(name, resource);
		this.pak = pak;
	}

	public String getName() {
		return name;
	}

	public Resource getResource() {
		return resource;
	}

	public void setName(String name) {
		if(this.pak != null)
			pak.rename(resource.getType(), this.name, name);
		this.name = name;
	}
}
