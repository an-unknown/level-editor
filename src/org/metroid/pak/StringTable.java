package org.metroid.pak;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.metroid.xml.dom.Element;

public class StringTable extends Resource {
	public static final int FOURCC = 0x53545247;

	public static final int MAGIC = 0x87654321;
	public static final int VERSION = 0x01;

	private Map<String, Integer> names;
	private Map<Integer, List<String>> strings;

	public StringTable() {
		super(FOURCC);
	}

	private class Language {
		public final int fourCC;
		public final int offset;
		public final int size;

		public Language(int fourCC, int offset, int size) {
			this.fourCC = fourCC;
			this.offset = offset;
			this.size = size;
		}

		@Override
		public String toString() {
			return "Language[lang=" + Utils.fourCC(fourCC) + ";offset=" + offset + ";size=" + size + "]";
		}
	}

	private class Name {
		public final int offset;
		public final int index;
		public String name;

		public Name(int offset, int index) {
			this.offset = offset;
			this.index = index;
		}

		@Override
		public String toString() {
			return "Name[offset=" + offset + ";index=" + index + ";name=" + name + "]";
		}
	}

	private class StoredString {
		private final int offset;
		private final String string;

		public StoredString(int offset, String string) {
			this.offset = offset;
			this.string = string;
		}
	}

	public void clear() {
		names = new HashMap<String, Integer>();
		strings = new HashMap<Integer, List<String>>();
	}

	private void setStringNames(Name[] names) {
		this.names = new HashMap<String, Integer>();
		for(Name name : names)
			this.names.put(name.name, name.index);
	}

	private void setStringTable(int lang, String[] strings) {
		this.strings.put(lang, new ArrayList<String>(Arrays.asList(strings)));
	}

	public int addString(int lang, String string) {
		List<String> s = strings.get(lang);
		if(s == null) {
			s = new ArrayList<String>();
			strings.put(lang, s);
		}

		int id = s.size();
		s.add(string);
		return id;
	}

	public void addName(String name, int id) {
		names.put(name, id);
	}

	public String getString(int lang, String name) {
		int id = names.get(name);
		List<String> s = strings.get(lang);
		return s.get(id);
	}

	public String[] getNames() {
		String[] result = new String[names.size()];
		int i = 0;
		for(String name : names.keySet())
			result[i++] = name;
		return result;
	}

	public boolean check() {
		int length = -1;
		for(Map.Entry<Integer, List<String>> lang : strings.entrySet()) {
			if(length == -1)
				length = lang.getValue().size();
			else if(length != lang.getValue().size())
				return false;
		}
		return true;
	}

	public int size() {
		if(!check())
			return -1;
		return strings.values().iterator().next().size();
	}

	@Override
	public void read(WordInputStream in) throws IOException {
		clear();

		int magic = in.read32bit();
		if(magic != MAGIC)
			throw new IOException(String.format("Invalid magic: 0x%08X", magic));
		int version = in.read32bit();
		if(version != VERSION)
			throw new IOException(String.format("Invalid version: %d", version));
		int languageCount = in.read32bit();
		int stringCount = in.read32bit();
		Language[] languages = new Language[languageCount];
		for(int i = 0; i < languageCount; i++) {
			int id = in.read32bit();
			int offset = in.read32bit();
			int size = in.read32bit();
			languages[i] = new Language(id, offset, size);
		}
		int nameCount = in.read32bit();
		int nameTableSize = in.read32bit();
		Name[] names = new Name[nameCount];
		long offset = in.tell();
		for(int i = 0; i < nameCount; i++) {
			int nameOffset = in.read32bit();
			int stringIndex = in.read32bit();
			names[i] = new Name(nameOffset, stringIndex);
		}

		StoredString[] nameStrings = new StoredString[nameCount];
		for(int i = 0; i < nameCount; i++) {
			int off = (int) (in.tell() - offset);
			StringBuilder b = new StringBuilder();
			int ch = in.read8bit();
			while(ch != 0) {
				b.append((char) ch);
				ch = in.read8bit();
			}
			nameStrings[i] = new StoredString(off, b.toString());
		}

		for(int i = 0; i < nameCount; i++) {
			Name name = names[i];
			for(int j = 0; j < nameCount; j++) {
				if(nameStrings[j].offset == name.offset) {
					name.name = nameStrings[j].string;
					break;
				}
			}
		}
		setStringNames(names);

		long end = offset + nameTableSize;
		if(end - in.tell() != 0)
			throw new IOException("error!");

		long table = in.tell();
		for(int i = 0; i < languageCount; i++) {
			offset = in.tell();
			int tbloff = (int) (offset - table);
			Language lang = null;
			for(int j = 0; j < languages.length; j++) {
				if(languages[j].offset == tbloff) {
					lang = languages[j];
					break;
				}
			}
			if(lang == null)
				throw new IOException("language table not found!");
			int[] offsets = new int[stringCount];
			String[] strings = new String[stringCount];
			for(int j = 0; j < offsets.length; j++)
				offsets[j] = in.read32bit();
			for(int j = 0; j < stringCount; j++) {
				int off = (int) (in.tell() - offset);
				StringBuilder b = new StringBuilder();
				int ch = in.read16bit();
				while(ch != 0) {
					b.append((char) ch);
					ch = in.read16bit();
				}
				for(int k = 0; k < offsets.length; k++) {
					if(offsets[k] == off) {
						strings[k] = b.toString();
						break;
					}
				}
			}
			setStringTable(lang.fourCC, strings);
		}
	}

	@Override
	public void write(WordOutputStream out) throws IOException {
		// TODO
	}

	private String getName(int id) {
		for(Map.Entry<String, Integer> entry : names.entrySet())
			if(entry.getValue() == id)
				return entry.getKey();
		return Integer.toString(id);
	}

	public void dump() {
		System.out.printf("%d strings\n", size());
		for(Map.Entry<Integer, List<String>> lang : this.strings.entrySet()) {
			System.out.printf("LANG: '%s'\n", Utils.fourCC(lang.getKey()));
			int i = 0;
			for(String s : lang.getValue())
				System.out.printf("'%s': '%s' -> '%s'\n", Utils.fourCC(lang.getKey()), getName(i++), s);
		}
	}

	@Override
	public Element write() {
		Element root = new Element("strings");
		for(Map.Entry<Integer, List<String>> lang : strings.entrySet()) {
			Element xmllang = new Element("language");
			xmllang.addAttribute("lang", Utils.fourCC(lang.getKey()));
			int i = 0;
			for(String s : lang.getValue()) {
				Element string = new Element("string", s);
				string.addAttribute("name", getName(i++));
				xmllang.addChild(string);
			}
			root.addChild(xmllang);
		}
		return root;
	}

	@Override
	public void read(Element root) throws IOException {
		if(!root.name.equals("strings"))
			throw new IOException("not a string table");
	}
}
