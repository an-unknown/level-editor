package org.metroid.pak;

import java.io.ByteArrayInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import org.metroid.pak.level.Level;
import org.metroid.pak.level.Room;
import org.metroid.pak.level.Tileset;
import org.metroid.pak.txtr.Texture;
import org.metroid.project.Project;
import org.metroid.project.XMLSerializable;
import org.metroid.util.UniqueSequence;

public abstract class Resource implements XMLSerializable {
	private static final UniqueSequence seq = new UniqueSequence();

	private int id = -1;
	private int type = 0;

	private String name = null;
	private boolean exportName = false;

	private Pak pak;

	public Resource(int type) {
		this.id = seq.next();
		this.type = type;
	}

	protected static Resource parse(Pak pak, int id, int fourCC, byte[] bytes) {
		try(WordInputStream in = new BEInputStream(new ByteArrayInputStream(bytes))) {
			switch(fourCC) {
			case StringTable.FOURCC: {
				StringTable table = new StringTable();
				table.setID(id);
				table.setPak(pak);
				table.read(in);
				return table;
			}
			case Texture.FOURCC: {
				Texture texture = new Texture();
				texture.setID(id);
				texture.setPak(pak);
				texture.read(in);
				return texture;
			}
			case Tileset.FOURCC: {
				Tileset tileset = new Tileset(pak.getProject());
				tileset.setID(id);
				tileset.setPak(pak);
				tileset.read(in);
				return tileset;
			}
			case Room.FOURCC: {
				Room room = new Room();
				room.setID(id);
				room.setPak(pak);
				room.read(in);
				return room;
			}
			case Level.FOURCC: {
				Level level = new Level();
				level.setID(id);
				level.setPak(pak);
				level.read(in);
				return level;
			}
			}
		} catch(IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	protected void setID(int id) {
		this.id = id;
	}

	public int getID() {
		return id;
	}

	public void setPak(Pak pak) {
		this.pak = pak;
	}

	public int getType() {
		return type;
	}

	public Pak getPak() {
		return pak;
	}

	public Project getProject() {
		if(getPak() == null)
			return null;
		else
			return getPak().getProject();
	}

	@SuppressWarnings("unchecked")
	protected <T> T getResource(int id, int fourCC) {
		if(pak == null)
			throw new IllegalStateException("pak is null!");
		return (T) pak.get(fourCC, id);
	}

	public void prepareForWrite() {
	}

	public void delayedLoad() {
	}

	public abstract void read(WordInputStream in) throws IOException;

	public abstract void write(WordOutputStream out) throws IOException;

	public final void read(InputStream in) throws IOException {
		read(new BEInputStream(in));
	}

	public final void write(OutputStream out) throws IOException {
		write(new BEOutputStream(out));
	}

	@Override
	public final int hashCode() {
		return id;
	}

	@Override
	public final boolean equals(Object o) {
		return o == this;
	}

	public Resource[] getDependencies() {
		return new Resource[0];
	}

	private void processDependency(Resource r, List<Resource> out) {
		// r.prepareForWrite();
		Resource[] dependencies = r.getDependencies();
		for(Resource dep : dependencies)
			processDependency(dep, out);
		if(!out.contains(r))
			out.add(r);
	}

	public Resource[] getAllDependencies() {
		// prepareForWrite();
		List<Resource> dependencies = new ArrayList<Resource>();
		for(Resource r : getDependencies())
			processDependency(r, dependencies);
		return dependencies.toArray(new Resource[dependencies.size()]);
	}

	public void writeToFile(String dir) throws IOException {
		String name = String.format("%s/%08x.%s", dir, getID(), Utils.fourCC(getType()));
		try(OutputStream out = new FileOutputStream(name)) {
			write(out);
		}
	}

	public void writeToFiles(String dir) throws IOException {
		Resource[] dependencies = getAllDependencies();
		writeToFile(dir);
		for(Resource r : dependencies)
			r.writeToFile(dir);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isExportName() {
		return exportName;
	}

	public void setExportName(boolean exportName) {
		this.exportName = exportName;
	}

	@Override
	public String toString() {
		return String.format("Resource[fourCC=%s,id=0x%08X]", Utils.fourCC(type), id);
	}
}
