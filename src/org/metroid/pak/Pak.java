package org.metroid.pak;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.logging.Logger;

import org.metroid.logging.Trace;
import org.metroid.pak.level.Level;
import org.metroid.pak.level.Room;
import org.metroid.pak.level.Tile;
import org.metroid.pak.level.Tileset;
import org.metroid.pak.txtr.ComparableTexture;
import org.metroid.pak.txtr.Texture;
import org.metroid.project.Project;
import org.metroid.xml.dom.Element;

public class Pak {
	private static final Logger log = Trace.create(Pak.class);

	public static final boolean OPTIMIZE_TEXTURES = false;

	public static final int VERSION = 0x00030005;

	private Set<Resource> resources;
	private Map<Integer, Resource> ids;
	private Map<String, Resource> names;

	private Project project;

	public Pak() {
		resources = new HashSet<>();
		ids = new HashMap<>();
		names = new HashMap<>();
	}

	public Pak(Project prj) {
		this();
		project = prj;
	}

	public Project getProject() {
		return project;
	}

	public boolean contains(Resource resource) {
		return resources.contains(resource);
	}

	public void add(String name, Resource resource) {
		add(resource);
		names.put(name, resource);
		resource.setExportName(true);
	}

	public void add(Resource resource) {
		if(contains(resource))
			throw new IllegalArgumentException("resource already in pak");
		int id = generateID();
		resource.setID(id);
		resource.setPak(this);
		ids.put(id, resource);
		resources.add(resource);
	}

	public void rename(int fourCC, String name, String newName) {
		Resource r = names.remove(name);
		if(r != null)
			names.put(newName, r);
	}

	public boolean exists(int fourCC, String name) {
		return names.containsKey(name);
	}

	private int generateID() {
		Random rng = new Random();
		int id = rng.nextInt();
		while(ids.containsKey(id))
			id = rng.nextInt();
		return id;
	}

	@SuppressWarnings("unchecked")
	public <T extends Resource> T get(int fourcc, int id) {
		for(Resource r : resources)
			if(r.getType() == fourcc && r.getID() == id)
				return (T) r;
		return null;
	}

	@SuppressWarnings("unchecked")
	public <T extends Resource> T get(int fourcc, String name) {
		Resource r = names.get(name);
		if(r != null && r.getType() == fourcc)
			return (T) r;
		return null;
	}

	public String[] list(int fourCC) {
		List<String> result = new ArrayList<String>();
		for(Map.Entry<String, Resource> resource : names.entrySet())
			if(resource.getValue().getType() == fourCC)
				result.add(resource.getKey());
		return result.toArray(new String[result.size()]);
	}

	public NamedResource[] list() {
		List<NamedResource> result = new ArrayList<NamedResource>();
		for(Map.Entry<String, Resource> resource : names.entrySet())
			result.add(new NamedResource(this, resource.getKey(), resource.getValue()));
		return result.toArray(new NamedResource[result.size()]);
	}

	// remove unreferenced, unnecessary elements
	private void cleanup() {
		ids.clear();
		resources.clear();
		NamedResource[] res = list();
		for(NamedResource r : res) {
			ids.put(r.getResource().getID(), r.getResource());
			resources.add(r.getResource());
		}
	}

	private List<Resource> getAllResources() {
		List<Resource> resources = new ArrayList<Resource>();
		for(Resource r : this.resources) {
			Resource[] deps = r.getAllDependencies();
			for(Resource dep : deps)
				resources.add(dep);
			resources.add(r);
		}
		return resources;
	}

	private void optimize() {
		List<Resource> resources = getAllResources();
		// optimize all resources
		// for(Resource resource : resources)
		// resource.prepareForWrite();
		// resources = getAllResources();
		if(OPTIMIZE_TEXTURES) {
			// minimize texture usage
			List<Tileset> tilesets = new ArrayList<Tileset>();
			List<Texture> textures = new ArrayList<Texture>();
			List<Room> rooms = new ArrayList<Room>();
			List<Sprite> sprites = new ArrayList<Sprite>();
			for(Resource r : resources) {
				if(r instanceof Tileset)
					tilesets.add((Tileset) r);
				else if(r instanceof Texture)
					textures.add((Texture) r);
				else if(r instanceof Room)
					rooms.add((Room) r);
				else if(r instanceof Sprite)
					sprites.add((Sprite) r);
			}
			// optimize texture usage
			Set<ComparableTexture> cmpTextures = new HashSet<ComparableTexture>();
			for(Texture tex : textures)
				cmpTextures.add(new ComparableTexture(tex));
			for(Room room : rooms) {
				for(Tile tile : room.getTileset().getTiles()) {
					for(ComparableTexture tex : cmpTextures) {
						if(tex.getTexture().equals(tile.getTexture())) {
							tile.setTexture(tex.getTexture());
							break;
						}
					}
				}
			}
			for(Sprite sprite : sprites) {
				for(ComparableTexture tex : cmpTextures) {
					if(tex.getTexture().equals(sprite.getTexture())) {
						sprite.setTexture(tex.getTexture());
						break;
					}
				}
			}
		}
		resources = getAllResources();
		// optimize all resources
		for(Resource resource : resources)
			resource.prepareForWrite();
		resources = getAllResources();
		int numTextures = 0;
		int numTilesets = 0;
		int numRooms = 0;
		int numTiles = 0;
		int numSprites = 0;
		for(Resource r : resources) {
			if(r instanceof Texture)
				numTextures++;
			else if(r instanceof Tileset) {
				numTilesets++;
				numTiles += ((Tileset) r).getTiles().length;
			} else if(r instanceof Room)
				numRooms++;
			else if(r instanceof Sprite)
				numSprites++;
		}
		System.out.printf("Pak::optimize summary: %d textures, %d tilesets, %d tiles, %d rooms, %d sprites\n",
				numTextures, numTilesets, numTiles, numRooms, numSprites);
	}

	public void read(InputStream in) throws IOException {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		byte[] buf = new byte[512];
		int n;
		while((n = in.read(buf)) != -1)
			bos.write(buf, 0, n);
		bos.flush();
		bos.close();
		read(bos.toByteArray());
	}

	public void write(OutputStream out) throws IOException {
		write(new BEOutputStream(out));
	}

	public void read(byte[] pak) throws IOException {
		class ResourceID {
			public final int fourCC;
			public final int id;

			public ResourceID(int fourCC, int id) {
				this.fourCC = fourCC;
				this.id = id;
			}
		}

		class File {
			public final boolean compressed;
			public final int fourCC;
			public final int id;
			public final int size;
			public final int offset;

			public File(boolean compressed, int fourCC, int id, int size, int offset) {
				this.compressed = compressed;
				this.fourCC = fourCC;
				this.id = id;
				this.size = size;
				this.offset = offset;
			}
		}

		Map<String, ResourceID> namedResources = new HashMap<String, ResourceID>();
		List<File> files = new ArrayList<File>();

		try(WordInputStream in = new BEInputStream(new ByteArrayInputStream(pak))) {
			int version = in.read32bit();
			if(version != VERSION)
				throw new IOException(String.format("Invalid file format version: 0x%08X", version));
			in.read32bit();
			int namedResourceCount = in.read32bit();

			for(int i = 0; i < namedResourceCount; i++) {
				int fourCC = in.read32bit();
				int id = in.read32bit();
				int length = in.read32bit();
				char[] name = new char[length];
				for(int j = 0; j < length; j++)
					name[j] = (char) in.read8bit();
				namedResources.put(new String(name), new ResourceID(fourCC, id));
			}

			int fileCount = in.read32bit();
			for(int i = 0; i < fileCount; i++) {
				int compressionFlag = in.read32bit();
				int fourCC = in.read32bit();
				int id = in.read32bit();
				int size = in.read32bit();
				int offset = in.read32bit();
				files.add(new File(compressionFlag != 0, fourCC, id, size, offset));
			}
		}

		for(File file : files) {
			if(file.compressed)
				continue;
			byte[] bytes = Arrays.copyOfRange(pak, file.offset, file.offset + file.size);
			Resource resource = Resource.parse(this, file.id, file.fourCC, bytes);
			if(!contains(resource)) {
				resources.add(resource);
				ids.put(resource.getID(), resource);
			}
		}
		for(Resource resource : resources)
			resource.delayedLoad();

		for(Map.Entry<String, ResourceID> resource : namedResources.entrySet())
			names.put(resource.getKey(), get(resource.getValue().fourCC, resource.getValue().id));
	}

	public void write(WordOutputStream out) throws IOException {
		cleanup();
		out.write32bit(VERSION);
		out.write32bit(0);
		out.write32bit(names.size());
		int totalOffset = 12;
		for(Map.Entry<String, Resource> name : names.entrySet()) {
			out.write32bit(name.getValue().getType()); // fourCC
			out.write32bit(name.getValue().getID());
			out.write32bit(name.getKey().length());
			out.write(name.getKey().getBytes());
			totalOffset += name.getKey().getBytes().length + 12;
		}

		optimize();
		List<Resource> resources = getAllResources();

		class ResourceData {
			public final int fourCC;
			public final int id;
			public final int size;
			public final int offset;

			public ResourceData(int fourCC, int id, int size, int offset) {
				this.fourCC = fourCC;
				this.id = id;
				this.size = size;
				this.offset = offset;
			}
		}

		ByteArrayOutputStream buf = new ByteArrayOutputStream();
		WordOutputStream words = new BEOutputStream(buf);

		List<ResourceData> pak = new ArrayList<ResourceData>();
		long start = totalOffset + resources.size() * 20 + 4;
		byte[] pad = new byte[(int) start];
		words.write(pad);
		words.pad32();
		long begin = words.tell();
		for(Resource r : resources) {
			long offset = words.tell();
			r.write(words);
			words.pad32(0xFF);
			long end = words.tell();
			int size = (int) (end - offset);
			ResourceData res = new ResourceData(r.getType(), r.getID(), size, (int) offset);
			pak.add(res);
		}
		words.flush();
		words.close();
		byte[] data = buf.toByteArray();

		out.write32bit(pak.size());
		for(ResourceData r : pak) {
			out.write32bit(0); // compression = off
			out.write32bit(r.fourCC);
			out.write32bit(r.id);
			out.write32bit(r.size);
			out.write32bit(r.offset);
		}
		out.pad32();
		out.write(data, (int) begin, (int) (data.length - begin));
	}

	public Element write() {
		Element root = new Element("pak");
		for(NamedResource namedres : list()) {
			String name = namedres.getName();
			Resource resource = namedres.getResource();
			log.info("Writing entry " + name);
			Element res = new Element("resource");
			res.addAttribute("name", name);
			res.addAttribute("export", resource.isExportName() ? "true" : "false");
			res.addChild(resource.write());
			root.addChild(res);
		}
		return root;
	}

	public void read(Project prj, Element root) throws IOException {
		if(!root.name.equals("pak")) {
			throw new IOException("not a pak");
		}
		project = prj;
		resources = new HashSet<>();
		ids = new HashMap<>();
		names = new HashMap<>();
		for(Element namedres : root.getElementsByTagName("resource")) {
			Element[] resources = namedres.getChildren();
			if(resources.length != 1) {
				throw new IllegalArgumentException("invalid resource: " + namedres.name);
			}
			String name = namedres.getAttribute("name");
			boolean isExport = namedres.getAttribute("export").equals("true");
			Element data = resources[0];
			Resource res = null;
			switch(data.name) {
			case "level":
				res = new Level();
				break;
			case "sprite":
				res = new Sprite();
				break;
			case "texture":
				res = null;
				add(name, prj.getTexture(name));
				break;
			default:
				throw new RuntimeException("unknown resource: " + data.name);
			}
			if(res != null) {
				res.setPak(this);
				res.read(data);
				res.setExportName(isExport);
				if(isExport)
					add(name, res);
			}
		}
	}
}
