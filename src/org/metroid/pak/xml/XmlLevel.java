package org.metroid.pak.xml;

import org.metroid.pak.level.Level;
import org.metroid.pak.level.Room;
import org.metroid.pak.level.RoomInfo;
import org.metroid.xml.dom.Element;

public class XmlLevel {
	public static Element write(Level level) {
		Element root = new Element("level");
		for(RoomInfo info : level.getRooms()) {
			Room room = info.getRoom();
			Element r = XmlRoom.write(room);
			r.addAttribute("pos-x", Integer.toString(info.getX()));
			r.addAttribute("pos-y", Integer.toString(info.getY()));
			root.addChild(r);
		}
		return root;
	}

	public static Level read(Element root) {
		if(!root.name.equals("level")) {
			throw new IllegalArgumentException("not a level");
		}
		Level level = new Level();
		for(Element info : root.getElementsByTagName("room")) {
			Room room = XmlRoom.read(info);
			int x = Integer.parseInt(info.getAttribute("pos-x"));
			int y = Integer.parseInt(info.getAttribute("pos-y"));
			level.addRoom(room, x, y);
		}
		return level;
	}
}
