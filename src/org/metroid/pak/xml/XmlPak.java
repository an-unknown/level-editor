package org.metroid.pak.xml;

import org.metroid.pak.NamedResource;
import org.metroid.pak.Pak;
import org.metroid.pak.Resource;
import org.metroid.pak.Sprite;
import org.metroid.pak.Utils;
import org.metroid.pak.level.Level;
import org.metroid.pak.txtr.Texture;
import org.metroid.project.Project;
import org.metroid.xml.dom.Element;

public class XmlPak {
	public static String getTextureName(Texture tex) {
		String name = tex.getName() == null
				? String.format("%08X.%s", tex.getID(), Utils.fourCC(Texture.FOURCC)) : tex.getName();
		return name;
	}

	public static Element write(Pak pak) {
		Element root = new Element("pak");
		for(NamedResource namedres : pak.list()) {
			String name = namedres.getName();
			Resource resource = namedres.getResource();
			Element res = new Element("resource");
			res.addAttribute("name", name);
			if(resource instanceof Level) {
				res.addChild(XmlLevel.write((Level) resource));
			} else if(resource instanceof Sprite) {
				res.addChild(XmlSprite.write((Sprite) resource));
			} else if(resource instanceof Texture) {
				res.addChild(XmlTexture.write((Texture) resource));
			}
			root.addChild(res);
		}
		return root;
	}

	public static Pak read(Element root) {
		if(!root.name.equals("pak")) {
			throw new IllegalArgumentException("not a pak");
		}
		Pak pak = new Pak();
		Project.setCurrentProject(pak.getProject());
		for(Element namedres : root.getElementsByTagName("resource")) {
			Element[] resources = namedres.getChildren();
			if(resources.length != 1) {
				throw new IllegalArgumentException("invalid resource: " + namedres.name);
			}
			String name = namedres.getAttribute("name");
			Element data = resources[0];
			Resource res = null;
			switch(data.name) {
			case "level":
				res = XmlLevel.read(data);
				break;
			case "sprite":
				res = XmlSprite.read(data);
				break;
			case "texture":
				res = XmlTexture.read(data);
				break;
			default:
				throw new RuntimeException("unknown resource: " + data.name);
			}
			pak.add(name, res);
		}
		return pak;
	}
}
