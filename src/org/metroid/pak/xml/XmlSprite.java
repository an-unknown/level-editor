package org.metroid.pak.xml;

import org.metroid.pak.Sprite;
import org.metroid.pak.txtr.Texture;
import org.metroid.project.Project;
import org.metroid.xml.dom.Element;

public class XmlSprite {
	public static Element write(Sprite sprite) {
		Element root = new Element("sprite");
		root.addAttribute("tex", XmlPak.getTextureName(sprite.getTexture()));
		root.addAttribute("width", Integer.toString(sprite.getWidth()));
		root.addAttribute("height", Integer.toString(sprite.getHeight()));
		root.addAttribute("frames", Integer.toString(sprite.getFrames()));
		root.addAttribute("padding-l", Integer.toString(sprite.getPaddingL()));
		root.addAttribute("padding-r", Integer.toString(sprite.getPaddingR()));
		root.addAttribute("padding-t", Integer.toString(sprite.getPaddingT()));
		root.addAttribute("padding-b", Integer.toString(sprite.getPaddingB()));
		root.addAttribute("loop", sprite.isLoop() ? "1" : "0");
		root.addAttribute("reverse", sprite.isReverse() ? "1" : "0");
		for(int i = 0; i < sprite.getFrameCount(); i++) {
			Element offset = new Element("offset");
			offset.addAttribute("x", Integer.toString(sprite.getOffsetX()[i]));
			offset.addAttribute("y", Integer.toString(sprite.getOffsetY()[i]));
			root.addChild(offset);
		}
		return root;
	}

	public static Sprite read(Element root) {
		if(!root.name.equals("sprite")) {
			throw new IllegalArgumentException("not a sprite");
		}
		Texture texture = Project.getCurrentProject().getTexture(root.getAttribute("tex"));
		int width = Integer.parseInt(root.getAttribute("width"));
		int height = Integer.parseInt(root.getAttribute("height"));
		int frames = Integer.parseInt(root.getAttribute("frames"));
		int paddingL = get(root, "padding-l", 0);
		int paddingR = get(root, "padding-r", 0);
		int paddingT = get(root, "padding-t", 0);
		int paddingB = get(root, "padding-b", 0);
		boolean loop = get(root, "loop", false);
		boolean reverse = get(root, "reverse", false);
		Element[] frameOffsets = root.getElementsByTagName("offset");
		if(frameOffsets.length == 0) {
			throw new IllegalArgumentException("invalid frame count");
		}
		int count = frameOffsets.length;
		int[] offsetX = new int[count];
		int[] offsetY = new int[count];
		for(int i = 0; i < count; i++) {
			Element offset = frameOffsets[i];
			offsetX[i] = Integer.parseInt(offset.getAttribute("x"));
			offsetY[i] = Integer.parseInt(offset.getAttribute("y"));
		}
		return new Sprite(texture, width, height, count, frames, offsetX, offsetY, paddingL, paddingR, paddingT,
				paddingB, loop, reverse);
	}

	private static int get(Element e, String name, int def) {
		String a = e.getAttribute(name);
		if(a == null) {
			return def;
		} else {
			return Integer.parseInt(a);
		}
	}

	private static boolean get(Element e, String name, boolean def) {
		String a = e.getAttribute(name);
		if(a == null) {
			return def;
		} else {
			return Integer.parseInt(a) != 0;
		}
	}
}
