package org.metroid.pak.xml;

import java.util.ArrayList;
import java.util.List;

import org.metroid.pak.level.Tile;
import org.metroid.pak.level.Tileset;
import org.metroid.pak.txtr.Texture;
import org.metroid.project.Project;
import org.metroid.xml.dom.Attribute;
import org.metroid.xml.dom.Element;
import org.metroid.xml.dom.Text;

public class XmlTileset {
	public static Element write(Tileset tileset) {
		Element root = new Element("tileset");
		root.addAttribute("tileWidth", Integer.toString(tileset.getTileWidth()));
		root.addAttribute("tileHeight", Integer.toString(tileset.getTileHeight()));
		Element emptyTile = new Element("empty-tile");
		emptyTile.addAttribute("tex", XmlPak.getTextureName(tileset.getEmptyTile().getTexture()));
		emptyTile.addAttribute("s", Short.toString(tileset.getEmptyTile().getS()));
		emptyTile.addAttribute("t", Short.toString(tileset.getEmptyTile().getT()));
		root.addChild(emptyTile);
		for(Tile tile : tileset.getTiles()) {
			Element t = new Element("tile");
			t.addAttribute(new Attribute("id", Short.toString(tile.getId())));
			t.addAttribute(new Attribute("tex", XmlPak.getTextureName(tile.getTexture())));
			t.addAttribute(new Attribute("s", Short.toString(tile.getS())));
			t.addAttribute(new Attribute("t", Short.toString(tile.getT())));
			root.addChild(t);
		}
		return root;
	}

	public static Tileset read(Element in) {
		List<Tile> tiles = new ArrayList<>();
		if(!in.name.equals("tileset")) {
			throw new IllegalArgumentException("not a tileset");
		}
		int tileWidth = Integer.parseInt(in.getAttribute("tileWidth"));
		int tileHeight = Integer.parseInt(in.getAttribute("tileHeight"));
		Tile empty = Project.getCurrentProject().getEmptyTile();
		for(Element e : in.getChildren()) {
			if(e.name.equals("tile")) {
				short s = Short.parseShort(e.getAttribute("s"));
				short t = Short.parseShort(e.getAttribute("t"));
				short id = Short.parseShort(e.getAttribute("id"));
				if(id != tiles.size()) {
					throw new RuntimeException("tile ID is wrong! (" + id + ")");
				}
				Texture tex = Project.getCurrentProject().getTexture(e.getAttribute("tex"));
				Tile tile = new Tile(s, t, tex);
				tiles.add(tile);
			} else if(e.name.equals("empty-tile")) {
				short s = Short.parseShort(e.getAttribute("s"));
				short t = Short.parseShort(e.getAttribute("t"));
				Texture tex = Project.getCurrentProject().getTexture(e.getAttribute("tex"));
				empty = new Tile(s, t, tex);
			} else if(e instanceof Text && ((Text) e).value.trim().length() > 0) {
				throw new IllegalArgumentException("unknown element: " + e.name);
			}
		}
		Tileset tileset = new Tileset(tileWidth, tileHeight, empty);
		for(Tile t : tiles) {
			tileset.addTile(t);
		}
		tileset.setTextures();
		return tileset;
	}
}
