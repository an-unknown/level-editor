package org.metroid.pak.xml;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import org.metroid.xml.dom.Element;
import org.metroid.xml.dom.XMLReader;

public class XmlFile {
	public static Element read(File in) throws IOException {
		try(FileInputStream fin = new FileInputStream(in)) {
			return XMLReader.read(fin);
		}
	}

	public static void write(File out, Element root) throws IOException {
		try(FileOutputStream fout = new FileOutputStream(out)) {
			String xml = root.toString();
			fout.write(xml.getBytes("utf-8"));
		}
	}
}
