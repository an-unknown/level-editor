package org.metroid.pak.xml;

import org.metroid.pak.txtr.Texture;
import org.metroid.project.Project;
import org.metroid.xml.dom.Element;

public class XmlTexture {
	public static Element write(Texture texture) {
		return new Element("texture", XmlPak.getTextureName(texture));
	}

	public static Texture read(Element texture) {
		if(!texture.name.equals("texture")) {
			throw new IllegalArgumentException("not a texture");
		}
		return Project.getCurrentProject().getTexture(texture.value);
	}
}
