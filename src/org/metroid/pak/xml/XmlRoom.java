package org.metroid.pak.xml;

import org.metroid.bts.TileBts;
import org.metroid.pak.Endianess;
import org.metroid.pak.level.Room;
import org.metroid.pak.level.Tileset;
import org.metroid.util.Base64;
import org.metroid.xml.dom.Element;

public class XmlRoom {
	private interface LayerReader {
		public int get(int x, int y);
	}

	private interface LayerWriter {
		public void set(int x, int y, int id);
	}

	public static Element write(Room room) {
		Element root = new Element("room");
		room.prepareForWrite();
		root.addAttribute("width", Integer.toString(room.getWidth()));
		root.addAttribute("height", Integer.toString(room.getHeight()));
		root.addChild(XmlTileset.write(room.getTileset()));
		root.addChild(xmlLayer("hidden", room.getWidth(), room.getHeight(),
				(x, y) -> room.getHidden(x, y).getId()));
		root.addChild(xmlLayer("fg1", room.getWidth(), room.getHeight(), (x, y) -> room.getFG1(x, y).getId()));
		root.addChild(xmlLayer("fg2", room.getWidth(), room.getHeight(), (x, y) -> room.getFG2(x, y).getId()));
		root.addChild(xmlLayer("bg1", room.getWidth(), room.getHeight(), (x, y) -> room.getBG1(x, y).getId()));
		root.addChild(xmlLayer("bg2", room.getWidth(), room.getHeight(), (x, y) -> room.getBG2(x, y).getId()));
		root.addChild(xmlBtsLayer("bts", room.getWidth(), room.getHeight(),
				(x, y) -> room.getBTS(x, y).getData()));
		return root;
	}

	public static Room read(Element root) {
		if(!root.name.equals("room")) {
			throw new IllegalArgumentException("not a room");
		}
		int width = Integer.parseInt(root.getAttribute("width"));
		int height = Integer.parseInt(root.getAttribute("height"));
		Element[] xmlTileset = root.getElementsByTagName("tileset");
		if(xmlTileset.length != 1) {
			throw new IllegalArgumentException("not exactly 1 tileset");
		}
		Tileset tileset = XmlTileset.read(xmlTileset[0]);
		Room room = new Room(tileset, width, height);
		for(Element layer : root.getElementsByTagName("layer")) {
			xmlReadLayer(room, layer);
		}
		return room;
	}

	private static Element xmlLayer(String name, int width, int height, LayerReader reader) {
		byte[] data = new byte[width * height * 2];
		for(int y = 0; y < height; y++) {
			for(int x = 0; x < width; x++) {
				int offset = (y * width + x) * 2;
				short id = (short) reader.get(x, y);
				Endianess.set16bit_BE(id, data, offset);
			}
		}
		Element layer = new Element("layer", Base64.encode(data));
		layer.addAttribute("name", name);
		return layer;
	}

	private static Element xmlBtsLayer(String name, int width, int height, LayerReader reader) {
		byte[] data = new byte[width * height * 4];
		for(int y = 0; y < height; y++) {
			for(int x = 0; x < width; x++) {
				int offset = (y * width + x) * 4;
				int id = reader.get(x, y);
				Endianess.set32bit_BE(id, data, offset);
			}
		}
		Element layer = new Element("layer", Base64.encode(data));
		layer.addAttribute("name", name);
		return layer;
	}

	private static void xmlReadLayer(Room room, Element layer) {
		if(!layer.name.equals("layer")) {
			throw new IllegalArgumentException("not a layer");
		}
		String name = layer.getAttribute("name");
		switch(name) {
		case "hidden":
			xmlReadLayer(room.getWidth(), room.getHeight(), layer.value,
					(x, y, id) -> room.setHidden(x, y, room.getTileset().getTile(id)));
			break;
		case "fg":
		case "fg1":
			xmlReadLayer(room.getWidth(), room.getHeight(), layer.value,
					(x, y, id) -> room.setFG1(x, y, room.getTileset().getTile(id)));
			break;
		case "fg2":
			xmlReadLayer(room.getWidth(), room.getHeight(), layer.value,
					(x, y, id) -> room.setFG2(x, y, room.getTileset().getTile(id)));
			break;
		case "bg1":
			xmlReadLayer(room.getWidth(), room.getHeight(), layer.value,
					(x, y, id) -> room.setBG1(x, y, room.getTileset().getTile(id)));
			break;
		case "bg2":
			xmlReadLayer(room.getWidth(), room.getHeight(), layer.value,
					(x, y, id) -> room.setBG2(x, y, room.getTileset().getTile(id)));
			break;
		case "bts":
			xmlReadBtsLayer(room.getWidth(), room.getHeight(), layer.value,
					(x, y, data) -> room.setBTS(x, y, new TileBts(data)));
			break;
		}
	}

	private static void xmlReadLayer(int width, int height, String data, LayerWriter writer) {
		byte[] binary = Base64.decode(data);
		for(int y = 0; y < height; y++) {
			for(int x = 0; x < width; x++) {
				int offset = (y * width + x) * 2;
				int id = Endianess.get16bit_BE(binary, offset);
				writer.set(x, y, id);
			}
		}
	}

	private static void xmlReadBtsLayer(int width, int height, String data, LayerWriter writer) {
		byte[] binary = Base64.decode(data);
		for(int y = 0; y < height; y++) {
			for(int x = 0; x < width; x++) {
				int offset = (y * width + x) * 4;
				int id = Endianess.get32bit_BE(binary, offset);
				writer.set(x, y, id);
			}
		}
	}
}
