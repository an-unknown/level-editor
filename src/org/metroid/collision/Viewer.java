package org.metroid.collision;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import javax.swing.Timer;

import org.metroid.editor.BtsImages;

public class Viewer extends JComponent {
	private static final long serialVersionUID = 983367900853992875L;
	public static final float SCALE = 32.0f;
	private static final float g = 20;

	private CollisionShape collisionShape;
	private Physics phy;

	private Collision data;
	private BufferedImage cache;

	private List<Line> lines;
	private List<Line> shapeLines;
	private List<Line> tangentLines;
	private List<Point> tiles;
	private List<Point> points;
	private List<Point> collisionPoints;

	private Point from;
	private Point to;

	private boolean paused = true;

	public Viewer() {
		phy = new Physics();
		phy.setVel(0, 0);
		phy.setAcc(0, -g);
		phy.setPos(0, 0);

		from = new Point(0, 0);
		to = new Point(0, 0);

		collisionShape = new CollisionShape(1, 1, -0.5f, 0);

		lines = new ArrayList<>();
		tangentLines = new ArrayList<>();
		shapeLines = new ArrayList<>();
		tiles = new ArrayList<>();
		points = new ArrayList<>();
		collisionPoints = new ArrayList<>();

		addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				mouse(e);
			}
		});

		addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseDragged(MouseEvent e) {
				mouse(e);
			}
		});

		addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				if(e.getKeyChar() == KeyEvent.VK_ESCAPE) {
					paused = !paused;
				}
			}
		});

		setFocusable(true);

		Timer timer = new Timer(10, (e) -> process());
		timer.start();
	}

	private void mouse(MouseEvent e) {
		float px = e.getX() / 32.0f;
		float py = data.getHeight() - (e.getY() / 32.0f);
		if(SwingUtilities.isLeftMouseButton(e)) {
			from = new Point(px, py);
			phy.setPos(px, py);
			phy.setVel(0, 0);
		} else {
			to = new Point(px, py);
		}
		display();
	}

	private long oldT = -1;

	private void display() {
		float vx = to.x - from.x;
		float vy = to.y - from.y;
		Physics p = new Physics(from.x, from.y, vx, vy, 0, 0);
		lines.clear();
		tangentLines.clear();
		shapeLines.clear();
		tiles.clear();
		points.clear();
		collisionPoints.clear();
		Point tangent = data.move(collisionShape, p, 1);
		if(tangent != null) {
			tangentLines.add(new Line(p.getPosX() - tangent.x / 2, p.getPosY() - tangent.y / 2,
					p.getPosX() + tangent.x / 2, p.getPosY() + tangent.y / 2));
		}
		repaint();
	}

	private void process() {
		if(oldT == -1) {
			oldT = System.currentTimeMillis();
		}
		long now = System.currentTimeMillis();
		long dt = now - oldT;
		oldT = now;
		if(!paused) {
			process(dt / 1000.0f);
			repaint();
		}
	}

	private void process(float dt) {
		lines.clear();
		tangentLines.clear();
		shapeLines.clear();
		tiles.clear();
		points.clear();
		collisionPoints.clear();
		Point tangent = data.move(collisionShape, phy, dt);
		if(tangent != null) {
			tangentLines.add(new Line(phy.getPosX() - tangent.x / 2, phy.getPosY() - tangent.y / 2,
					phy.getPosX() + tangent.x / 2, phy.getPosY() + tangent.y / 2));
		}
	}

	private int sz(float s) {
		return (int) (s * SCALE);
	}

	private int px(float x) {
		return sz(x);
	}

	private int py(float y) {
		return sz(data.getHeight() - y);
	}

	public void setCollision(int[] bts, int width, int height) {
		data = new Collision(bts, width, height);
		data.setDebugLine(lines::add);
		data.setDebugShapeLine(shapeLines::add);
		data.setDebugTile(tiles::add);
		data.setDebugPoint(points::add);
		data.setDebugCollisionPoint(collisionPoints::add);
		cache = new BufferedImage(sz(data.getWidth()), sz(data.getHeight()), BufferedImage.TYPE_INT_ARGB);
		Graphics g = cache.createGraphics();
		for(int y = 0; y < data.getHeight(); y++) {
			for(int x = 0; x < data.getWidth(); x++) {
				BtsImages.paint(g, px(x), py(y + 1), data.getTileBts(x, y));
			}
		}
		g.dispose();
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		if(cache != null) {
			g.drawImage(cache, 0, 0, this);
		}

		g.setColor(Color.BLACK);
		g.drawLine(0, py(0), getWidth(), py(0));

		g.setColor(Color.RED);
		g.drawRect(px(phy.getPosX() + collisionShape.getOffsetX()),
				py(phy.getPosY() + collisionShape.getOffsetY()) - sz(collisionShape.getHeight()),
				sz(collisionShape.getWidth()), sz(collisionShape.getHeight()));

		g.setColor(Color.BLUE);
		for(Line line : lines) {
			g.drawLine(px(line.x1), py(line.y1), px(line.x2), py(line.y2));
		}

		g.setColor(Color.BLUE);
		for(Point tile : tiles) {
			g.drawRect(px(tile.x), py(tile.y + 1), sz(1), sz(1));
		}

		g.setColor(Color.MAGENTA);
		for(Line line : shapeLines) {
			g.drawLine(px(line.x1), py(line.y1), px(line.x2), py(line.y2));
		}

		g.setColor(Color.CYAN);
		for(Point point : points) {
			g.fillOval(px(point.x) - 2, py(point.y) - 2, 4, 4);
		}

		g.setColor(Color.RED);
		for(Point point : collisionPoints) {
			g.fillOval(px(point.x) - 2, py(point.y) - 2, 4, 4);
		}

		g.setColor(Color.ORANGE);
		for(Line line : tangentLines) {
			g.drawLine(px(line.x1), py(line.y1), px(line.x2), py(line.y2));
		}
	}
}
