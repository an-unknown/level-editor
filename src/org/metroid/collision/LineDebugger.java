package org.metroid.collision;

import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.SwingUtilities;

@SuppressWarnings("serial")
public class LineDebugger extends Canvas {
	private Point a1 = new Point(0, 0);
	private Point a2 = new Point(0, 0);
	private Point b1 = new Point(0, 0);
	private Point b2 = new Point(0, 0);

	public static void main(String[] args) {
		LineDebugger d = new LineDebugger();
		Frame frame = new Frame("Line Intersection");
		frame.setSize(300, 300);
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				frame.dispose();
				System.exit(0);
			}
		});
		frame.setLayout(new BorderLayout());
		frame.add(BorderLayout.CENTER, d);
		frame.setVisible(true);
	}

	public LineDebugger() {
		addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				mouse(e);
			}
		});
		addMouseMotionListener(new MouseAdapter() {
			@Override
			public void mouseDragged(MouseEvent e) {
				mouse(e);
			}
		});
	}

	public static void drawShape(Graphics g, Line[] shape, Point offset) {
		for(Line edge : shape) {
			g.drawLine((int) (edge.x1 + offset.x), (int) (edge.y1 + offset.y), (int) (edge.x2 + offset.x),
					(int) (edge.y2 + offset.y));
		}
	}

	public static final int SIZE = 50;
	public static final Line[] BOX = { new Line(0, 0, 0, SIZE), new Line(0, SIZE, SIZE, SIZE),
			new Line(SIZE, SIZE, SIZE, 0), new Line(SIZE, 0, 0, 0) };

	public static final Line[] TRIANGLE = { new Line(0, 0, SIZE, 0), new Line(SIZE, 0, SIZE, SIZE),
			new Line(SIZE, SIZE, 0, 0) };

	@Override
	public void paint(Graphics g) {
		g.setColor(Color.BLUE);
		g.drawLine((int) a1.x, (int) a1.y, (int) a2.x, (int) a2.y);
		g.setColor(Color.GREEN);
		g.drawLine((int) b1.x, (int) b1.y, (int) b2.x, (int) b2.y);
		CollisionPoint c = Collision.collisionLine(a1.x, a1.y, a2.x, a2.y, b1.x, b1.y, b2.x, b2.y);
		if(c != null) {
			g.setColor(Color.RED);
			int x = (int) c.p.x - 2;
			int y = (int) c.p.y - 2;
			g.fillOval(x, y, 4, 4);
		}

		// box
		g.setColor(Color.CYAN);
		Point offset = new Point(200, 80);
		drawShape(g, BOX, offset);
		Line line = new Line(a1.x, a1.y, a2.x, a2.y);
		CollisionPoint cp = Collision.collisionShapeOffset(line, BOX, offset);
		if(cp != null) {
			g.setColor(Color.RED);
			int x = (int) cp.p.x - 2;
			int y = (int) cp.p.y - 2;
			g.fillOval(x, y, 4, 4);
		}

		// triangle
		g.setColor(Color.CYAN);
		offset = new Point(100, 200);
		drawShape(g, TRIANGLE, offset);
		line = new Line(a1.x, a1.y, a2.x, a2.y);
		cp = Collision.collisionShapeOffset(line, TRIANGLE, offset);
		if(cp != null) {
			g.setColor(Color.RED);
			int x = (int) cp.p.x - 2;
			int y = (int) cp.p.y - 2;
			g.fillOval(x, y, 4, 4);
		}
	}

	private void mouse(MouseEvent e) {
		if(e.isAltDown() || e.isControlDown()) {
			if(SwingUtilities.isLeftMouseButton(e)) {
				b1 = new Point(e.getX(), e.getY());
			} else {
				b2 = new Point(e.getX(), e.getY());
			}
		} else {
			if(SwingUtilities.isLeftMouseButton(e)) {
				a1 = new Point(e.getX(), e.getY());
			} else {
				a2 = new Point(e.getX(), e.getY());
			}
		}
		repaint();
	}
}
