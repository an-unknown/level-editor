package org.metroid.collision;

public class CollisionObject {
	private CollisionShape shape;
	private Physics phy;

	public CollisionShape getShape() {
		return shape;
	}

	public Physics getPhysics() {
		return phy;
	}
}
