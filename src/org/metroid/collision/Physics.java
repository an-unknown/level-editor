package org.metroid.collision;

public class Physics {
	private float posX;
	private float posY;
	private float velX;
	private float velY;
	private float accX;
	private float accY;

	public Physics() {
		this(0, 0, 0, 0, 0, 0);
	}

	public Physics(float posX, float posY, float velX, float velY, float accX, float accY) {
		setPos(posX, posY);
		setVel(velX, velY);
		setAcc(accX, accY);
	}

	@Override
	public Physics clone() {
		return new Physics(posX, posY, velX, velY, accX, accY);
	}

	public void setAccX(float accX) {
		this.accX = accX;
	}

	public void setAccY(float accY) {
		this.accY = accY;
	}

	public void setAcc(float accX, float accY) {
		this.accX = accX;
		this.accY = accY;
	}

	public void setVelX(float velX) {
		this.velX = velX;
	}

	public void setVelY(float velY) {
		this.velY = velY;
	}

	public void setVel(float velX, float velY) {
		this.velX = velX;
		this.velY = velY;
	}

	public void setPosX(float posX) {
		this.posX = posX;
	}

	public void setPosY(float posY) {
		this.posY = posY;
	}

	public void setPos(float posX, float posY) {
		this.posX = posX;
		this.posY = posY;
	}

	public void process(float dt) {
		velX += accX * dt;
		velY += accY * dt;
		posX += velX * dt;
		posY += velY * dt;
	}

	public float getPosX() {
		return posX;
	}

	public float getPosY() {
		return posY;
	}

	public float getVelX() {
		return velX;
	}

	public float getVelY() {
		return velY;
	}

	@Override
	public String toString() {
		return String.format("PHY[pos=(%s,%s),vel=(%s,%s),acc=(%s,%s)", posX, posY, velX, velY, accX, accY);
	}
}
