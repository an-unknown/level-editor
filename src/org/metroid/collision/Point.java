package org.metroid.collision;

public class Point {
	public final float x;
	public final float y;

	public Point(float x, float y) {
		this.x = x;
		this.y = y;
	}

	@Override
	public String toString() {
		return String.format("(%s,%s)", x, y);
	}
}
