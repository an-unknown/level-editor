package org.metroid.collision;

public class CollisionPoint {
	public final Point p;
	public final Point tangent;
	public final float k;

	public CollisionPoint(Point p, Point tangent, float k) {
		this.p = p;
		this.tangent = tangent;
		this.k = k;
	}

	public CollisionPoint(Point p, Line tangent, float k) {
		this(p, getTangent(tangent), k);
	}

	private static Point getTangent(Line t) {
		float dx = t.x2 - t.x1;
		float dy = t.y2 - t.y1;
		float length = (float) Math.sqrt(dx * dx + dy * dy);
		return new Point(dx / length, dy / length);
	}
}
