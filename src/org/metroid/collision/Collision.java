package org.metroid.collision;

import java.util.function.Consumer;

import org.metroid.bts.TileBts;
import org.metroid.bts.TileType;

public class Collision {
	private static final float EPS = 1e-4f;

	private int width;
	private int height;
	private int[] bts;

	private Consumer<Line> debugLine;
	private Consumer<Line> debugShapeLine;
	private Consumer<Point> debugTile;
	private Consumer<Point> debugPoint;
	private Consumer<Point> debugCollisionPoint;

	public Collision(int[] bts, int width, int height) {
		this.bts = bts;
		this.width = width;
		this.height = height;
	}

	private static boolean eq(float x, float y, float eps) {
		return Math.abs(x - y) <= eps;
	}

	private static int ftoi(float x) {
		return ftoi(x, EPS);
	}

	private static int ftoi(float x, float eps) {
		int ix = (int) x;
		if(eq(x, ix + 1, eps)) {
			return ix + 1;
		} else {
			return ix;
		}
	}

	private int getBTS(int x, int y) {
		return bts[(height - y - 1) * width + x];
	}

	public int getTileType(int x, int y) {
		return getTileBts(x, y).getTileType();
	}

	public int getTileSubtype(int x, int y) {
		return getTileBts(x, y).getTileSubtype();
	}

	public TileBts getTileBts(int x, int y) {
		return new TileBts(getBTS(x, y));
	}

	public boolean isAir(int x, int y) {
		return getTileType(x, y) == TileType.AIR;
	}

	public boolean isSolid(int x, int y) {
		return getTileType(x, y) == TileType.SOLID;
	}

	public boolean isSlope(int x, int y) {
		return getTileType(x, y) == TileType.SLOPE;
	}

	public int getSlope(int x, int y) {
		assert isSlope(x, y);
		return getTileSubtype(x, y);
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public Point move(CollisionShape shape, Physics phy, float dt) {
		Physics old = phy.clone();
		phy.process(dt);
		if(phy.getPosX() < 0) {
			phy.setPosX(0);
			phy.setVelX(0);
		}
		if(phy.getPosY() < 0) {
			phy.setPosY(0);
			phy.setVelY(0);
		}
		if(phy.getPosX() > getWidth()) {
			phy.setPosX(getWidth());
			phy.setVelX(0);
		}
		if(phy.getPosY() > getHeight()) {
			phy.setPosY(getHeight());
			phy.setVelY(0);
		}

		CollisionPoint p = move(shape, old.getPosX(), old.getPosY(), phy.getPosX(), phy.getPosY());
		if(p != null) {
			phy.setPosX(p.p.x);
			phy.setPosY(p.p.y);
			phy.setVelX(-phy.getVelX() / 10.0f);
			phy.setVelY(-phy.getVelY() / 10.0f);
			if(phy.getVelX() < 1) {
				phy.setVelX(0);
			}
			if(phy.getVelY() < 1) {
				phy.setVelY(0);
			}
			return p.tangent;
		}
		return null;
	}

	private void debugLine(float x1, float y1, float x2, float y2) {
		if(debugLine != null) {
			debugLine.accept(new Line(x1, y1, x2, y2));
		}
	}

	private void debugShapeLine(float x1, float y1, float x2, float y2) {
		if(debugShapeLine != null) {
			debugShapeLine.accept(new Line(x1, y1, x2, y2));
		}
	}

	private void debugShape(Line[] shape, Point offset) {
		for(Line line : shape) {
			debugShapeLine(line.x1 + offset.x, line.y1 + offset.y, line.x2 + offset.x, line.y2 + offset.y);
		}
	}

	private void debugTile(int x, int y) {
		if(debugTile != null) {
			debugTile.accept(new Point(x, y));
		}
	}

	private void debugPoint(float x, float y) {
		if(debugPoint != null) {
			debugPoint.accept(new Point(x, y));
		}
	}

	private void debugCollisionPoint(float x, float y) {
		if(debugCollisionPoint != null) {
			debugCollisionPoint.accept(new Point(x, y));
		}
	}

	public void setDebugLine(Consumer<Line> debugLine) {
		this.debugLine = debugLine;
	}

	public void setDebugShapeLine(Consumer<Line> debugShapeLine) {
		this.debugShapeLine = debugShapeLine;
	}

	public void setDebugTile(Consumer<Point> debugTile) {
		this.debugTile = debugTile;
	}

	public void setDebugPoint(Consumer<Point> debugPoint) {
		this.debugPoint = debugPoint;
	}

	public void setDebugCollisionPoint(Consumer<Point> debugCollisionPoint) {
		this.debugCollisionPoint = debugCollisionPoint;
	}

	public CollisionPoint move(CollisionShape shape, float startX, float startY, float endX, float endY) {
		debugLine(startX, startY, endX, endY);

		return rasterize(startX, startY, endX, endY);
	}

	private CollisionPoint rasterize(float sx, float sy, float ex, float ey) {
		int isx = ftoi(sx);
		int isy = ftoi(sy);
		int iex = ftoi(ex);
		int iey = ftoi(ey);
		if(isx == iex && isy == iey) {
			debugTile(isx, isy);
			CollisionPoint p = collisionTile(isx, isy, sx, sy, ex, ey);
			if(p != null) {
				debugCollisionPoint(p.p.x, p.p.y);
			}
			return p;
		} else if(isx == iex) {
			return rasterizeH(isx, isy, iey, sx, sy, ex, ey);
		} else if(isy == iey) {
			return rasterizeV(isx, isy, iex, sx, sy, ex, ey);
		} else if(sx < ex) {
			return rasterizeLR(sx, sy, ex, ey);
		} else if(sx > ex) {
			return rasterizeRL(sx, sy, ex, ey);
		} else {
			throw new AssertionError();
		}
	}

	private CollisionPoint rasterizeH(int x, int sy, int ey, float csx, float csy, float cex, float cey) {
		int dir = sy < ey ? 1 : -1;
		for(int y = sy; y != ey + dir; y += dir) {
			debugTile(x, y);
			CollisionPoint p = collisionTile(x, y, csx, csy, cex, cey);
			if(p != null) {
				debugCollisionPoint(p.p.x, p.p.y);
				return p;
			}
		}
		return null;
	}

	private CollisionPoint rasterizeV(int sx, int y, int ex, float csx, float csy, float cex, float cey) {
		int dir = sx < ex ? 1 : -1;
		for(int x = sx; x != ex + dir; x += dir) {
			debugTile(x, y);
			CollisionPoint p = collisionTile(x, y, csx, csy, cex, cey);
			if(p != null) {
				debugCollisionPoint(p.p.x, p.p.y);
				return p;
			}
		}
		return null;
	}

	private CollisionPoint rasterizeLR(float sx, float sy, float ex, float ey) {
		float dx = ex - sx;
		float dy = ey - sy;
		float k = dy / dx;
		if(Math.abs(k) <= 1) {
			return rasterizeLR1(sx, sy, ex, ey);
		} else if(k > 1) {
			return rasterizeLR2(sx, sy, ex, ey);
		} else {
			return rasterizeLR3(sx, sy, ex, ey);
		}
	}

	private CollisionPoint rasterizeRL(float sx, float sy, float ex, float ey) {
		float dx = sx - ex;
		float dy = ey - sy;
		float k = dy / dx;
		if(Math.abs(k) <= 1) {
			return rasterizeRL1(sx, sy, ex, ey);
		} else if(k > 1) {
			return rasterizeLR2(sx, sy, ex, ey);
		} else {
			return rasterizeLR3(sx, sy, ex, ey);
		}
	}

	private CollisionPoint rasterizeLR1(float sx, float sy, float ex, float ey) {
		float x = sx;
		float y = sy;
		float dx = ex - sx;
		float dy = ey - sy;
		float k = dy / dx;
		float d = x - ftoi(x);
		x += 1.0 - d;
		y += (1.0 - d) * k;
		CollisionPoint p1 = null;
		CollisionPoint p;
		if(ftoi(y) != ftoi(sy)) {
			debugTile(ftoi(x) - 1, ftoi(y));
			p1 = collisionTile(ftoi(x) - 1, ftoi(y), sx, sy, ex, ey);
		}
		debugTile(ftoi(sx), ftoi(sy));
		p = collisionTile(ftoi(sx), ftoi(sy), sx, sy, ex, ey);
		if(p != null) {
			if(p1 != null && p1.k < p.k) {
				debugCollisionPoint(p1.p.x, p1.p.y);
				return p1;
			} else {
				debugCollisionPoint(p.p.x, p.p.y);
				return p;
			}
		} else if(p1 != null) {
			debugCollisionPoint(p1.p.x, p1.p.y);
			return p1;
		}
		int lastY = ftoi(y);
		for(int i = ftoi(sx); i < ftoi(ex); i++) {
			debugPoint(x, y);
			debugTile(ftoi(x), ftoi(y));
			p1 = collisionTile(ftoi(x), ftoi(y), sx, sy, ex, ey);
			if(lastY != ftoi(y)) {
				debugTile(ftoi(x) - 1, ftoi(y));
				p = collisionTile(ftoi(x) - 1, ftoi(y), sx, sy, ex, ey);
				if(p != null) {
					if(p1 != null && p1.k < p.k) {
						debugCollisionPoint(p1.p.x, p1.p.y);
						return p1;
					} else {
						debugCollisionPoint(p.p.x, p.p.y);
						return p;
					}
				} else if(p1 != null) {
					debugCollisionPoint(p1.p.x, p1.p.y);
					return p1;
				}
			} else if(p1 != null) {
				debugCollisionPoint(p1.p.x, p1.p.y);
				return p1;
			}
			lastY = ftoi(y);
			x += 1;
			y += k;
		}
		if(ey - ftoi(ey) > 0) {
			debugTile(ftoi(ex), ftoi(ey));
			p = collisionTile(ftoi(ex), ftoi(ey), sx, sy, ex, ey);
			if(p != null) {
				debugCollisionPoint(p.p.x, p.p.y);
				return p;
			}
		}
		return null;
	}

	private CollisionPoint rasterizeLR2(float sx, float sy, float ex, float ey) {
		float x = sx;
		float y = sy;
		float dx = ex - sx;
		float dy = ey - sy;
		float k = dx / dy;
		float d = y - ftoi(y);
		x += (1.0 - d) * k;
		y += 1.0 - d;
		CollisionPoint p1 = null;
		CollisionPoint p;
		if(ftoi(x) != ftoi(sx)) {
			debugTile(ftoi(x), ftoi(y) - 1);
			p1 = collisionTile(ftoi(x), ftoi(y) - 1, sx, sy, ex, ey);
		}
		debugTile(ftoi(sx), ftoi(sy));
		p = collisionTile(ftoi(sx), ftoi(sy), sx, sy, ex, ey);
		if(p != null) {
			if(p1 != null && p1.k < p.k) {
				debugCollisionPoint(p1.p.x, p1.p.y);
				return p1;
			} else {
				debugCollisionPoint(p.p.x, p.p.y);
				return p;
			}
		} else if(p1 != null) {
			debugCollisionPoint(p1.p.x, p1.p.y);
			return p1;
		}
		int lastX = ftoi(x);
		for(int i = ftoi(sy); i < ftoi(ey); i++) {
			debugPoint(x, y);
			debugTile(ftoi(x), ftoi(y));
			p1 = collisionTile(ftoi(x), ftoi(y), sx, sy, ex, ey);
			if(lastX != ftoi(x)) {
				debugTile(ftoi(x), ftoi(y) - 1);
				p = collisionTile(ftoi(x), ftoi(y) - 1, sx, sy, ex, ey);
				if(p != null) {
					if(p1 != null && p1.k < p.k) {
						debugCollisionPoint(p1.p.x, p1.p.y);
						return p1;
					} else {
						debugCollisionPoint(p.p.x, p.p.y);
						return p;
					}
				} else if(p1 != null) {
					debugCollisionPoint(p1.p.x, p1.p.y);
					return p1;
				}
			} else if(p1 != null) {
				debugCollisionPoint(p1.p.x, p1.p.y);
				return p1;
			}
			lastX = ftoi(x);
			x += k;
			y += 1;
		}
		if(ex - ftoi(ex) > 0) {
			debugTile(ftoi(ex), ftoi(ey));
			p = collisionTile(ftoi(ex), ftoi(ey), sx, sy, ex, ey);
			if(p != null) {
				debugCollisionPoint(p.p.x, p.p.y);
				return p;
			}
		}
		return null;
	}

	private CollisionPoint rasterizeLR3(float sx, float sy, float ex, float ey) {
		float x = sx;
		float y = sy;
		float dx = ex - sx;
		float dy = ey - sy;
		float k = dx / dy;
		float d = y - ftoi(y);
		x -= d * k;
		y -= d;
		CollisionPoint p1 = null;
		CollisionPoint p;
		if(ftoi(x) != ftoi(sx)) {
			debugTile(ftoi(x), ftoi(y) - 1);
			p1 = collisionTile(ftoi(x), ftoi(y) - 1, sx, sy, ex, ey);
		}
		debugTile(ftoi(sx), ftoi(sy));
		p = collisionTile(ftoi(sx), ftoi(sy), sx, sy, ex, ey);
		if(p != null) {
			if(p1 != null && p1.k < p.k) {
				debugCollisionPoint(p1.p.x, p1.p.y);
				return p1;
			} else {
				debugCollisionPoint(p.p.x, p.p.y);
				return p;
			}
		} else if(p1 != null) {
			debugCollisionPoint(p1.p.x, p1.p.y);
			return p1;
		}
		int off = dx > 0 ? -1 : 1;
		int lastX = ftoi(x);
		for(int i = ftoi(sy); i > ftoi(ey); i--) {
			debugPoint(x, y);
			debugTile(ftoi(x), ftoi(y));
			p = p1;
			p1 = collisionTile(ftoi(x), ftoi(y), sx, sy, ex, ey);
			if(p != null) {
				if(p1 != null && p1.k < p.k) {
					debugCollisionPoint(p1.p.x, p1.p.y);
					return p1;
				} else {
					debugCollisionPoint(p.p.x, p.p.y);
					return p;
				}
			}
			if(lastX != ftoi(x)) {
				debugTile(ftoi(x) + off, ftoi(y));
				p = collisionTile(ftoi(x) + off, ftoi(y), sx, sy, ex, ey);
				if(p != null) {
					if(p1 != null && p1.k < p.k) {
						debugCollisionPoint(p1.p.x, p1.p.y);
						return p1;
					} else {
						debugCollisionPoint(p.p.x, p.p.y);
						return p;
					}
				} else if(p1 != null) {
					debugCollisionPoint(p1.p.x, p1.p.y);
					return p1;
				}
			} else if(p1 != null) {
				debugCollisionPoint(p1.p.x, p1.p.y);
				return p1;
			}
			lastX = ftoi(x);
			x -= k;
			y -= 1;
		}
		debugTile(ftoi(ex), ftoi(ey));
		p1 = collisionTile(ftoi(ex), ftoi(ey), sx, sy, ex, ey);
		if(lastX != ftoi(x)) {
			debugTile(ftoi(x) + off, ftoi(ey));
			p = collisionTile(ftoi(x) + off, ftoi(ey), sx, sy, ex, ey);
			if(p != null) {
				if(p1 != null && p1.k < p.k) {
					debugCollisionPoint(p1.p.x, p1.p.y);
					return p1;
				} else {
					debugCollisionPoint(p.p.x, p.p.y);
					return p;
				}
			} else if(p1 != null) {
				debugCollisionPoint(p1.p.x, p1.p.y);
				return p1;
			}
		} else if(p1 != null) {
			debugCollisionPoint(p1.p.x, p1.p.y);
			return p1;
		}
		return null;
	}

	private CollisionPoint rasterizeRL1(float sx, float sy, float ex, float ey) {
		float x = sx;
		float y = sy;
		float dx = ex - sx;
		float dy = ey - sy;
		float k = dy / dx;
		float d = x - ftoi(x);
		x -= d;
		y -= d * k;
		CollisionPoint p1 = null;
		CollisionPoint p;
		if(ftoi(y) != ftoi(sy)) {
			debugTile(ftoi(x) - 1, ftoi(y));
			p1 = collisionTile(ftoi(x) - 1, ftoi(y), sx, sy, ex, ey);
		}
		debugTile(ftoi(sx), ftoi(sy));
		p = collisionTile(ftoi(sx), ftoi(sy), sx, sy, ex, ey);
		if(p != null) {
			if(p1 != null && p1.k < p.k) {
				debugCollisionPoint(p1.p.x, p1.p.y);
				return p1;
			} else {
				debugCollisionPoint(p.p.x, p.p.y);
				return p;
			}
		} else if(p1 != null) {
			debugCollisionPoint(p1.p.x, p1.p.y);
			return p1;
		}
		int lastY = ftoi(y);
		int off = dy > 0 ? -1 : 1;
		for(int i = ftoi(sx); i > ftoi(ex); i--) {
			debugPoint(x, y);
			debugTile(ftoi(x), ftoi(y));
			p = p1;
			p1 = collisionTile(ftoi(x), ftoi(y), sx, sy, ex, ey);
			if(p != null) {
				if(p1 != null && p1.k < p.k) {
					debugCollisionPoint(p1.p.x, p1.p.y);
					return p1;
				} else {
					debugCollisionPoint(p.p.x, p.p.y);
					return p;
				}
			}
			if(lastY != ftoi(y)) {
				debugTile(ftoi(x), ftoi(y) + off);
				p = collisionTile(ftoi(x), ftoi(y) + off, sx, sy, ex, ey);
				if(p != null) {
					if(p1 != null && p1.k < p.k) {
						debugCollisionPoint(p1.p.x, p1.p.y);
						return p1;
					} else {
						debugCollisionPoint(p.p.x, p.p.y);
						return p;
					}
				} else if(p1 != null) {
					debugCollisionPoint(p1.p.x, p1.p.y);
					return p1;
				}
			} else if(p1 != null) {
				debugCollisionPoint(p1.p.x, p1.p.y);
				return p1;
			}
			lastY = ftoi(y);
			x -= 1;
			y -= k;
		}
		debugTile(ftoi(ex), ftoi(ey));
		p1 = collisionTile(ftoi(ex), ftoi(ey), sx, sy, ex, ey);
		if(lastY != ftoi(y)) {
			debugTile(ftoi(x), ftoi(y) + off);
			p = collisionTile(ftoi(x), ftoi(y) + off, sx, sy, ex, ey);
			if(p != null) {
				if(p1 != null && p1.k < p.k) {
					debugCollisionPoint(p1.p.x, p1.p.y);
					return p1;
				} else {
					debugCollisionPoint(p.p.x, p.p.y);
					return p;
				}
			} else if(p1 != null) {
				debugCollisionPoint(p1.p.x, p1.p.y);
				return p1;
			}
		} else if(p1 != null) {
			debugCollisionPoint(p1.p.x, p1.p.y);
			return p1;
		}
		return null;
	}

	// @formatter:off
	/* math:
	 * 
	 * g1(t) = a1 + t * (a2 - a1)
	 * g2(t) = b1 + t * (b2 - b1)
	 * 
	 * p = a1 + k1 * (a2 - a1)
	 * p = b1 + k2 * (b2 - b1)
	 * 
	 * a1 + k1 * (a2 - a1) = b1 + k2 * (b2 - b1)
	 * 
	 * a1 + k1 * (a2 - a1) - b1 - k2 * (b2 - b1) = 0
	 * k1 * (a2 - a1) - k2 * (b2 - b1) = b1 - a1
	 * 
	 * solve:
	 * k1 * (ax2 - ax1) - k2 * (bx2 - bx1) = bx1 - ax1
	 * k1 * (ay2 - ay1) - k2 * (by2 - by1) = by1 - ay1
	 * 
	 * substitute:
	 * adx = ax2 - ax1
	 * ady = ay2 - ay1
	 * bdx = bx2 - bx1
	 * bdy = by2 - by1
	 * dx = bx1 - ax1
	 * dy = by1 - ay1
	 * 
	 * k1 * adx - k2 * bdx = dx
	 * k1 * ady - k2 * bdy = dy
	 * 
	 * M0 = [[adx, bdx],
	 *       [ady, bdy]]
	 * M1 = [[ dx, bdx],
	 *       [ dy, bdy]]
	 * M2 = [[adx,  dx],
	 *       [ady,  dy]]
	 * 
	 * k1 = det(M1) / det(M0)
	 * 
	 */
	// @formatter:on
	public static CollisionPoint collisionLine(float ax1, float ay1, float ax2, float ay2, float bx1, float by1,
			float bx2, float by2) {
		float adx = ax2 - ax1;
		float ady = ay2 - ay1;
		float bdx = bx2 - bx1;
		float bdy = by2 - by1;
		float dx = bx1 - ax1;
		float dy = by1 - ay1;

		float dm0 = adx * bdy - ady * bdx;
		float dm1 = dx * bdy - dy * bdx;
		float dm2 = adx * dy - ady * dx;

		if(dm0 == 0) {
			return null;
		}

		float k1 = dm1 / dm0;
		float k2 = -dm2 / dm0;

		if(k1 < 0 || k1 > 1) {
			return null;
		}

		if(k2 < 0 || k2 > 1) {
			return null;
		}

		return new CollisionPoint(new Point(ax1 + k1 * adx, ay1 + k1 * ady), new Line(bx1, by1, bx2, by2), k1);
	}

	public static CollisionPoint collisionShape(Line line, Line[] shape) {
		CollisionPoint c = null;
		for(Line edge : shape) {
			CollisionPoint p = collisionLine(line.x1, line.y1, line.x2, line.y2, edge.x1, edge.y1, edge.x2,
					edge.y2);
			if(p != null) {
				if(c == null) {
					c = p;
				} else if(c.k > p.k) {
					c = p;
				}
			}
		}
		if(c == null) {
			return null;
		} else {
			return c;
		}
	}

	public static CollisionPoint collisionShapeOffset(Line line, Line[] shape, Point shapePosition) {
		Line l = new Line(line.x1 - shapePosition.x, line.y1 - shapePosition.y, line.x2 - shapePosition.x,
				line.y2 - shapePosition.y);
		CollisionPoint c = collisionShape(l, shape);
		if(c != null) {
			return new CollisionPoint(new Point(c.p.x + shapePosition.x, c.p.y + shapePosition.y),
					c.tangent, c.k);
		} else {
			return null;
		}
	}

	public static final Line[] BOX = { new Line(0, 0, 0, 1), new Line(0, 1, 1, 1), new Line(1, 1, 1, 0),
			new Line(1, 0, 0, 0) };

	// @formatter:off
	private static final float TR1 = (float) (1.0 / 3.0);
	private static final float TR2 = (float) (2.0 / 3.0);
	public static final Line[] SLOPES[] = {
		/* tri1 */ { new Line(0, 0, 1, 0), new Line(1, 0, 1, 1), new Line(1, 1, 0, 0) },
		// 1/2 triangles
		/* tri2 */ { new Line(0, 0, 1, 0), new Line(1, 0, 1, .5f), new Line(1, .5f, 0, 0) },
		/* tri3 */ { new Line(0, 0, 1, 0), new Line(1, 0, 1, 1), new Line(1, 1, 0, .5f), new Line(0, .5f, 0, 0) },
		/* box1 */ { new Line(0, 0, 0, .5f), new Line(0, .5f, 1, .5f), new Line(1, .5f, 1, 0), new Line(1, 0, 0, 0) },
		/* box2 */ { new Line(0, 0, .5f, 0), new Line(.5f, 0, .5f, 1), new Line(.5f, 1, 0, 1), new Line(0, 1, 0, 0) },
		// 1/3 triangles
		/* tri4 */ { new Line(0, 0, 1, 0), new Line(1, 0, 1, TR1), new Line(1, TR1, 0, 0) },
		/* tri5 */ { new Line(0, 0, 1, 0), new Line(1, 0, 1, TR2), new Line(1, TR2, 0, TR1), new Line(0, TR1, 0, 0) },
		/* tri6 */ { new Line(0, 0, 1, 0), new Line(1, 0, 1, 1), new Line(1, 1, 0, TR2), new Line(0, TR2, 0, 0) },
		// 1/2 triangles
		/* tri7 */ { new Line(.5f, 0, 1, 0), new Line(1, 0, 1, 1), new Line(1, 1, .5f, 0) },
		/* tri8 */ { new Line(0, 0, 1, 0), new Line(1, 0, 1, 1), new Line(1, 1, .5f, 1), new Line(.5f, 1, 0, 0) },
		/* box3 */ { new Line(.5f, 0, .5f, 1), new Line(.5f, 1, 1, 1), new Line(1, 1, 1, 0), new Line(1, 0, .5f, 0) },
		/* pol1 */ { new Line(0, 0, 1, 0), new Line(1, 0, 1, 1), new Line(1, 1, TR1, TR1), new Line(TR1, TR1, 0, 0) },
		/* tri9 */ { new Line(0, 0, 1, 0), new Line(1, 0, .5f, 1), new Line(.5f, 1, 0, 0) },
		/* triA */ { new Line(0, 0, 1, 0), new Line(1, 0, .5f, .5f), new Line(.5f, .5f, 0, 0) },
		// 1/3 triangles
		/* triB */ { new Line(TR2, 0, 1, 0), new Line(1, 0, 1, 1), new Line(1, 1, TR2, 0) },
		/* triC */ { new Line(TR1, 0, 1, 0), new Line(1, 0, 1, 1), new Line(1, 1, TR2, 1), new Line(TR2, 1, TR1, 0) },
		/* triD */ { new Line(0, 0, 1, 0), new Line(1, 0, 1, 1), new Line(1, 1, TR1, 1), new Line(TR1, 1, 0, 0) },
		// stair triangles
		/* str1 */ { new Line(.5f, 0, 1, 0), new Line(1, 0, 1, .5f), new Line(1, .5f, .5f, 0) },
		/* str2 */ { new Line(0, 0, 1, 0), new Line(1, 0, 1, 1), new Line(1, 1, .5f, 1), new Line(.5f, 1, 0, .5f), new Line(0, .5f, 0, 0) },
		/* tri8 */ { new Line(0, 0, 1, 0), new Line(1, 0, 1, 1), new Line(1, 1, .5f, 1), new Line(.5f, 1, 0, 0) },
		/* box4 */ { },
		/* cir1 */ { },
		/* cir2 */ { },
		/* cir3 */ { },
		/* cir4 */ { },
		/* cir5 */ { },
		/* cir6 */ { },
		// mirrored
		/* tri1 */ { new Line(0, 0, 1, 0), new Line(1, 0, 0, 1), new Line(0, 1, 0, 0) },
		/* tri2 */ { new Line(0, 0, 1, 0), new Line(1, 0, 0, .5f), new Line(0, .5f, 0, 0) },
		/* tri3 */ { new Line(0, 0, 1, 0), new Line(1, 0, 1, .5f), new Line(1, .5f, 0, 1), new Line(0, 1, 0, 0) }
	};
	// @formatter:on

	private CollisionPoint collisionTile(int x, int y, float sx, float sy, float ex, float ey) {
		if(x < 0 || y < 0) {
			return null;
		}
		if(x >= width || y >= height) {
			return null;
		}

		if(isAir(x, y)) {
			return null;
		}
		if(isSolid(x, y)) {
			// check collision
			Line l = new Line(sx, sy, ex, ey);
			return collisionShapeOffset(l, BOX, new Point(x, y));
		} else if(isSlope(x, y)) {
			int slopeid = getSlope(x, y);
			if(slopeid < SLOPES.length) {
				debugShape(SLOPES[slopeid], new Point(x, y));
				Line l = new Line(sx, sy, ex, ey);
				return collisionShapeOffset(l, SLOPES[slopeid], new Point(x, y));
			}
		}
		return null;
	}
}
