package org.metroid.collision;

public class Line {
	public final float x1;
	public final float y1;
	public final float x2;
	public final float y2;

	public Line(float x1, float y1, float x2, float y2) {
		this.x1 = x1;
		this.y1 = y1;
		this.x2 = x2;
		this.y2 = y2;
	}

	public Point a() {
		return new Point(x1, y1);
	}

	public Point b() {
		return new Point(x2, y2);
	}

	@Override
	public String toString() {
		return String.format("[%s, %s]", a(), b());
	}
}
