package org.metroid.collision;

public class CollisionShape {
	private float width;
	private float height;
	private float offX;
	private float offY;

	public CollisionShape(float width, float height, float offX, float offY) {
		this.width = width;
		this.height = height;
		this.offX = offX;
		this.offY = offY;
	}

	public float getWidth() {
		return width;
	}

	public float getHeight() {
		return height;
	}

	public float getOffsetX() {
		return offX;
	}

	public float getOffsetY() {
		return offY;
	}
}
