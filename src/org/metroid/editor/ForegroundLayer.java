package org.metroid.editor;

import org.metroid.pak.level.Room;
import org.metroid.pak.level.Tile;

public class ForegroundLayer extends RoomLayer {
	private boolean fg1;

	public ForegroundLayer(Room room, boolean fg1) {
		super(room);
		this.fg1 = fg1;
	}

	@Override
	public Tile get(int x, int y) {
		return fg1 ? room.getFG1(x, y) : room.getFG2(x, y);
	}

	@Override
	public void set(int x, int y, Tile tile) {
		if(fg1)
			room.setFG1(x, y, tile);
		else
			room.setFG2(x, y, tile);
	}
}
