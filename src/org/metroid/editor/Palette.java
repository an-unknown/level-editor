package org.metroid.editor;

import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.util.HashSet;
import java.util.Set;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import org.metroid.Listener;
import org.metroid.pak.level.Tile;
import org.metroid.pak.level.Tileset;

public class Palette extends Canvas {
	private static final long serialVersionUID = 1L;

	private Tile[][] tiles;

	private Tileset tileset;

	private BufferedImage cache;
	private boolean invalidate;

	private TileSelection selection;

	private int hoverTileX;
	private int hoverTileY;

	private Set<Listener> listeners;

	public Palette(Tileset tileset) {
		this.tileset = tileset;
		tiles = new Tile[64][64];
		invalidate = true;
		listeners = new HashSet<Listener>();
		selection = null;
		MouseHandler h = new MouseHandler();
		addMouseListener(h);
		addMouseMotionListener(h);
	}

	public void clear() {
		for(int y = 0; y < tiles.length; y++)
			for(int x = 0; x < tiles[y].length; x++)
				tiles[y][x] = tileset.getEmptyTile();
		repaint();
	}

	private void prepare() {
		cache = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_4BYTE_ABGR);
		invalidate = false;
		Graphics g = cache.getGraphics();
		g.setColor(Color.GRAY);
		g.fillRect(0, 0, cache.getWidth(), cache.getHeight());
		for(int y = 0; y < tiles.length; y++) {
			for(int x = 0; x < tiles[y].length; x++) {
				Tile tile = tiles[y][x];
				if(tile == null)
					continue;
				BufferedImage img = tileset.get(tile);
				g.drawImage(img, x * tileset.getTileWidth(), y * tileset.getTileHeight(), null);
			}
		}

		int w = tileset.getTileWidth();
		int h = tileset.getTileHeight();
		if(selection != null) {
			Point p = getCoordinates(selection.x, selection.y);
			g.setColor(Color.LIGHT_GRAY);
			g.drawRect(p.x, p.y, w * selection.width - 1, h * selection.height - 1);
		}
		if(hoverTileX != -1) {
			Point p = getCoordinates(hoverTileX, hoverTileY);
			g.setColor(Color.LIGHT_GRAY);
			g.drawRect(p.x, p.y, w - 1, h - 1);
		}

		g.dispose();
	}

	public JFrame createWindow() {
		JFrame frame = new JFrame("Palette");
		frame.setAlwaysOnTop(true);
		frame.setSize(300, 300);
		frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		frame.getContentPane().setLayout(new BorderLayout());
		frame.getContentPane().add(BorderLayout.CENTER, this);
		return frame;
	}

	private Point getCoordinates(int x, int y) {
		return new Point(x * tileset.getTileWidth(), y * tileset.getTileHeight());
	}

	public void setTileset(Tileset tileset) {
		this.tileset = tileset;
		for(int y = 0; y < getTilesY(); y++) {
			for(int x = 0; x < getTilesX(); x++) {
				tiles[y][x] = null;
			}
		}
	}

	@Override
	public void update(Graphics g) {
		paint(g);
	}

	@Override
	public void paint(Graphics g) {
		if(cache == null || invalidate || cache.getWidth() != getWidth() || cache.getHeight() != getHeight())
			prepare();
		g.drawImage(cache, 0, 0, null);
	}

	public int getTilesX() {
		return tiles[0].length;
	}

	public int getTilesY() {
		return tiles.length;
	}

	public Tile getTile(int x, int y) {
		return tiles[y][x];
	}

	public void setTile(int x, int y, Tile tile) {
		tiles[y][x] = tile;
		invalidate = true;
		repaint();
	}

	public void setSelection(TileSelection s) {
		clear();
		setSelection(0, 0, s);
	}

	public void setSelection(int x, int y, TileSelection s) {
		for(int py = 0; py < s.height; py++) {
			for(int px = 0; px < s.width; px++) {
				tiles[y + py][x + px] = s.get(px, py);
			}
		}
		invalidate = true;
		repaint();
	}

	public TileSelection getSelection() {
		return selection;
	}

	public void addSelectionListener(Listener l) {
		listeners.add(l);
	}

	protected void fireSelectionChangedEvent() {
		// ignore exceptions here
		for(Listener l : listeners)
			l.call();
	}

	private TileSelection getSelection(int x, int y, int w, int h) {
		Tile[][] tiles = new Tile[h][w];
		for(int py = 0; py < h; py++)
			for(int px = 0; px < w; px++)
				tiles[py][px] = getTile(x + px, y + py);
		return new TileSelection(x, y, tiles);
	}

	private class MouseHandler extends MouseAdapter {
		private int startX;
		private int startY;

		@Override
		public void mouseMoved(MouseEvent e) {
			startX = e.getX() / tileset.getTileWidth();
			startY = e.getY() / tileset.getTileHeight();
			updatePoint(e.getX(), e.getY());
		}

		@Override
		public void mouseDragged(MouseEvent e) {
			if(SwingUtilities.isLeftMouseButton(e))
				updateSelection(e.getX(), e.getY());
			else
				updatePoint(e.getX(), e.getY());
		}

		@Override
		public void mousePressed(MouseEvent e) {
			hoverTileX = hoverTileY = -1;
			startX = e.getX() / tileset.getTileWidth();
			startY = e.getY() / tileset.getTileHeight();
			if(SwingUtilities.isLeftMouseButton(e))
				updateSelection(e.getX(), e.getY());
			else
				updatePoint(e.getX(), e.getY());
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			if(SwingUtilities.isLeftMouseButton(e))
				updateSelection(e.getX(), e.getY());
			updatePoint(e.getX(), e.getY());
		}

		@Override
		public void mouseEntered(MouseEvent e) {
			updatePoint(e.getX(), e.getY());
		}

		@Override
		public void mouseExited(MouseEvent e) {
			hoverTileX = hoverTileY = -1;
		}

		private synchronized void updateSelection(int x, int y) {
			int endX = x / tileset.getTileWidth();
			int endY = y / tileset.getTileHeight();
			int sx = startX > endX ? endX : startX;
			int sy = startY > endY ? endY : startY;
			int ex = startX > endX ? startX : endX;
			int ey = startY > endY ? startY : endY;
			if(sx >= getTilesX() || sy >= getTilesY()) {
				selection = null;
				return;
			}
			if(sx < 0)
				sx = 0;
			if(sy < 0)
				sy = 0;
			if(ex >= getTilesX())
				ex = getTilesX() - 1;
			if(ey >= getTilesY())
				ey = getTilesY() - 1;
			int w = ex - sx + 1;
			int h = ey - sy + 1;
			TileSelection newSelection = getSelection(sx, sy, w, h);
			if(newSelection.equals(selection)) // order is important
				return;
			selection = newSelection;
			fireSelectionChangedEvent();
			invalidate = true;
			repaint();
		}

		private synchronized void updatePoint(int x, int y) {
			int px = x / tileset.getTileWidth();
			int py = y / tileset.getTileHeight();
			if(px >= getTilesX() || py >= getTilesY() || px < 0 || py < 0) {
				hoverTileX = hoverTileY = -1;
				repaint();
				return;
			}
			if(px >= getTilesX())
				px = getTilesX() - 1;
			if(py >= getTilesY())
				py = getTilesY() - 1;
			hoverTileX = px;
			hoverTileY = py;
			invalidate = true;
			repaint();
		}
	}
}
