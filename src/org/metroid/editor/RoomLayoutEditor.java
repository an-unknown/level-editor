package org.metroid.editor;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashSet;
import java.util.Set;

import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.SwingUtilities;

import org.metroid.CoordinateListener;
import org.metroid.Listener;
import org.metroid.bts.TileBts;
import org.metroid.pak.level.Room;
import org.metroid.pak.level.Tile;
import org.metroid.pak.level.Tileset;

public class RoomLayoutEditor extends JPanel {
	private static final long serialVersionUID = -8831193258885349100L;

	public static final int HIDDEN = 0;
	public static final int FG1 = 1;
	public static final int FG2 = 2;
	public static final int BG1 = 3;
	public static final int BG2 = 4;
	public static final int BTS = 5;
	public static final int BGSCROLL = 6;

	public static final int SCREEN_WIDTH = 20;
	public static final int SCREEN_HEIGHT = 15;

	private BtsLayer bts;
	private Layer[] layers;
	private boolean[] show;

	private int currentLayer;

	private GX gx;
	private int hoverX;
	private int hoverY;
	private JScrollBar scrollerX;
	private JScrollBar scrollerY;

	private BufferedImage cache;
	private boolean invalidate;

	private TileSelection selectedTiles;
	private int selectedBts;

	private Room room;

	private Set<CoordinateListener> coordinateListeners;
	private Set<Listener> changeListeners;

	private TileSelection selection;

	private Set<Listener> listeners;

	public RoomLayoutEditor(Room room) {
		this.room = room;
		listeners = new HashSet<Listener>();

		layers = new Layer[] { new HiddenLayer(room), new ForegroundLayer(room, true),
				new ForegroundLayer(room, false), new BackgroundLayer(room, true),
				new BackgroundLayer(room, false), bts = new BtsLayer(room) };
		show = new boolean[layers.length];
		for(int i = 0; i < show.length; i++)
			show[i] = true;

		gx = new GX();
		scrollerX = new JScrollBar(JScrollBar.HORIZONTAL);
		scrollerY = new JScrollBar(JScrollBar.VERTICAL);
		setCurrentLayer(FG1);
		scrollerX.setValues(0, getScrollWidth(), 0, getRoomWidth());
		scrollerY.setValues(0, getScrollHeight(), 0, getRoomHeight());

		scrollerX.addAdjustmentListener((e) -> gx.repaint());
		scrollerY.addAdjustmentListener((e) -> gx.repaint());

		setLayout(new BorderLayout());
		add(BorderLayout.CENTER, gx);
		add(BorderLayout.SOUTH, scrollerX);
		add(BorderLayout.EAST, scrollerY);

		MouseHandler h = new MouseHandler();
		gx.addMouseListener(h);
		gx.addMouseMotionListener(h);
		gx.addComponentListener(new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent e) {
				scrollerX.setVisibleAmount(getScrollWidth());
				scrollerY.setVisibleAmount(getScrollHeight());
				repaint();
			}
		});

		coordinateListeners = new HashSet<CoordinateListener>();
		changeListeners = new HashSet<Listener>();

		prepare();
	}

	public void setRoom(Room room) {
		invalidate = true;
		this.room = room;

		layers = new Layer[] { new HiddenLayer(room), new ForegroundLayer(room, true),
				new ForegroundLayer(room, false), new BackgroundLayer(room, true),
				new BackgroundLayer(room, false), bts = new BtsLayer(room) };
		scrollerX.setValue(0);
		scrollerY.setValue(0);
		scrollerX.setMaximum(getRoomWidth());
		scrollerY.setMaximum(getRoomHeight());
		setCurrentLayer(currentLayer);
		gx.repaint();
		revalidate();
	}

	public int getRoomWidth() {
		return room.getWidth();
	}

	public int getRoomHeight() {
		return room.getHeight();
	}

	public Tileset getTileset() {
		return room.getTileset();
	}

	private int getScrollWidth() {
		int v = gx.getWidth() / getTileset().getTileWidth();
		int r = gx.getWidth() % getTileset().getTileWidth();
		if(r == 0 || r == getTileset().getTileWidth() - 1)
			v++;
		if(v < 0)
			v = 0;
		return v;
	}

	private int getScrollHeight() {
		int v = gx.getHeight() / getTileset().getTileHeight();
		int r = gx.getHeight() % getTileset().getTileHeight();
		if(r == 0 || r == getTileset().getTileHeight() - 1)
			v++;
		if(v < 0)
			v = 0;
		return v;
	}

	public void setSelectedTiles(TileSelection tiles) {
		selectedTiles = tiles;
	}

	public void setSelectedBts(TileBts bts) {
		if(bts != null)
			selectedBts = bts.getData();
	}

	public TileSelection getSelection() {
		return selection;
	}

	public void setCurrentLayer(int layer) {
		currentLayer = layer;
		scrollerX.setVisibleAmount(gx.getWidth() / getTileset().getTileWidth());
		scrollerY.setVisibleAmount(gx.getHeight() / getTileset().getTileHeight());
	}

	public int getCurrentLayerID() {
		return currentLayer;
	}

	public Layer getLayer(int id) {
		return layers[id];
	}

	public Layer getCurrentLayer() {
		return getLayer(getCurrentLayerID());
	}

	public void setLayerVisibility(int layer, boolean visible) {
		invalidate = true;
		show[layer] = visible;
		gx.repaint();
	}

	public void fill() {
		if(selectedTiles == null)
			return;
		getCurrentLayer().fill(selectedTiles);
		invalidate = true;
		gx.repaint();
	}

	public void put(int x, int y) {
		if(getCurrentLayer() == bts) {
			bts.setBTS(x, y, selectedBts);
			update(x, y, 1, 1);
			gx.repaint();
			fireChanged();
			return;
		}
		if(selectedTiles == null)
			return;
		getCurrentLayer().put(x, y, selectedTiles);
		update(x, y, selectedTiles.width, selectedTiles.height);
		gx.repaint();
		fireChanged();
	}

	public Tile get(int x, int y) {
		if(getCurrentLayer() == bts) {
			return null;
		}
		return getCurrentLayer().get(x, y);
	}

	public int getBTS(int x, int y) {
		return bts.getBTS(x, y).getData();
	}

	public void setBTS(int x, int y, int bts) {
		this.bts.setBTS(x, y, bts);
		update(x, y, 1, 1);
		gx.repaint();
		fireChanged();
	}

	public void resizeRoom(int width, int height) {
		if(width == getRoomWidth() && height == getRoomHeight())
			return;
		room.resize(width, height);
		scrollerX.setValue(0);
		scrollerY.setValue(0);
		scrollerX.setMaximum(width);
		scrollerY.setMaximum(height);
		invalidate = true;
		gx.repaint();
		fireChanged();
	}

	private void prepare() {
		int w = getTileset().getTileWidth();
		int h = getTileset().getTileHeight();
		if(cache == null || cache.getWidth() != getRoomWidth() * w || cache.getHeight() != getRoomHeight() * h)
			cache = new BufferedImage(getRoomWidth() * w, getRoomHeight() * h, BufferedImage.TYPE_INT_ARGB);
		Graphics g = cache.getGraphics();
		g.setColor(Color.GRAY);
		g.fillRect(0, 0, cache.getWidth(), cache.getHeight());
		g.dispose();

		update(0, 0, getRoomWidth(), getRoomHeight());

		invalidate = false;
	}

	private void update(int x, int y, int width, int height) {
		int w = getTileset().getTileWidth();
		int h = getTileset().getTileHeight();

		Graphics g = cache.getGraphics();
		g.setColor(Color.GRAY);
		g.fillRect(x * w, y * h, width * w, height * h);
		for(int i = layers.length - 1; i >= 0; i--)
			if(i != BTS && show[i])
				layers[i].paint(g, x, y, width, height);
		if(show[BTS])
			layers[BTS].paint(g, x, y, width, height);

		g.setColor(Color.RED);
		for(int px = 0; px < cache.getWidth(); px += SCREEN_WIDTH) {
			g.drawLine(px * w, 0, px * w, cache.getHeight());
			g.drawLine(px * w - 1, 0, px * w - 1, cache.getHeight());
		}
		for(int py = 0; py < cache.getHeight(); py += SCREEN_HEIGHT) {
			g.drawLine(0, py * h, cache.getWidth(), py * h);
			g.drawLine(0, py * h - 1, cache.getWidth(), py * h - 1);
		}

		g.dispose();
	}

	public void addSelectionListener(Listener l) {
		listeners.add(l);
	}

	protected void fireSelectionChangedEvent() {
		// ignore exceptions here
		for(Listener l : listeners)
			l.call();
	}

	private TileSelection getSelection(int x, int y, int w, int h) {
		Tile[][] tiles = new Tile[h][w];
		for(int py = 0; py < h; py++)
			for(int px = 0; px < w; px++)
				tiles[py][px] = get(x + px, y + py);
		return new TileSelection(x, y, tiles);
	}

	private class GX extends JComponent {
		private static final long serialVersionUID = -2294361522766593503L;

		public GX() {
			setBackground(Color.LIGHT_GRAY);
		}

		@Override
		public void update(Graphics g) {
			paint(g);
		}

		@Override
		public void paint(Graphics g) {
			super.paint(g);
			if(invalidate) {
				// TODO: fix text output
				String text = "Updating...";
				FontMetrics metrics = g.getFontMetrics(g.getFont());
				int w = metrics.stringWidth(text);
				int h = metrics.getHeight();
				g.setColor(Color.LIGHT_GRAY);
				// g.fillRect(0, 0, getWidth(), getHeight());
				g.fillRect(0, 16 - h, w + 8, h + 8);
				g.setColor(Color.RED);
				g.drawString(text, 4, 16);
				prepare();
				g.setColor(Color.LIGHT_GRAY);
				g.fillRect(0, 0, getWidth(), getHeight());
			}
			int w = getTileset().getTileWidth();
			int h = getTileset().getTileHeight();
			int ox = scrollerX.getValue() * w;
			int oy = scrollerY.getValue() * h;
			g.translate(-ox, -oy);
			g.drawImage(cache, 0, 0, null);
			if(hoverX != -1 && hoverY != -1) {
				g.setColor(Color.LIGHT_GRAY);
				g.drawRect(hoverX * w, hoverY * h, w - 1, h - 1);
			}

			if(selection != null) {
				Point p = new Point(selection.x * w, selection.y * h);
				g.setColor(Color.LIGHT_GRAY);
				g.drawRect(p.x, p.y, w * selection.width - 1, h * selection.height - 1);
			}
		}
	}

	private static int mod(int x, int y) {
		if(x < 0) {
			int r = -x % y;
			if(r == 0)
				return r;
			return y - r;
		}
		return x % y;
	}

	public void addCoordinateListener(CoordinateListener l) {
		coordinateListeners.add(l);
	}

	protected void fireCoordinatesChanged(int x, int y) {
		for(CoordinateListener l : coordinateListeners)
			l.changed(x, y);
	}

	public void addChangeListener(Listener l) {
		changeListeners.add(l);
	}

	protected void fireChanged() {
		for(Listener l : changeListeners)
			l.call();
	}

	private class MouseHandler extends MouseAdapter {
		private Point start;

		@Override
		public void mouseMoved(MouseEvent e) {
			updatePoint(e.getX(), e.getY());
		}

		@Override
		public void mouseDragged(MouseEvent e) {
			if(SwingUtilities.isLeftMouseButton(e)) {
				updatePoint(e.getX(), e.getY());
				Point p = getPoint(e.getX(), e.getY());
				if(start == null)
					start = p;
				if(p == null)
					return;
				if(getCurrentLayerID() == BTS) {
					put(p.x, p.y);
					return;
				}
				int dx = p.x - start.x;
				int dy = p.y - start.y;
				int w = 1;
				int h = 1;
				if(selectedTiles != null) {
					w = selectedTiles.width;
					h = selectedTiles.height;
				}
				int rx = mod(dx, w);
				int ry = mod(dy, h);
				int x = p.x - rx;
				int y = p.y - ry;
				if(x >= 0 && y >= 0)
					put(x, y);
			} else
				updateSelection(e.getX(), e.getY());
		}

		@Override
		public void mousePressed(MouseEvent e) {
			updatePoint(e.getX(), e.getY());
			start = getPoint(e.getX(), e.getY());
			if(start == null || start.x >= getRoomWidth() || start.y >= getRoomHeight())
				return;
			if(SwingUtilities.isLeftMouseButton(e))
				put(start.x, start.y);
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			if(!SwingUtilities.isLeftMouseButton(e))
				updateSelection(e.getX(), e.getY());
			updatePoint(e.getX(), e.getY());
		}

		@Override
		public void mouseEntered(MouseEvent e) {
			updatePoint(e.getX(), e.getY());
		}

		@Override
		public void mouseExited(MouseEvent e) {
			hoverX = hoverY = -1;
		}

		private synchronized void updateSelection(int x, int y) {
			int endX = getPoint(x, y).x;
			int endY = getPoint(x, y).y;
			int sx = start.x > endX ? endX : start.x;
			int sy = start.y > endY ? endY : start.y;
			int ex = start.x > endX ? start.x : endX;
			int ey = start.y > endY ? start.y : endY;
			if(sx >= getRoomWidth() || sy >= getRoomHeight()) {
				selection = null;
				return;
			}
			if(sx < 0)
				sx = 0;
			if(sy < 0)
				sy = 0;
			if(ex >= getRoomWidth())
				ex = getRoomWidth() - 1;
			if(ey >= getRoomHeight())
				ey = getRoomHeight() - 1;
			int w = ex - sx + 1;
			int h = ey - sy + 1;
			TileSelection newSelection = getSelection(sx, sy, w, h);
			if(newSelection.equals(selection)) // order is important
				return;
			selection = newSelection;
			fireSelectionChangedEvent();
			repaint();
		}

		private Point getPoint(int x, int y) {
			int px = x / getTileset().getTileWidth();
			int py = y / getTileset().getTileHeight();
			if(px >= getRoomWidth() || py >= getRoomHeight() || px < 0 || py < 0) {
				return null;
			}
			x = px + scrollerX.getValue();
			y = py + scrollerY.getValue();
			return new Point(x, y);
		}

		private synchronized void updatePoint(int x, int y) {
			Point p = getPoint(x, y);
			if(p == null)
				hoverX = hoverY = -1;
			else {
				hoverX = p.x;
				hoverY = p.y;
				fireCoordinatesChanged(hoverX, hoverY);
			}
			gx.repaint();
		}
	}

	public void read(InputStream in) throws IOException {
		room.read(in);
		resizeRoom(room.getWidth(), room.getHeight());
		invalidate = true;
		repaint();
	}

	public void write(OutputStream out) throws IOException {
		room.write(out);
	}
}
