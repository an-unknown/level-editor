package org.metroid.editor;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;

import org.metroid.pak.level.Level;
import org.metroid.pak.level.RoomInfo;

public class LevelRenderer {
	public static BufferedImage render(Level level) {
		return render(level, 1.0);
	}

	public static BufferedImage render(Level level, double size) {
		int tileSize = (int) Math.round(level.getProject().getTileWidth() * size);
		Rectangle area = level.getArea();
		int width = area.width * tileSize;
		int height = area.height * tileSize;
		BufferedImage out = new BufferedImage(width, height, BufferedImage.TYPE_4BYTE_ABGR);
		Graphics g = out.createGraphics();
		RoomInfo[] rooms = level.getRooms();
		for(RoomInfo room : rooms) {
			int x = (room.getX() - area.x) * tileSize;
			int y = (room.getY() - area.y) * tileSize;
			BufferedImage img = RoomRenderer.render(room.getRoom(), size);
			g.drawImage(img, x, y, null);
		}
		g.dispose();
		return out;
	}
}
