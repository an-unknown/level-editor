package org.metroid.editor;

import org.metroid.pak.level.Room;
import org.metroid.pak.level.Tile;

public class BackgroundLayer extends RoomLayer {
	private boolean bg1;

	public BackgroundLayer(Room room, boolean bg1) {
		super(room);
		this.bg1 = bg1;
	}

	@Override
	public Tile get(int x, int y) {
		return bg1 ? room.getBG1(x, y) : room.getBG2(x, y);
	}

	@Override
	public void set(int x, int y, Tile tile) {
		if(bg1)
			room.setBG1(x, y, tile);
		else
			room.setBG2(x, y, tile);
	}
}
