package org.metroid.editor;

import java.awt.Graphics;

import org.metroid.pak.level.Tile;

public interface Layer {
	public void resize(int width, int height);

	public void paint(Graphics g);

	public void paint(Graphics g, int x, int y, int w, int h);

	public int getWidth();

	public int getHeight();

	public void fill(TileSelection tiles);

	public void put(int x, int y, TileSelection tiles);

	public Tile get(int x, int y);
}
