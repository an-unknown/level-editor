package org.metroid.editor;

import java.awt.Graphics;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;

import org.metroid.pak.level.Room;
import org.metroid.pak.level.Tileset;

public class RoomRenderer {
	public static void paint(Graphics g, Room room, int x, int y) {
		Layer[] layers = { new BackgroundLayer(room, false), new BackgroundLayer(room, true),
				new ForegroundLayer(room, false), new ForegroundLayer(room, true),
				new HiddenLayer(room) };
		g.translate(-x, -y);
		for(Layer layer : layers) {
			layer.paint(g);
		}
		g.translate(x, y);
	}

	public static BufferedImage render(Room room) {
		Tileset tileset = room.getTileset();
		int width = room.getWidth() * tileset.getTileWidth();
		int height = room.getHeight() * tileset.getTileHeight();
		BufferedImage img = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		Graphics g = img.getGraphics();
		paint(g, room, 0, 0);
		g.dispose();
		return img;
	}

	public static BufferedImage render(Room room, double size) {
		BufferedImage before = render(room);
		int w = (int) (before.getWidth() * size);
		int h = (int) (before.getHeight() * size);
		BufferedImage after = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
		AffineTransform at = new AffineTransform();
		at.scale(size, size);
		AffineTransformOp scaleOp = new AffineTransformOp(at, AffineTransformOp.TYPE_BILINEAR);
		after = scaleOp.filter(before, after);
		return after;
	}
}
