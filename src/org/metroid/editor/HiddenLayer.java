package org.metroid.editor;

import org.metroid.bts.TileBts;
import org.metroid.bts.TileType;
import org.metroid.pak.level.Room;
import org.metroid.pak.level.Tile;

public class HiddenLayer extends RoomLayer {
	private boolean oldBehavior = false;

	public HiddenLayer(Room room) {
		super(room);
	}

	@Override
	public Tile get(int x, int y) {
		Tile tile = room.getHidden(x, y);
		if(tile != room.getTileset().getEmptyTile())
			return tile;
		TileBts bts = room.getBTS(x, y);
		if(bts.getTileType() == TileType.HIDDEN_PASSAGE) {
			return bts.getTile();
		}
		return room.getTileset().getEmptyTile();
	}

	@Override
	public void set(int x, int y, Tile tile) {
		if(oldBehavior) {
			TileBts bts = room.getBTS(x, y);
			bts.setTileType(TileType.HIDDEN_PASSAGE);
			bts.setTile(tile);
		} else {
			room.setHidden(x, y, tile);
		}
	}
}
