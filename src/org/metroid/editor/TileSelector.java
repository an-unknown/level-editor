package org.metroid.editor;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.util.HashSet;
import java.util.Set;

import javax.swing.JComponent;

import org.metroid.Listener;
import org.metroid.pak.level.Tile;
import org.metroid.pak.level.Tileset;
import org.metroid.pak.txtr.Texture;
import org.metroid.project.Project;

public class TileSelector extends JComponent {
	private static final long serialVersionUID = 4446791654740084442L;

	private Tileset tileset;
	private Project project;
	private TileSelection selection;

	private int hoverTileX;
	private int hoverTileY;

	private Set<Listener> listeners;

	private String currentTexture;

	private BufferedImage cache;

	public TileSelector(Tileset tileset, Project project) {
		this.tileset = tileset;
		this.project = project;
		listeners = new HashSet<Listener>();
		selection = null;
		setCurrentTexture(null);
		MouseHandler h = new MouseHandler();
		addMouseListener(h);
		addMouseMotionListener(h);
		update();
	}

	public void setProject(Project project) {
		this.project = project;
		setCurrentTexture(null);
		update();
	}

	public void update() {
		if(currentTexture == null) {
			String[] textures = project.getTextureNames();
			if(textures.length > 0) {
				setCurrentTexture(textures[0]);
				return;
			}
		}
	}

	public void setTileset(Tileset tileset) {
		this.tileset = tileset;
		update();
	}

	public void setCurrentTexture(String name) {
		this.currentTexture = name;
		if(currentTexture == null || getTexture() == null) {
			setMinimumSize(new Dimension(tileset.getTileWidth(), tileset.getTileHeight()));
			setPreferredSize(getMinimumSize());
			invalidate();
			repaint();
			return;
		}
		setMinimumSize(new Dimension(tileset.getTileWidth(), tileset.getTileHeight()));
		setPreferredSize(new Dimension(getTexture().getWidth(), getTexture().getHeight()));
		invalidate();
		selection = null;
		repaint();
	}

	public Texture getTexture() {
		return project.getTexture(currentTexture);
	}

	public TileSelection getSelection() {
		return selection;
	}

	public void addSelectionListener(Listener l) {
		listeners.add(l);
	}

	protected void fireSelectionChangedEvent() {
		// ignore exceptions here
		for(Listener l : listeners)
			l.call();
	}

	private Point getTextureCoordinates(int x, int y) {
		return new Point(x * tileset.getTileWidth(), y * tileset.getTileHeight());
	}

	private int getTilesX() {
		if(getTexture() == null)
			return 0;
		return getTexture().getTexture().getWidth() / tileset.getTileWidth();
	}

	private int getTilesY() {
		if(getTexture() == null)
			return 0;
		return getTexture().getTexture().getHeight() / tileset.getTileHeight();
	}

	private TileSelection getSelection(int x, int y, int w, int h) {
		if(getTexture() == null)
			return null;
		Tile[][] tiles = new Tile[h][w];
		for(int py = 0; py < h; py++)
			for(int px = 0; px < w; px++)
				tiles[py][px] = tileset.createTile((short) (x + px), (short) (y + py), getTexture());
		return new TileSelection(x, y, tiles);
	}

	private void prepare() {
		cache = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_ARGB);

		Graphics g = cache.getGraphics();
		g.setColor(Color.GRAY);
		g.fillRect(0, 0, cache.getWidth(), cache.getHeight());

		if(getTexture() != null)
			g.drawImage(getTexture().getTexture(), 0, 0, null);

		int w = tileset.getTileWidth();
		int h = tileset.getTileHeight();
		if(selection != null) {
			Point p = getTextureCoordinates(selection.x, selection.y);
			g.setColor(Color.LIGHT_GRAY);
			g.drawRect(p.x, p.y, w * selection.width - 1, h * selection.height - 1);
		}
		if(hoverTileX != -1) {
			Point p = getTextureCoordinates(hoverTileX, hoverTileY);
			g.setColor(Color.LIGHT_GRAY);
			g.drawRect(p.x, p.y, w - 1, h - 1);
		}

		g.dispose();
	}

	@Override
	public void update(Graphics g) {
		paint(g);
	}

	@Override
	public void paint(Graphics g) {
		prepare();
		g.drawImage(cache, 0, 0, null);

		/*
		 * // fill unused space int width =
		 * getTexture().getTexture().getWidth(); int height =
		 * getTexture().getTexture().getHeight(); int _w = getWidth() -
		 * width; int _h = getHeight() - height; g.setColor(Color.GRAY);
		 * if(_w > 0) g.fillRect(width, 0, _w, getHeight()); if(_h > 0)
		 * g.fillRect(0, height, getWidth(), _h);
		 */
	}

	private class MouseHandler extends MouseAdapter {
		private int startX;
		private int startY;

		@Override
		public void mouseMoved(MouseEvent e) {
			startX = e.getX() / tileset.getTileWidth();
			startY = e.getY() / tileset.getTileHeight();
			updatePoint(e.getX(), e.getY());
		}

		@Override
		public void mouseDragged(MouseEvent e) {
			updateSelection(e.getX(), e.getY());
		}

		@Override
		public void mousePressed(MouseEvent e) {
			hoverTileX = hoverTileY = -1;
			startX = e.getX() / tileset.getTileWidth();
			startY = e.getY() / tileset.getTileHeight();
			updateSelection(e.getX(), e.getY());
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			updateSelection(e.getX(), e.getY());
			updatePoint(e.getX(), e.getY());
		}

		@Override
		public void mouseEntered(MouseEvent e) {
			updatePoint(e.getX(), e.getY());
		}

		@Override
		public void mouseExited(MouseEvent e) {
			hoverTileX = hoverTileY = -1;
		}

		private synchronized void updateSelection(int x, int y) {
			int endX = x / tileset.getTileWidth();
			int endY = y / tileset.getTileHeight();
			int sx = startX > endX ? endX : startX;
			int sy = startY > endY ? endY : startY;
			int ex = startX > endX ? startX : endX;
			int ey = startY > endY ? startY : endY;
			if(sx >= getTilesX() || sy >= getTilesY()) {
				selection = null;
				return;
			}
			if(sx < 0)
				sx = 0;
			if(sy < 0)
				sy = 0;
			if(ex >= getTilesX())
				ex = getTilesX() - 1;
			if(ey >= getTilesY())
				ey = getTilesY() - 1;
			int w = ex - sx + 1;
			int h = ey - sy + 1;
			TileSelection newSelection = getSelection(sx, sy, w, h);
			if(newSelection.equals(selection)) // order is important
				return;
			selection = newSelection;
			fireSelectionChangedEvent();
			repaint();
		}

		private synchronized void updatePoint(int x, int y) {
			int px = x / tileset.getTileWidth();
			int py = y / tileset.getTileHeight();
			if(px >= getTilesX() || py >= getTilesY() || px < 0 || py < 0) {
				hoverTileX = hoverTileY = -1;
				repaint();
				return;
			}
			if(px >= getTilesX())
				px = getTilesX() - 1;
			if(py >= getTilesY())
				py = getTilesY() - 1;
			hoverTileX = px;
			hoverTileY = py;
			repaint();
		}
	}
}
