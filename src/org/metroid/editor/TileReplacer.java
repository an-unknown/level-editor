package org.metroid.editor;

import java.awt.BorderLayout;
import java.util.logging.Logger;

import javax.swing.JComboBox;
import javax.swing.JPanel;

import org.metroid.logging.Levels;
import org.metroid.logging.Trace;
import org.metroid.pak.NamedResource;
import org.metroid.pak.level.Level;
import org.metroid.pak.level.Room;
import org.metroid.pak.level.RoomInfo;
import org.metroid.pak.level.Tile;
import org.metroid.pak.level.Tileset;
import org.metroid.project.Project;

public class TileReplacer extends JPanel {
	private static final long serialVersionUID = -1337568193691875516L;
	private static final Logger log = Trace.create(TileReplacer.class);

	private Tileset tileset;
	private TileSelector source;
	private TileSelector destination;
	private JComboBox<String> textureNamesSrc;
	private JComboBox<String> textureNamesDst;
	private Project project;

	public TileReplacer(Project project) {
		super(new BorderLayout());
		this.project = project;
		tileset = new Tileset(project);

		source = new TileSelector(tileset, project);
		destination = new TileSelector(tileset, project);

		textureNamesSrc = new JComboBox<>();
		for(String name : project.getTextureNames())
			textureNamesSrc.addItem(name);
		textureNamesSrc.addItemListener((e) -> {
			String name = textureNamesSrc.getItemAt(textureNamesSrc.getSelectedIndex());
			source.setCurrentTexture(name);
			revalidate();
		});

		textureNamesDst = new JComboBox<>();
		for(String name : project.getTextureNames())
			textureNamesDst.addItem(name);
		textureNamesDst.addItemListener((e) -> {
			String name = textureNamesDst.getItemAt(textureNamesDst.getSelectedIndex());
			destination.setCurrentTexture(name);
			revalidate();
		});

		JPanel left = new JPanel(new BorderLayout());
		left.add(BorderLayout.NORTH, textureNamesSrc);
		left.add(BorderLayout.CENTER, source);

		JPanel right = new JPanel(new BorderLayout());
		right.add(BorderLayout.NORTH, textureNamesDst);
		right.add(BorderLayout.CENTER, destination);

		add(BorderLayout.EAST, right);
		add(BorderLayout.WEST, left);
	}

	public boolean replace() {
		TileSelection src = source.getSelection();
		TileSelection dst = destination.getSelection();
		if(src == null)
			return false;
		if(dst == null)
			return false;
		if(src.width != dst.width)
			return false;
		if(src.height != dst.height)
			return false;
		int count = 0;
		for(NamedResource res : project.getPak().list()) {
			if(res.getResource().getType() == Level.FOURCC) {
				Level level = (Level) res.getResource();
				for(RoomInfo info : level.getRooms()) {
					Room room = info.getRoom();
					Tileset tiles = room.getTileset();
					for(Tile tile : tiles.getTiles()) {
						for(int y = 0; y < src.height; y++) {
							for(int x = 0; x < src.width; x++) {
								Tile s = src.get(x, y);
								Tile d = dst.get(x, y);
								if(tile.getTexture() == s.getTexture() && tile.s == s.s
										&& tile.t == s.t) {
									tile.s = d.s;
									tile.t = d.t;
									tile.setTexture(d.getTexture());
									count++;
								}
							}
						}
					}
				}
			}
		}
		log.log(Levels.INFO, String.format("Replaced %d tiles", count));
		return true;
	}
}
