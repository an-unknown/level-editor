package org.metroid.editor.tiles;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.logging.Logger;

import javax.swing.JComponent;

import org.metroid.logging.Levels;
import org.metroid.logging.Trace;
import org.metroid.pak.txtr.Texture;

public class SourceTexture extends JComponent {
	private static final Logger log = Trace.create(SourceTexture.class);

	private static final long serialVersionUID = 1L;

	private Texture texture;

	private BufferedImage cache;

	public SourceTexture() {
	}

	public void setTexture(Texture texture) {
		this.texture = texture;
		if(texture == null) {
			log.log(Levels.DEBUG, "Displaying no texture");
			setMinimumSize(new Dimension(0, 0));
			setPreferredSize(new Dimension(0, 0));
			cache = null;
		} else {
			log.log(Levels.DEBUG,
					"Displaying texture " + texture.getName() + " (" + texture.getFileName() + ")");
			setMinimumSize(new Dimension(texture.getWidth(), getHeight()));
			setPreferredSize(new Dimension(texture.getWidth(), getHeight()));
			cache = new BufferedImage(texture.getTexture().getWidth(), texture.getTexture().getHeight(),
					texture.getTexture().getType());
			texture.getTexture().copyData(cache.getRaster());
		}
		repaint();
	}

	public Texture getTexture() {
		return texture;
	}

	public void reset() {
		texture.getTexture().copyData(cache.getRaster());
	}

	public void mark(int x, int y, int width, int height) {
		if(cache != null) {
			Graphics g = cache.getGraphics();
			g.setColor(Color.RED);
			g.drawRect(x, y, width - 1, height - 1);
			g.dispose();
			repaint();
		}
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);
		if(cache != null)
			g.drawImage(cache, 0, 0, null);
	}
}
