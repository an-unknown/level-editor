package org.metroid.editor.tiles;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Consumer;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.metroid.editor.LabeledPairLayout;
import org.metroid.editor.TileReplacer;
import org.metroid.pak.NamedResource;
import org.metroid.pak.level.Level;
import org.metroid.pak.level.Room;
import org.metroid.pak.level.RoomInfo;
import org.metroid.pak.level.Tile;
import org.metroid.pak.level.Tileset;
import org.metroid.project.Project;
import org.metroid.util.ScreenHelper;

public class TileEditor extends JPanel {
	private static final long serialVersionUID = 1L;

	private SourceTexture texture;
	private JComboBox<String> textureSelector;
	private JButton add;
	private JButton remove;
	private JButton rename;
	private JButton change;
	private JButton showTiles;

	private Project project;
	private String[] names = new String[0];

	private Set<Runnable> listeners = new HashSet<>();

	public TileEditor(Project project) {
		this.project = project;
		texture = new SourceTexture();
		texture.setTexture(null);
		textureSelector = new JComboBox<>();
		setLayout(new BorderLayout());
		add(BorderLayout.CENTER, texture);
		add(BorderLayout.NORTH, textureSelector);
		JPanel south = new JPanel(new FlowLayout());
		south.add(add = new JButton("add"));
		south.add(remove = new JButton("remove"));
		south.add(rename = new JButton("rename"));
		south.add(change = new JButton("change"));
		south.add(showTiles = new JButton("show tiles"));
		add(BorderLayout.SOUTH, south);
		textureSelector.addItemListener((e) -> {
			int id = textureSelector.getSelectedIndex();
			if(id >= 0)
				texture.setTexture(this.project.getTexture(names[id]));
		});
		add.addActionListener((e) -> {
			JDialog dlg = new JDialog((Frame) null, "Add Texture", true);
			JPanel center = new JPanel(new LabeledPairLayout());
			JTextField name;
			JTextField path;
			center.add("label", new JLabel("Name"));
			center.add("value", name = new JTextField());
			center.add("label", new JLabel("Path"));
			center.add("value", path = new JTextField());
			JButton ok = new JButton("OK");
			JButton cancel = new JButton("Cancel");
			JPanel buttons = new JPanel(new FlowLayout());
			buttons.add(ok);
			buttons.add(cancel);
			dlg.getContentPane().setLayout(new BorderLayout());
			dlg.getContentPane().add(BorderLayout.CENTER, center);
			dlg.getContentPane().add(BorderLayout.SOUTH, buttons);
			dlg.setSize(400, 200);
			dlg.setLocation(ScreenHelper.getWindowPosition(dlg.getSize()));
			ok.addActionListener((ev) -> {
				String texName = name.getText();
				String texPath = path.getText();
				if(texName.trim().length() == 0) {
					JOptionPane.showMessageDialog(dlg, "No name", "Error",
							JOptionPane.ERROR_MESSAGE);
					return;
				}
				if(texPath.trim().length() == 0) {
					JOptionPane.showMessageDialog(dlg, "No path", "Error",
							JOptionPane.ERROR_MESSAGE);
					return;
				}
				if(this.project.getTexture(texName) != null) {
					JOptionPane.showMessageDialog(dlg, "Name already in use", "Error",
							JOptionPane.ERROR_MESSAGE);
					return;
				}
				try {
					this.project.addTexture(texName, texPath);
					updateAll();
					dlg.dispose();
				} catch(FileNotFoundException exc) {
					JOptionPane.showMessageDialog(dlg, "File not found", "Error",
							JOptionPane.ERROR_MESSAGE);
					return;
				} catch(IOException exc) {
					JOptionPane.showMessageDialog(dlg, "Cannot read file: " + exc.getMessage(),
							"Error", JOptionPane.ERROR_MESSAGE);
					return;
				}
			});
			cancel.addActionListener((ev) -> dlg.dispose());
			dlg.setVisible(true);
		});
		remove.addActionListener((e) -> {
			int id = textureSelector.getSelectedIndex();
			if(id >= 0) {
				this.project.removeTexture(names[id]);
				updateAll();
			}
		});
		rename.addActionListener((e) -> {
			int id = textureSelector.getSelectedIndex();
			if(id >= 0) {
				// TODO
				updateAll();
			}
		});
		change.addActionListener((e) -> {
			JFrame dlg = new JFrame("Change tiles...");
			TileReplacer replacer = new TileReplacer(this.project);
			JButton ok = new JButton("OK");
			ok.addActionListener((ev) -> {
				if(replacer.replace())
					dlg.dispose();
			});
			JButton cancel = new JButton("Cancel");
			cancel.addActionListener((ev) -> dlg.dispose());
			JPanel buttons = new JPanel(new FlowLayout());
			buttons.add(ok);
			buttons.add(cancel);
			dlg.getContentPane().setLayout(new BorderLayout());
			dlg.getContentPane().add(BorderLayout.CENTER, replacer);
			dlg.getContentPane().add(BorderLayout.SOUTH, buttons);
			dlg.setSize(400, 200);
			dlg.setLocation(ScreenHelper.getWindowPosition(dlg.getSize()));
			dlg.setVisible(true);
		});
		showTiles.addActionListener((e) -> {
			int width = this.project.getTileWidth();
			int height = this.project.getTileHeight();
			texture.reset();
			forEachTile((tile) -> {
				// same reference!
				if(tile.getTexture() == texture.getTexture()) {
					int px = tile.s * width;
					int py = tile.t * height;
					texture.mark(px, py, width, height);
				}
			});
		});
		update();
	}

	public void update() {
		names = project.getTextureNames();
		textureSelector.removeAllItems();
		for(String name : names)
			textureSelector.addItem(name);
		revalidate();
	}

	public void addTextureChangeListener(Runnable r) {
		listeners.add(r);
	}

	public void updateAll() {
		for(Runnable l : listeners) {
			l.run();
		}
	}

	public void setProject(Project prj) {
		this.project = prj;
		update();
	}

	public void forEachTile(Consumer<Tile> callback) {
		for(NamedResource res : project.getPak().list()) {
			if(res.getResource().getType() == Level.FOURCC) {
				Level level = (Level) res.getResource();
				for(RoomInfo info : level.getRooms()) {
					Room room = info.getRoom();
					Tileset tiles = room.getTileset();
					for(Tile tile : tiles.getTiles()) {
						callback.accept(tile);
					}
				}
			}
		}
	}
}
