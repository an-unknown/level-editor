package org.metroid.editor;

import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.JPanel;

import org.metroid.Listener;
import org.metroid.bts.Items;
import org.metroid.bts.TileBts;
import org.metroid.bts.TileSubtype;
import org.metroid.bts.TileType;

public class BtsSelector extends JPanel {
	private static final long serialVersionUID = 1L;

	private static int[] items = { Items.CHARGE_BEAM, Items.PLASMA_BEAM, Items.SPAZER_BEAM, Items.ICE_BEAM,
			Items.WAVE_BEAM, Items.LONG_BEAM, Items.F_BEAM, Items.BOMB, Items.MORPH_BALL, Items.SPIDER_BALL,
			Items.HIGHJUMP_BOOTS, Items.SPRING_BALL, Items.POWER_GRIP, Items.SCREW_ATTACK, Items.SPACE_JUMP,
			Items.SPEED_BOOSTER, Items.VARIA_SUIT, Items.GRAVITY_SUIT, Items.UNKNOWN_SUIT, Items.XRAY_VISOR,
			Items.GRAPPLE_BEAM, Items.SEEKER_MISSILE, Items.ENERGY_TANK, Items.RESERVE_TANK };
	private static TileBts[] btsTemplates;

	private GX gx;
	private int hoverTileX;
	private int hoverTileY;
	private int tilesX;
	private int tilesY;
	private int selection;
	private Set<Listener> listeners;

	static {
		List<TileBts> bts = new ArrayList<TileBts>();
		bts.add(create(TileType.AIR, TileSubtype.AIR_NORMAL));
		bts.add(create(TileType.AIR, TileSubtype.AIR_FOOL_XRAY));
		bts.add(create(TileType.AIR, TileSubtype.AIR_SPIDERBALL_HORIZONTAL));
		bts.add(create(TileType.AIR, TileSubtype.AIR_SPIDERBALL_VERTICAL));
		bts.add(create(TileType.AIR, TileSubtype.AIR_SPIDERBALL_Q1));
		bts.add(create(TileType.AIR, TileSubtype.AIR_SPIDERBALL_Q2));
		bts.add(create(TileType.AIR, TileSubtype.AIR_SPIDERBALL_Q3));
		bts.add(create(TileType.AIR, TileSubtype.AIR_SPIDERBALL_Q4));
		bts.add(create(TileType.SOLID, TileSubtype.SOLID_NORMAL));
		bts.add(create(TileType.SOLID, TileSubtype.SOLID_GRAPPLE));
		bts.add(create(TileType.SOLID, TileSubtype.SOLID_LADDER));
		bts.add(create(TileType.SOLID, TileSubtype.SOLID_LADDER2));
		bts.add(create(TileType.SOLID, TileSubtype.SOLID_RAIL));
		for(int item : items)
			bts.add(create(TileType.ITEM, TileSubtype.ITEM_TYPE_EQUIPMENT | item));
		bts.add(create(TileType.ITEM, TileSubtype.ITEM_TYPE_MISSILE));
		bts.add(create(TileType.ITEM, TileSubtype.ITEM_TYPE_SUPER_MISSILE));
		bts.add(create(TileType.ITEM, TileSubtype.ITEM_TYPE_POWER_BOMB));
		bts.add(create(TileType.DESTRUCTIBLE, TileSubtype.DESTRUCTIBLE_BEAM));
		bts.add(create(TileType.DESTRUCTIBLE, TileSubtype.DESTRUCTIBLE_BOMB));
		bts.add(create(TileType.DESTRUCTIBLE, TileSubtype.DESTRUCTIBLE_POWERBOMB));
		bts.add(create(TileType.DESTRUCTIBLE, TileSubtype.DESTRUCTIBLE_MISSILE));
		bts.add(create(TileType.DESTRUCTIBLE, TileSubtype.DESTRUCTIBLE_SUPERMISSILE));
		bts.add(create(TileType.DESTRUCTIBLE, TileSubtype.DESTRUCTIBLE_SCREW));
		bts.add(create(TileType.DESTRUCTIBLE, TileSubtype.DESTRUCTIBLE_SPEEDBOOST));
		bts.add(create(TileType.CRUMBLE, TileSubtype.CRUMBLE_NORMAL));
		bts.add(create(TileType.CRUMBLE, TileSubtype.CRUMBLE_GRAPPLE));
		bts.add(create(TileType.TREADMILL, TileSubtype.TREADMILL_CONVEYOR_LEFT));
		bts.add(create(TileType.TREADMILL, TileSubtype.TREADMILL_CONVEYOR_RIGHT));
		bts.add(create(TileType.TREADMILL, TileSubtype.TREADMILL_QUICKSAND));
		bts.add(create(TileType.TREADMILL, TileSubtype.TREADMILL_SANDFALL));
		for(int i = 0; i < 108; i++)
			bts.add(create(TileType.SLOPE, i));
		btsTemplates = bts.toArray(new TileBts[bts.size()]);
	}

	private static TileBts create(int type, int subtype) {
		TileBts bts = new TileBts();
		bts.setTileType(type);
		bts.setTileSubtype(subtype);
		return bts;
	}

	public BtsSelector() {
		listeners = new HashSet<Listener>();
		setLayout(new BorderLayout());
		add(BorderLayout.CENTER, gx = new GX());
		MouseHandler h = new MouseHandler();
		gx.addMouseListener(h);
		gx.addMouseMotionListener(h);
		setMinimumSize(new Dimension(BtsImages.SIZE * btsTemplates.length, BtsImages.SIZE));
		setPreferredSize(new Dimension(BtsImages.SIZE * btsTemplates.length, BtsImages.SIZE));
	}

	public void addSelectionListener(Listener l) {
		listeners.add(l);
	}

	protected void fireSelectionChangedEvent() {
		for(Listener l : listeners)
			l.call();
	}

	public TileBts getSelection() {
		if(selection == -1)
			return null;
		return btsTemplates[selection];
	}

	private class GX extends Canvas {
		private static final long serialVersionUID = 1L;

		private BufferedImage cache;
		private BufferedImage img;

		private Point getTextureCoordinates(int i) {
			// avoid division by zero if canvas is too small
			if(tilesX == 0)
				return getTextureCoordinates(i % tilesX, 0);
			int x = i % tilesX;
			int y = i / tilesX;
			return getTextureCoordinates(x, y);
		}

		private Point getTextureCoordinates(int x, int y) {
			return new Point(x * BtsImages.SIZE, y * BtsImages.SIZE);
		}

		private void prepareCache() {
			if(cache != null && cache.getWidth() == getWidth() && cache.getHeight() == getHeight())
				return;
			cache = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_ARGB);
			Graphics g = cache.getGraphics();
			g.setColor(Color.BLACK);
			g.fillRect(0, 0, cache.getWidth(), cache.getHeight());
			int x = 0;
			int y = 0;
			tilesX = cache.getWidth() / BtsImages.SIZE;
			tilesY = cache.getHeight() / BtsImages.SIZE;
			for(int i = 0; i < btsTemplates.length; i++) {
				TileBts bts = btsTemplates[i];
				BtsImages.paint(g, x, y, bts);
				if(x + 2 * BtsImages.SIZE > cache.getWidth()) {
					x = 0;
					y += BtsImages.SIZE;
				} else
					x += BtsImages.SIZE;
			}
			g.dispose();
		}

		private void prepare() {
			prepareCache();
			img = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_ARGB);
			Graphics g = img.getGraphics();
			g.drawImage(cache, 0, 0, null);
			int w = BtsImages.SIZE;
			int h = BtsImages.SIZE;
			if(selection != -1) {
				Point p = getTextureCoordinates(selection);
				g.setColor(Color.LIGHT_GRAY);
				g.drawRect(p.x, p.y, w - 1, h - 1);
			}
			if(hoverTileX != -1) {
				Point p = getTextureCoordinates(hoverTileX, hoverTileY);
				g.setColor(Color.LIGHT_GRAY);
				g.drawRect(p.x, p.y, w - 1, h - 1);
			}
			g.dispose();
		}

		@Override
		public void update(Graphics g) {
			paint(g);
		}

		@Override
		public void paint(Graphics g) {
			prepare();
			g.drawImage(img, 0, 0, null);
		}
	}

	private class MouseHandler extends MouseAdapter {
		@Override
		public void mouseMoved(MouseEvent e) {
			updatePoint(e.getX(), e.getY());
		}

		@Override
		public void mouseDragged(MouseEvent e) {
			updateSelection(e.getX(), e.getY());
			updatePoint(e.getX(), e.getY());
		}

		@Override
		public void mousePressed(MouseEvent e) {
			hoverTileX = hoverTileY = -1;
			updateSelection(e.getX(), e.getY());
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			updateSelection(e.getX(), e.getY());
			updatePoint(e.getX(), e.getY());
		}

		@Override
		public void mouseEntered(MouseEvent e) {
			updatePoint(e.getX(), e.getY());
		}

		@Override
		public void mouseExited(MouseEvent e) {
			hoverTileX = hoverTileY = -1;
		}

		private synchronized void updateSelection(int x, int y) {
			int px = x / BtsImages.SIZE;
			int py = y / BtsImages.SIZE;
			if(px < 0)
				px = 0;
			if(py < 0)
				py = 0;
			if(px >= tilesX)
				px = tilesX - 1;
			if(py >= tilesY)
				py = tilesY - 1;
			int newSelection = py * tilesX + px;
			if(selection == newSelection)
				return;
			selection = newSelection;
			fireSelectionChangedEvent();
			gx.repaint();
		}

		private synchronized void updatePoint(int x, int y) {
			int px = x / BtsImages.SIZE;
			int py = y / BtsImages.SIZE;
			if(px >= tilesX || py >= tilesY || px < 0 || py < 0) {
				hoverTileX = hoverTileY = -1;
				repaint();
				return;
			}
			if(px >= tilesX)
				px = tilesX - 1;
			if(py >= tilesY)
				py = tilesY - 1;
			hoverTileX = px;
			hoverTileY = py;
			gx.repaint();
		}
	}
}
