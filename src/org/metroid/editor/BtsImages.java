package org.metroid.editor;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

import org.metroid.bts.Items;
import org.metroid.bts.TileBts;
import org.metroid.bts.TileSubtype;
import org.metroid.bts.TileType;
import org.metroid.pak.txtr.Texture;

public class BtsImages {
	public static final Texture ITEMS = load("items.png");
	public static final Texture BLOCKS = load("blocks.png");
	public static final Texture SLOPES = load("slopes.png");
	public static final Texture BTS = load("bts.png");

	public static final int SIZE = 32;

	public static final int BLOCK_CRUMBLE = 0;
	public static final int BLOCK_BEAM = 1;
	public static final int BLOCK_BOMB = 2;
	public static final int BLOCK_PBOMB = 3;
	public static final int BLOCK_MISSILE = 4;
	public static final int BLOCK_SMISSILE = 5;
	public static final int BLOCK_SPEEDBOOSTER = 6;
	public static final int BLOCK_SCREWATTACK = 7;
	public static final int BLOCK_FOOLXRAY = 8;
	public static final int BLOCK_GRAPPLE = 9;
	public static final int BLOCK_GRAPPLECRUMBLE = 10;
	public static final int BLOCK_SPIKES = 11;
	public static final int BLOCK_LEFT = 12;
	public static final int BLOCK_RIGHT = 13;
	public static final int BLOCK_DOWN = 14;
	public static final int BLOCK_FASTCRUMBLE = 15;

	public static final int BTS_LADDER1 = 0;
	public static final int BTS_LADDER2 = 1;
	public static final int BTS_SWITCH = 3;
	public static final int BTS_CONVEYOR = 5;
	public static final int BTS_QUICKSAND = 6;
	public static final int BTS_SANDFALL = 7;
	public static final int BTS_SPIDERBALL_HORIZONTAL = 8;
	public static final int BTS_SPIDERBALL_VERTICAL = 9;
	public static final int BTS_SPIDERBALL_Q2 = 10;
	public static final int BTS_SPIDERBALL_Q1 = 11;
	public static final int BTS_RAIL = 12;
	public static final int BTS_SOLID = 13;
	public static final int BTS_SPIDERBALL_Q3 = 14;
	public static final int BTS_SPIDERBALL_Q4 = 15;

	private static InputStream getResource(String name) {
		return BtsImages.class.getResourceAsStream("/org/metroid/assets/editor/" + name);
	}

	private static Texture load(String name) {
		try {
			return getTexture(name);
		} catch(Throwable t) {
			t.printStackTrace();
			return null;
		}
	}

	private static Texture getTexture(String name) throws IOException {
		BufferedImage image = ImageIO.read(getResource(name));
		return new Texture(image);
	}

	private static BufferedImage get(Texture tex, int i) {
		int tilesX = tex.getWidth() / SIZE;
		int x = i % tilesX;
		int y = i / tilesX;
		return tex.getSubimage(x * SIZE, y * SIZE, SIZE, SIZE);
	}

	public static void paint(Graphics g, int x, int y, TileBts bts) {
		BufferedImage img = null;
		switch(bts.getTileType()) {
		case TileType.AIR:
			switch(bts.getTileSubtype()) {
			case TileSubtype.AIR_FOOL_XRAY:
				img = get(BLOCKS, BLOCK_FOOLXRAY);
				break;
			case TileSubtype.AIR_SPIDERBALL_HORIZONTAL:
				img = get(BTS, BTS_SPIDERBALL_HORIZONTAL);
				break;
			case TileSubtype.AIR_SPIDERBALL_VERTICAL:
				img = get(BTS, BTS_SPIDERBALL_VERTICAL);
				break;
			case TileSubtype.AIR_SPIDERBALL_Q1:
				img = get(BTS, BTS_SPIDERBALL_Q1);
				break;
			case TileSubtype.AIR_SPIDERBALL_Q2:
				img = get(BTS, BTS_SPIDERBALL_Q2);
				break;
			case TileSubtype.AIR_SPIDERBALL_Q3:
				img = get(BTS, BTS_SPIDERBALL_Q3);
				break;
			case TileSubtype.AIR_SPIDERBALL_Q4:
				img = get(BTS, BTS_SPIDERBALL_Q4);
				break;
			}
			break;
		case TileType.SLOPE:
			img = get(SLOPES, bts.getTileSubtype());
			break;
		case TileType.TREADMILL:
			switch(bts.getTileSubtype()) {
			case TileSubtype.TREADMILL_QUICKSAND:
				img = get(BTS, BTS_QUICKSAND);
				break;
			case TileSubtype.TREADMILL_SANDFALL:
				img = get(BTS, BTS_SANDFALL);
				break;
			case TileSubtype.TREADMILL_CONVEYOR_LEFT:
				img = get(BLOCKS, BLOCK_LEFT);
				break;
			case TileSubtype.TREADMILL_CONVEYOR_RIGHT:
				img = get(BLOCKS, BLOCK_RIGHT);
				break;
			}
			break;
		case TileType.SOLID:
			switch(bts.getTileSubtype()) {
			case TileSubtype.SOLID_NORMAL:
				img = get(BTS, BTS_SOLID);
				break;
			case TileSubtype.SOLID_GRAPPLE:
				img = get(BLOCKS, BLOCK_GRAPPLE);
				break;
			case TileSubtype.SOLID_LADDER:
				img = get(BTS, BTS_LADDER1);
				break;
			case TileSubtype.SOLID_LADDER2:
				img = get(BTS, BTS_LADDER2);
				break;
			case TileSubtype.SOLID_RAIL:
				img = get(BTS, BTS_RAIL);
				break;
			}
			break;
		case TileType.CRUMBLE:
			if((bts.getTileSubtype() & TileSubtype.CRUMBLE_GRAPPLE) != 0)
				img = get(BLOCKS, BLOCK_GRAPPLECRUMBLE);
			else
				img = get(BLOCKS, BLOCK_CRUMBLE);
			break;
		case TileType.DESTRUCTIBLE:
			switch(bts.getTileSubtype() & TileSubtype.DESTRUCTIBLE_MASK) {
			case TileSubtype.DESTRUCTIBLE_BEAM:
			case TileSubtype.DESTRUCTIBLE_PLASMA:
			case TileSubtype.DESTRUCTIBLE_WAVE:
			case TileSubtype.DESTRUCTIBLE_SPAZER:
			case TileSubtype.DESTRUCTIBLE_ICE:
				img = get(BLOCKS, BLOCK_BEAM);
				break;
			case TileSubtype.DESTRUCTIBLE_MISSILE:
				img = get(BLOCKS, BLOCK_MISSILE);
				break;
			case TileSubtype.DESTRUCTIBLE_SUPERMISSILE:
				img = get(BLOCKS, BLOCK_SMISSILE);
				break;
			case TileSubtype.DESTRUCTIBLE_BOMB:
				img = get(BLOCKS, BLOCK_BOMB);
				break;
			case TileSubtype.DESTRUCTIBLE_POWERBOMB:
				img = get(BLOCKS, BLOCK_PBOMB);
				break;
			case TileSubtype.DESTRUCTIBLE_SPEEDBOOST:
				img = get(BLOCKS, BLOCK_SPEEDBOOSTER);
				break;
			case TileSubtype.DESTRUCTIBLE_SCREW:
				img = get(BLOCKS, BLOCK_SCREWATTACK);
				break;
			}
			break;
		case TileType.ITEM:
			switch(bts.getTileSubtype() & ~TileSubtype.ITEM_MASK) {
			case TileSubtype.ITEM_TYPE_MISSILE:
				img = get(ITEMS, Items.MISSILE);
				break;
			case TileSubtype.ITEM_TYPE_SUPER_MISSILE:
				img = get(ITEMS, Items.SUPER_MISSILE);
				break;
			case TileSubtype.ITEM_TYPE_POWER_BOMB:
				img = get(ITEMS, Items.POWER_BOMB);
				break;
			case TileSubtype.ITEM_TYPE_EQUIPMENT:
				img = get(ITEMS, bts.getTileSubtype() & TileSubtype.ITEM_MASK);
				break;
			}
			break;
		case TileType.MACHINE:
			switch(bts.getTileSubtype()) {
			case TileSubtype.MACHINE_SWITCH:
				img = get(BTS, BTS_SWITCH);
				break;
			}
			break;
		}
		if(img != null)
			g.drawImage(img, x, y, null);
	}
}
