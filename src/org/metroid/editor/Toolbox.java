package org.metroid.editor;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;

import org.metroid.pak.level.Room;
import org.metroid.project.Project;

public class Toolbox {
	private JFrame toolbox;

	private TileSelector tileSelector;
	private BtsSelector btsSelector;

	private JComboBox<String> textureNames;

	private Project project;

	public Toolbox(TileSelector tileSelector, BtsSelector btsSelector, Project project) {
		this.tileSelector = tileSelector;
		this.btsSelector = btsSelector;
		this.project = project;
		createWindow();
	}

	private void createWindow() {
		textureNames = new JComboBox<>();
		for(String name : project.getTextureNames())
			textureNames.addItem(name);
		textureNames.addItemListener((e) -> {
			String name = textureNames.getItemAt(textureNames.getSelectedIndex());
			tileSelector.setCurrentTexture(name);
			revalidate();
		});

		toolbox = new JFrame("Toolbox");
		JPanel north = new JPanel(new FlowLayout());
		north.add(textureNames);
		JPanel center = new JPanel(new BorderLayout());
		center.add(BorderLayout.WEST, tileSelector);
		center.add(BorderLayout.CENTER, btsSelector);
		toolbox.getContentPane().setLayout(new BorderLayout());
		toolbox.add(BorderLayout.NORTH, north);
		toolbox.add(BorderLayout.CENTER, center);
		toolbox.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);

		toolbox.setSize(600, 400);
		toolbox.setLocation(0, 0);
		// toolbox.setType(Window.Type.UTILITY);
		toolbox.setAlwaysOnTop(true);
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public void setRoom(Room room) {
		textureNames.setSelectedItem(room.getTileset().getEmptyTile().getTexture().getName());
	}

	public void update(Room room) {
		tileSelector.update();
		String selected = (String) textureNames.getSelectedItem();
		textureNames.removeAllItems();
		for(String name : project.getTextureNames())
			textureNames.addItem(name);
		if(selected != null && project.getTexture(selected) != null)
			textureNames.setSelectedItem(selected);
		else if(room != null)
			textureNames.setSelectedItem(room.getTileset().getEmptyTile().getTexture().getName());
		else if(textureNames.getItemCount() > 0)
			textureNames.setSelectedIndex(0);
		selected = (String) textureNames.getSelectedItem();
		tileSelector.setCurrentTexture(selected);
		revalidate();
	}

	public void show() {
		toolbox.setVisible(true);
	}

	public void revalidate() {
		toolbox.revalidate();
	}

	public void dispose() {
		toolbox.dispose();
	}
}
