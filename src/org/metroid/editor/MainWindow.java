package org.metroid.editor;

import java.awt.BorderLayout;
import java.awt.FileDialog;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JTabbedPane;

import org.metroid.editor.tiles.TileEditor;
import org.metroid.pak.level.Level;
import org.metroid.project.Project;
import org.metroid.util.ScreenHelper;

public class MainWindow extends JFrame {
	private static final long serialVersionUID = 1L;

	private JTabbedPane tabs;
	private PakEditor pakEditor;
	private LevelEditor levelEditor;
	private Toolbox toolbox;
	private RoomEditor roomEditor;
	private TileEditor tileEditor;
	private Palette palette;
	public JFrame paletteWindow;

	private Project prj;

	private FileDialog load;
	private FileDialog save;
	private FileDialog screenshot;

	public MainWindow() throws IOException {
		super("Level Editor");

		load = new FileDialog(this, "Load...", FileDialog.LOAD);
		save = new FileDialog(this, "Save...", FileDialog.SAVE);
		screenshot = new FileDialog(this, "Save screenshot...", FileDialog.SAVE);

		prj = Project.read(new File("default.xml"));
		Level level = LevelEditor.createDefaultLevel(prj);
		prj.getPak().add("default", level);

		tabs = new JTabbedPane();
		tabs.addTab("Pak", pakEditor = new PakEditor(prj.getPak()));
		tabs.addTab("Level", levelEditor = new LevelEditor(level));
		tabs.addTab("Room", roomEditor = new RoomEditor(prj, levelEditor.getRoom(0)));
		tabs.addTab("Textures", tileEditor = new TileEditor(prj));
		toolbox = roomEditor.getToolbox();
		palette = roomEditor.getPalette();
		paletteWindow = palette.createWindow();

		roomEditor.addChangeListener(() -> levelEditor.refresh());
		levelEditor.addRoomChangedListener(() -> {
			roomEditor.setRoom(levelEditor.getRoom());
			tileEditor.update();
		});
		pakEditor.addLevelSelectionListener(() -> {
			Level l = pakEditor.getSelectedLevel();
			if(l != null)
				levelEditor.setLevel(l);
			tileEditor.update();
		});
		tileEditor.addTextureChangeListener(() -> update());

		setLayout(new BorderLayout());
		add(BorderLayout.CENTER, tabs);

		JMenuBar menu = new JMenuBar();
		JMenu fileMenu = new JMenu("File");
		JMenuItem read = new JMenuItem("Read");
		JMenuItem write = new JMenuItem("Write");
		JMenuItem screenshot = new JMenuItem("Screenshot");
		JMenuItem exportImage = new JMenuItem("Export image");
		JMenuItem quit = new JMenuItem("Exit");

		read.addActionListener((e) -> {
			try {
				this.load.setVisible(true);
				if(this.load.getFile() == null)
					return;
				String file = this.load.getDirectory() + this.load.getFile();
				readXml(file);
			} catch(IOException ex) {
				ex.printStackTrace();
			}
		});
		write.addActionListener((e) -> {
			try {
				this.save.setVisible(true);
				if(this.save.getFile() == null)
					return;
				String file = this.save.getDirectory() + this.save.getFile();
				writeXml(file);
			} catch(IOException ex) {
				ex.printStackTrace();
			}
		});
		screenshot.addActionListener((e) -> {
			try {
				this.screenshot.setVisible(true);
				if(this.screenshot.getFile() == null)
					return;
				String file = this.screenshot.getDirectory() + this.screenshot.getFile();
				ImageIO.write(RoomRenderer.render(roomEditor.getRoom()), "png", new File(file));
			} catch(IOException ex) {
				ex.printStackTrace();
			}
		});
		exportImage.addActionListener((e) -> {
			try {
				this.screenshot.setVisible(true);
				if(this.screenshot.getFile() == null)
					return;
				String file = this.screenshot.getDirectory() + this.screenshot.getFile();
				ImageIO.write(LevelRenderer.render(levelEditor.getLevel()), "png", new File(file));
			} catch(IOException ex) {
				ex.printStackTrace();
			}
		});
		quit.addActionListener((e) -> quit());

		fileMenu.add(read);
		fileMenu.add(write);
		fileMenu.addSeparator();
		fileMenu.add(screenshot);
		fileMenu.add(exportImage);
		fileMenu.addSeparator();
		fileMenu.add(quit);
		menu.add(fileMenu);
		setJMenuBar(menu);

		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				quit();
			}
		});
	}

	private void quit() {
		toolbox.dispose();
		paletteWindow.dispose();
		dispose();
		System.exit(0);
	}

	public void update() {
		tileEditor.update();
		roomEditor.update();
		tileEditor.update();
	}

	public void readXml(String filename) throws IOException {
		prj = Project.read(new File(filename));
		tileEditor.setProject(prj);
		roomEditor.setProject(prj);
		pakEditor.setPak(prj.getPak());
	}

	public void writeXml(String filename) throws IOException {
		prj.write(new File(filename));
	}

	public static void main(String[] args) throws IOException {
		MainWindow w = new MainWindow();
		w.setSize(800, 600);
		w.setLocation(ScreenHelper.getWindowPosition(w.getSize()));
		w.setVisible(true);
		w.toolbox.show();
		w.paletteWindow.setVisible(true);
	}
}