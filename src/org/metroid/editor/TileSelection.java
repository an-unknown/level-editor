package org.metroid.editor;

import java.util.Arrays;

import org.metroid.pak.level.Tile;

public class TileSelection {
	public final int x;
	public final int y;
	public final int width;
	public final int height;
	public final Tile[][] tiles;

	public TileSelection(int x, int y, Tile[][] tiles) {
		this.x = x;
		this.y = y;
		this.tiles = tiles;
		this.width = tiles[0].length;
		this.height = tiles.length;
	}

	public Tile get(int x, int y) {
		return tiles[y][x];
	}

	@Override
	public boolean equals(Object o) {
		if(!(o instanceof TileSelection))
			return false;
		TileSelection s = (TileSelection) o;
		return Arrays.equals(s.tiles, tiles);
	}

	@Override
	public int hashCode() {
		return Arrays.hashCode(tiles);
	}
}
