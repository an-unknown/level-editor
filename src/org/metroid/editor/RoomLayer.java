package org.metroid.editor;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

import org.metroid.pak.level.Room;
import org.metroid.pak.level.Tile;
import org.metroid.project.Project;

public abstract class RoomLayer implements Layer {
	protected Room room;

	public RoomLayer(Room room) {
		this.room = room;
	}

	protected Project getProject() {
		return room.getProject();
	}

	@Override
	public void resize(int width, int height) {
		room.resize(width, height);
	}

	@Override
	public void paint(Graphics g) {
		paint(g, 0, 0, getWidth(), getHeight());
	}

	@Override
	public void paint(Graphics g, int x, int y, int _w, int _h) {
		int w = room.getTileset().getTileWidth();
		int h = room.getTileset().getTileHeight();

		int width = getWidth();
		int height = getHeight();
		int ex = x + _w;
		int ey = y + _h;
		if(ex >= width)
			ex = width;
		if(ey >= height)
			ey = height;

		for(int py = y; py < ey; py++) {
			for(int px = x; px < ex; px++) {
				Tile tile = get(px, py);
				BufferedImage img = room.getTileset().get(tile);
				g.drawImage(img, px * w, py * h, null);
			}
		}
	}

	@Override
	public int getWidth() {
		return room.getWidth();
	}

	@Override
	public int getHeight() {
		return room.getHeight();
	}

	@Override
	public void fill(TileSelection tiles) {
	}

	@Override
	public void put(int x, int y, TileSelection tiles) {
		int maxx = x + tiles.width;
		int maxy = y + tiles.height;
		int w = maxx > room.getWidth() ? (room.getWidth() - x) : tiles.width;
		int h = maxy > room.getHeight() ? (room.getHeight() - y) : tiles.height;
		for(int py = 0; py < h; py++)
			for(int px = 0; px < w; px++)
				set(x + px, y + py, tiles.get(px, py));
	}

	@Override
	public abstract Tile get(int x, int y);

	public abstract void set(int x, int y, Tile tile);
}
