package org.metroid.editor;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.metroid.Listener;
import org.metroid.pak.level.Room;
import org.metroid.project.Project;

public class RoomEditor extends JPanel {
	private static final long serialVersionUID = 1L;

	private Room room;

	private TileSelector tileSelector;
	private BtsSelector btsSelector;
	private RoomLayoutEditor layoutEditor;

	private JTextField width;
	private JTextField height;

	private Toolbox toolbox;
	private Palette palette;

	public RoomEditor(Project project, Room room) {
		this.room = room;
		setLayout(new BorderLayout());
		JComboBox<String> currentLayer = new JComboBox<String>();
		currentLayer.addItem("HIDDEN");
		currentLayer.addItem("FG1");
		currentLayer.addItem("FG2");
		currentLayer.addItem("BG1");
		currentLayer.addItem("BG2");
		currentLayer.addItem("BTS");
		currentLayer.addItemListener((e) -> layoutEditor.setCurrentLayer(currentLayer.getSelectedIndex()));
		JCheckBox showHIDDEN = new JCheckBox("HIDDEN");
		JCheckBox showFG1 = new JCheckBox("FG1");
		JCheckBox showFG2 = new JCheckBox("FG2");
		JCheckBox showBG1 = new JCheckBox("BG1");
		JCheckBox showBG2 = new JCheckBox("BG2");
		JCheckBox showBTS = new JCheckBox("BTS");
		showHIDDEN.setSelected(true);
		showFG1.setSelected(true);
		showFG2.setSelected(true);
		showBG1.setSelected(true);
		showBG2.setSelected(true);
		showBTS.setSelected(true);
		showHIDDEN.addItemListener((e) -> layoutEditor.setLayerVisibility(RoomLayoutEditor.HIDDEN,
				showHIDDEN.isSelected()));
		showFG1.addItemListener(
				(e) -> layoutEditor.setLayerVisibility(RoomLayoutEditor.FG1, showFG1.isSelected()));
		showFG2.addItemListener(
				(e) -> layoutEditor.setLayerVisibility(RoomLayoutEditor.FG2, showFG2.isSelected()));
		showBG1.addItemListener(
				(e) -> layoutEditor.setLayerVisibility(RoomLayoutEditor.BG1, showBG1.isSelected()));
		showBG2.addItemListener(
				(e) -> layoutEditor.setLayerVisibility(RoomLayoutEditor.BG2, showBG2.isSelected()));
		showBTS.addItemListener(
				(e) -> layoutEditor.setLayerVisibility(RoomLayoutEditor.BTS, showBTS.isSelected()));
		width = new JTextField(Integer.toString(room.getWidth()), 5);
		height = new JTextField(Integer.toString(room.getHeight()), 5);
		JButton resize = new JButton("resize");
		resize.addActionListener((e) -> resize());
		JPanel north = new JPanel(new FlowLayout());
		north.add(currentLayer);
		north.add(showHIDDEN);
		north.add(showFG1);
		north.add(showFG2);
		north.add(showBG1);
		north.add(showBG2);
		north.add(showBTS);
		north.add(width);
		north.add(height);
		north.add(resize);
		JPanel south = new JPanel(new BorderLayout());
		south.add(BorderLayout.WEST, tileSelector = new TileSelector(room.getTileset(), project));
		south.add(BorderLayout.CENTER, btsSelector = new BtsSelector());
		add(BorderLayout.NORTH, north);
		add(BorderLayout.CENTER, layoutEditor = new RoomLayoutEditor(room));
		// add(BorderLayout.SOUTH, south);
		tileSelector.addSelectionListener(() -> layoutEditor.setSelectedTiles(tileSelector.getSelection()));
		btsSelector.addSelectionListener(() -> layoutEditor.setSelectedBts(btsSelector.getSelection()));

		layoutEditor.setCurrentLayer(RoomLayoutEditor.FG1);
		currentLayer.setSelectedIndex(RoomLayoutEditor.FG1);

		toolbox = new Toolbox(tileSelector, btsSelector, project);
		palette = new Palette(room.getTileset());

		layoutEditor.addSelectionListener(() -> palette.setSelection(layoutEditor.getSelection()));
		tileSelector.addSelectionListener(() -> palette.setSelection(tileSelector.getSelection()));
		palette.addSelectionListener(() -> layoutEditor.setSelectedTiles(palette.getSelection()));
	}

	public Toolbox getToolbox() {
		return toolbox;
	}

	public Palette getPalette() {
		return palette;
	}

	public void setProject(Project project) {
		tileSelector.setProject(project);
		toolbox.setProject(project);
		update();
	}

	public void update() {
		toolbox.update(room);
		revalidate();
	}

	public Room getRoom() {
		return room;
	}

	public void setRoom(Room room) {
		this.room = room;
		layoutEditor.setRoom(room);
		tileSelector.setTileset(room.getTileset());
		palette.setTileset(room.getTileset());
		toolbox.setRoom(room);
		updateSize();
		revalidate();
	}

	private void updateSize() {
		width.setText(Integer.toString(room.getWidth()));
		height.setText(Integer.toString(room.getHeight()));
	}

	private void resize() {
		int width = room.getWidth();
		int height = room.getHeight();
		try {
			width = Integer.parseInt(this.width.getText());
		} catch(NumberFormatException e) {
		}
		try {
			height = Integer.parseInt(this.height.getText());
		} catch(NumberFormatException e) {
		}
		layoutEditor.resizeRoom(width, height);
		updateSize();
	}

	public void addChangeListener(Listener l) {
		layoutEditor.addChangeListener(l);
	}
}
