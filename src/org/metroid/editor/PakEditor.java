package org.metroid.editor;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.util.HashSet;
import java.util.Set;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;

import org.metroid.Listener;
import org.metroid.pak.NamedResource;
import org.metroid.pak.Pak;
import org.metroid.pak.Resource;
import org.metroid.pak.Utils;
import org.metroid.pak.level.Level;

public class PakEditor extends JPanel {
	private static final long serialVersionUID = 1L;

	private Pak pak;

	private JTable table;
	private Model model;
	private NamedResource[] resources;

	private Set<Listener> listeners;

	private class Model extends AbstractTableModel {
		private static final long serialVersionUID = 1L;

		@Override
		public int getColumnCount() {
			return 3;
		}

		@Override
		public int getRowCount() {
			return resources.length;
		}

		@Override
		public Object getValueAt(int row, int column) {
			switch(column) {
			case 0:
				return Utils.fourCC(resources[row].getResource().getType());
			case 1:
				return resources[row].getName();
			default:
				return "";
			}
		}

		@Override
		public void setValueAt(Object o, int row, int column) {
			String name = String.valueOf(o);
			if(pak.exists(resources[row].getResource().getType(), name))
				return;
			pak.rename(resources[row].getResource().getType(), resources[row].getName(), name);
			update();
		}

		@Override
		public String getColumnName(int column) {
			switch(column) {
			case 0:
				return "Type";
			case 1:
				return "Name";
			case 2:
				return "Action";
			default:
				return "Unknown";
			}
		}

		@Override
		public boolean isCellEditable(int row, int col) {
			return col == 1;
		}

		public void update() {
			resources = pak.list();
			fireTableDataChanged();
		}
	}

	public PakEditor(Pak pak) {
		super(new BorderLayout());
		listeners = new HashSet<Listener>();
		resources = new NamedResource[0];
		model = new Model();
		table = new MixedTable(model);
		JPanel north = new JPanel(new FlowLayout());
		JButton addLevel = new JButton("Add level");
		addLevel.addActionListener((e) -> {
			if(pak.get(Level.FOURCC, "untitled") == null) {
				Level level = LevelEditor.createDefaultLevel(pak.getProject());
				pak.add("untitled", level);
				model.update();
			}
		});
		north.add(addLevel);
		JButton editLevel = new JButton("Edit level");
		editLevel.addActionListener((e) -> fireLevelChanged());
		north.add(editLevel);
		add(BorderLayout.NORTH, north);
		add(BorderLayout.CENTER, new JScrollPane(table));
		setPak(pak);
	}

	public void setPak(Pak pak) {
		this.pak = pak;
		model.update();
		fireLevelChanged();
	}

	public Pak getPak() {
		return pak;
	}

	public void addLevelSelectionListener(Listener l) {
		listeners.add(l);
	}

	private void fireLevelChanged() {
		for(Listener l : listeners)
			l.call();
	}

	private Level getDefaultLevel() {
		for(NamedResource r : resources)
			if(r.getResource().getType() == Level.FOURCC)
				return (Level) r.getResource();
		return null;
	}

	public Level getSelectedLevel() {
		int index = table.getSelectionModel().getMinSelectionIndex();
		if(index == -1)
			return getDefaultLevel();
		Resource r = resources[index].getResource();
		if(r instanceof Level)
			return (Level) r;
		return getDefaultLevel();
	}
}
