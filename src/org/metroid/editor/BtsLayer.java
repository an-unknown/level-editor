package org.metroid.editor;

import java.awt.Graphics;

import org.metroid.bts.TileBts;
import org.metroid.pak.level.Room;
import org.metroid.pak.level.Tile;

public class BtsLayer implements Layer {
	private Room room;

	public BtsLayer(Room room) {
		this.room = room;
	}

	public TileBts getBTS(int x, int y) {
		return room.getBTS(x, y);
	}

	public void setBTS(int x, int y, int bts) {
		room.setBTS(x, y, bts);
	}

	@Override
	public void resize(int width, int height) {
		room.resize(width, height);
	}

	@Override
	public void paint(Graphics g) {
		paint(g, 0, 0, getWidth(), getHeight());
	}

	@Override
	public void paint(Graphics g, int x, int y, int _w, int _h) {
		int w = room.getTileset().getTileWidth();
		int h = room.getTileset().getTileHeight();

		int width = getWidth();
		int height = getHeight();
		int ex = x + _w;
		int ey = y + _h;
		if(ex >= width)
			ex = width;
		if(ey >= height)
			ey = height;

		for(int py = y; py < ey; py++) {
			for(int px = x; px < ex; px++) {
				TileBts bts = room.getBTS(px, py);
				BtsImages.paint(g, px * w, py * h, bts);
			}
		}
	}

	@Override
	public int getWidth() {
		return room.getWidth();
	}

	@Override
	public int getHeight() {
		return room.getHeight();
	}

	@Override
	public void fill(TileSelection tiles) {
	}

	@Override
	public void put(int x, int y, TileSelection tiles) {
	}

	@Override
	public Tile get(int x, int y) {
		return null;
	}
}
