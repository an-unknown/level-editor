package org.metroid.editor;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.util.HashSet;
import java.util.Set;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import org.metroid.Listener;
import org.metroid.pak.level.Level;
import org.metroid.pak.level.Room;
import org.metroid.pak.level.RoomInfo;
import org.metroid.pak.level.Tile;
import org.metroid.pak.level.Tileset;
import org.metroid.project.Project;

public class LevelEditor extends JPanel {
	private static final long serialVersionUID = 1L;
	public static final int TILE_SIZE = 8;
	public static final double ROOM_SCALE = 0.25;

	private Level level;

	private JScrollBar scrollerX;
	private JScrollBar scrollerY;

	private GX gx;

	private BufferedImage cache;

	private Set<Listener> listeners;
	private RoomInfo selectedRoom;
	private Rectangle moveSelection;

	private boolean addNewRoom = false;
	private boolean deleteRoom = false;

	private JTextField width;
	private JTextField height;

	private JTextField spawnX;
	private JTextField spawnY;

	private JLabel statusBar;

	private int levelWidth = 512;
	private int levelHeight = 512;

	private int tileSize = TILE_SIZE;
	private double roomScale = ROOM_SCALE;

	public LevelEditor(Project project) {
		super(new BorderLayout());
		level = createDefaultLevel(project);

		gx = new GX();
		scrollerX = new JScrollBar(JScrollBar.HORIZONTAL);
		scrollerY = new JScrollBar(JScrollBar.VERTICAL);
		scrollerX.setValues(0, getScrollWidth(), 0, getLevelWidth());
		scrollerY.setValues(0, getScrollHeight(), 0, getLevelHeight());

		scrollerX.addAdjustmentListener((e) -> gx.repaint());
		scrollerY.addAdjustmentListener((e) -> gx.repaint());

		setLayout(new BorderLayout());

		JPanel north = new JPanel(new FlowLayout());
		JButton addRoom = new JButton("Add room");
		addRoom.addActionListener((e) -> addNewRoom = true);
		JButton rmRoom = new JButton("Delete room");
		rmRoom.addActionListener((e) -> deleteRoom = true);
		width = new JTextField(Integer.toString(getLevelWidth()), 6);
		height = new JTextField(Integer.toString(getLevelHeight()), 6);
		JButton resize = new JButton("resize");
		resize.addActionListener((e) -> resize());
		spawnX = new JTextField(Integer.toString(level.getSpawnX()), 6);
		spawnY = new JTextField(Integer.toString(level.getSpawnY()), 6);
		JButton setSpawn = new JButton("Set spawn");
		setSpawn.addActionListener((e) -> {
			int x = level.getSpawnX();
			int y = level.getSpawnY();
			try {
				x = Integer.parseInt(spawnX.getText().trim());
			} catch(NumberFormatException ex) {
			}
			try {
				y = Integer.parseInt(spawnY.getText().trim());
			} catch(NumberFormatException ex) {
			}
			setSpawn(x, y);
		});
		JComboBox<Integer> scale = new JComboBox<>();
		scale.addItem(4);
		scale.addItem(8);
		scale.addItem(16);
		scale.addItemListener((e) -> setScale(scale.getItemAt(scale.getSelectedIndex())));
		scale.setSelectedIndex(0);
		north.add(addRoom);
		north.add(rmRoom);
		north.add(width);
		north.add(height);
		north.add(resize);
		north.add(spawnX);
		north.add(spawnY);
		north.add(setSpawn);
		north.add(new JLabel("Scale:"));
		north.add(scale);
		JPanel center = new JPanel(new BorderLayout());
		center.add(BorderLayout.CENTER, gx);
		center.add(BorderLayout.SOUTH, scrollerX);
		center.add(BorderLayout.EAST, scrollerY);
		statusBar = new JLabel();
		updateStatusBar();
		add(BorderLayout.NORTH, north);
		add(BorderLayout.CENTER, center);
		add(BorderLayout.SOUTH, statusBar);

		MouseHandler h = new MouseHandler();
		gx.addMouseListener(h);
		gx.addMouseMotionListener(h);
		gx.addComponentListener(new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent e) {
				scrollerX.setVisibleAmount(getScrollWidth());
				scrollerY.setVisibleAmount(getScrollHeight());
				repaint();
			}
		});

		listeners = new HashSet<Listener>();
	}

	public LevelEditor(Level level) {
		this(level.getProject());
		setLevel(level);
	}

	private int getScrollWidth() {
		int v = gx.getWidth() / tileSize;
		int r = gx.getWidth() % tileSize;
		if(r == 0 || r == tileSize - 1)
			v++;
		if(v < 0)
			v = 0;
		return v;
	}

	private int getScrollHeight() {
		int v = gx.getHeight() / tileSize;
		int r = gx.getHeight() % tileSize;
		if(r == 0 || r == tileSize - 1)
			v++;
		if(v < 0)
			v = 0;
		return v;
	}

	public int getLevelWidth() {
		return levelWidth;
	}

	public int getLevelHeight() {
		return levelHeight;
	}

	private void resize() {
		int width = getLevelWidth();
		int height = getLevelHeight();
		try {
			width = Integer.parseInt(this.width.getText());
		} catch(NumberFormatException e) {
		}
		try {
			height = Integer.parseInt(this.height.getText());
		} catch(NumberFormatException e) {
		}
		if(width < 0)
			width = getLevelWidth();
		if(height < 0)
			height = getLevelWidth();
		Rectangle area = getLevelSize();
		if(width < area.width || height < area.height) {
			width = area.width;
			height = area.height;
		}
		this.levelWidth = width;
		this.levelHeight = height;
		this.width.setText(Integer.toString(getLevelWidth()));
		this.height.setText(Integer.toString(getLevelHeight()));
		scrollerX.setValues(0, getScrollWidth(), 0, getLevelWidth());
		scrollerY.setValues(0, getScrollHeight(), 0, getLevelHeight());
		cache = null;
		gx.repaint();
		updateStatusBar();
	}

	private void checkSize() {
		Rectangle area = getLevelSize();
		if(getLevelWidth() < area.width || getLevelHeight() < area.height) {
			this.levelWidth = area.width;
			this.levelHeight = area.height;
			this.width.setText(Integer.toString(getLevelWidth()));
			this.height.setText(Integer.toString(getLevelHeight()));
		}
		scrollerX.setValues(0, getScrollWidth(), 0, getLevelWidth());
		scrollerY.setValues(0, getScrollHeight(), 0, getLevelHeight());
		cache = null;
		gx.repaint();
	}

	public void addRoomChangedListener(Listener l) {
		listeners.add(l);
	}

	protected void fireRoomChanged() {
		for(Listener l : listeners)
			l.call();
	}

	public Level getLevel() {
		return level;
	}

	public void setLevel(Level level) {
		this.level = level;
		selectedRoom = level.getRoomInfo(0);
		checkSize();
		scrollerX.setValues(0, getScrollWidth(), 0, getLevelWidth());
		scrollerY.setValues(0, getScrollHeight(), 0, getLevelHeight());
		spawnX.setText(Integer.toString(level.getSpawnX()));
		spawnY.setText(Integer.toString(level.getSpawnY()));
		cache = null;
		repaint();
		fireRoomChanged();
		updateStatusBar();
	}

	public Room getRoom() {
		return selectedRoom.getRoom();
	}

	public Room getRoom(int id) {
		return level.getRoom(id);
	}

	public int rooms() {
		return level.rooms();
	}

	public void setScale(int div) {
		roomScale = 1.0 / div;
		tileSize = (int) Math.round(level.getProject().getTileWidth() * roomScale);
		refresh();
		gx.repaint();
	}

	private void setSpawn(int x, int y) {
		level.setSpawn(x, y);
		spawnX.setText(Integer.toString(x));
		spawnY.setText(Integer.toString(y));
		cache = null;
		gx.repaint();
	}

	public static Room createRoom(Project project) {
		return createRoom(project, RoomLayoutEditor.SCREEN_WIDTH, RoomLayoutEditor.SCREEN_HEIGHT);
	}

	public static Room createRoom(Project project, int width, int height) {
		Tileset tileset = createTileset(project);
		Room room = new Room(tileset, width, height);
		room.setPak(project.getPak());
		return room;
	}

	public static Tileset createTileset(Project project) {
		Tile emptyTile = project.getEmptyTile();
		Tileset tileset = new Tileset(project.getTileWidth(), project.getTileHeight(), emptyTile);
		return tileset;
	}

	public static Level createDefaultLevel(Project project) {
		Level level = new Level();
		level.setPak(project.getPak());
		level.addRoom(createRoom(project), 0, 0);
		return level;
	}

	private void updateStatusBar() {
		Rectangle area = level.getArea();
		statusBar.setText(String.format("Size: %dx%d", area.width, area.height));
	}

	private void prepare() {
		if(cache != null)
			return;
		cache = new BufferedImage(getLevelWidth() * tileSize, getLevelHeight() * tileSize,
				BufferedImage.TYPE_INT_ARGB);
		Graphics g = cache.getGraphics();
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, cache.getWidth(), cache.getHeight());

		// draw screen sizes
		g.setColor(Color.RED);
		int w = tileSize;
		int h = tileSize;
		for(int px = 0; px < cache.getWidth() / w; px += RoomLayoutEditor.SCREEN_WIDTH) {
			g.drawLine(px * w, 0, px * w, cache.getHeight());
			g.drawLine(px * w - 1, 0, px * w - 1, cache.getHeight());
		}
		for(int py = 0; py < cache.getHeight() / h; py += RoomLayoutEditor.SCREEN_HEIGHT) {
			g.drawLine(0, py * h, cache.getWidth(), py * h);
			g.drawLine(0, py * h - 1, cache.getWidth(), py * h - 1);
		}

		// draw rooms
		RoomInfo[] rooms = level.getRooms();
		for(RoomInfo room : rooms) {
			int x = room.getX() * tileSize;
			int y = room.getY() * tileSize;
			BufferedImage img = RoomRenderer.render(room.getRoom(), roomScale);
			g.drawImage(img, x, y, null);
			g.setColor(Color.GREEN);
			g.drawRect(x, y, img.getWidth() - 1, img.getHeight() - 1);
		}

		// draw spawn point
		g.setColor(Color.RED);
		int spawnX = level.getSpawnX() * tileSize;
		int spawnY = level.getSpawnY() * tileSize;
		g.drawLine(spawnX, spawnY, spawnX + tileSize, spawnY + tileSize);
		g.drawLine(spawnX, spawnY + tileSize, spawnX + tileSize, spawnY);

		// draw area
		Rectangle area = level.getArea();
		g.setColor(Color.LIGHT_GRAY);
		g.drawRect(area.x * tileSize, area.y * tileSize, area.width * tileSize, area.height * tileSize);

		g.dispose();
	}

	public void refresh() {
		cache = null;
	}

	public Rectangle getLevelSize() {
		int startX = level.getRoomX(0);
		int startY = level.getRoomY(0);
		int endX = startX + level.getRoom(0).getWidth();
		int endY = startY + level.getRoom(0).getHeight();
		for(RoomInfo room : level.getRooms()) {
			if(room.getX() < startX)
				startX = room.getX();
			if(room.getY() < startY)
				startY = room.getY();
			int ex = room.getX() + room.getWidth();
			int ey = room.getY() + room.getHeight();
			if(ex > endX)
				endX = ex;
			if(ey > endY)
				endY = ey;
		}
		return new Rectangle(startX, startY, endX - startX, endY - startY);
	}

	public RoomInfo getRoom(int x, int y) {
		RoomInfo result = null;
		for(RoomInfo room : level.getRooms()) {
			int x1 = room.getX();
			int y1 = room.getY();
			int x2 = room.getX() + room.getWidth();
			int y2 = room.getY() + room.getHeight();
			if(x1 <= x && x < x2 && y1 <= y && y < y2) {
				if(result == null)
					result = room;
				else if(result != null && (result.getWidth() > room.getWidth()
						|| result.getHeight() > room.getHeight()))
					result = room;
			}
		}
		return result;
	}

	private class GX extends JComponent {
		private static final long serialVersionUID = 1L;

		@Override
		public void update(Graphics g) {
			paint(g);
		}

		@Override
		public void paint(Graphics g) {
			super.paint(g);
			prepare();
			int ox = scrollerX.getValue() * tileSize;
			int oy = scrollerY.getValue() * tileSize;
			g.translate(-ox, -oy);
			g.drawImage(cache, 0, 0, null);
			if(selectedRoom != null) {
				g.setColor(Color.CYAN);
				int x = selectedRoom.getX() * tileSize;
				int y = selectedRoom.getY() * tileSize;
				int w = selectedRoom.getWidth() * tileSize;
				int h = selectedRoom.getHeight() * tileSize;
				g.drawRect(x, y, w - 1, h - 1);
			}
			if(moveSelection != null) {
				g.setColor(Color.CYAN);
				int x = moveSelection.x * tileSize;
				int y = moveSelection.y * tileSize;
				int w = moveSelection.width * tileSize;
				int h = moveSelection.height * tileSize;
				g.drawRect(x, y, w - 1, h - 1);
			}
		}
	}

	private class MouseHandler extends MouseAdapter {
		private Point start;

		@Override
		public void mouseMoved(MouseEvent e) {
		}

		@Override
		public void mouseDragged(MouseEvent e) {
			Point p = getPoint(e.getX(), e.getY());
			if(start == null)
				start = p;
			if(p == null)
				return;
			if(moveSelection != null) {
				int dx = start.x - selectedRoom.getX();
				int dy = start.y - selectedRoom.getY();
				moveSelection = new Rectangle(p.x - dx, p.y - dy, moveSelection.width,
						moveSelection.height);
				gx.repaint();
			}
		}

		@Override
		public void mousePressed(MouseEvent e) {
			start = getPoint(e.getX(), e.getY());
			if(start == null || start.x >= getLevelWidth() || start.y >= getLevelHeight())
				return;
			if(deleteRoom) {
				deleteRoom = false;
				RoomInfo room = getRoom(start.x, start.y);
				if(room != null) {
					level.removeRoom(room);
					cache = null;
					gx.repaint();
				}
			}
			if(addNewRoom) {
				Room room = createRoom(level.getProject());
				level.addRoom(room, start.x, start.y);
				addNewRoom = false;
				cache = null;
				gx.repaint();
			}
			selectedRoom = getRoom(start.x, start.y);
			if(selectedRoom != null)
				moveSelection = new Rectangle(selectedRoom.getX(), selectedRoom.getY(),
						selectedRoom.getWidth(), selectedRoom.getHeight());
			updateStatusBar();
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			if(moveSelection != null) {
				Point p = getPoint(e.getX(), e.getY());
				int dx = start.x - selectedRoom.getX();
				int dy = start.y - selectedRoom.getY();
				int px = p.x - dx;
				int py = p.y - dy;
				if(px != selectedRoom.getX() || py != selectedRoom.getY()) {
					selectedRoom.setX(px);
					selectedRoom.setY(py);
					cache = null;
					gx.repaint();
				}
			}
			if(selectedRoom != null)
				fireRoomChanged();
			moveSelection = null;
			gx.repaint();
			updateStatusBar();
		}

		@Override
		public void mouseClicked(MouseEvent e) {
			if(SwingUtilities.isRightMouseButton(e)) {
				Point p = getPoint(e.getX(), e.getY());
				setSpawn(p.x, p.y);
			}
		}

		@Override
		public void mouseEntered(MouseEvent e) {
		}

		@Override
		public void mouseExited(MouseEvent e) {
		}

		private Point getPoint(int x, int y) {
			int px = x / tileSize;
			int py = y / tileSize;
			if(px >= getLevelWidth() || py >= getLevelHeight() || px < 0 || py < 0) {
				return null;
			}
			x = px + scrollerX.getValue();
			y = py + scrollerY.getValue();
			return new Point(x, y);
		}
	}
}
