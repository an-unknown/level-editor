package org.metroid;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.metroid.pak.Pak;
import org.metroid.pak.xml.XmlFile;
import org.metroid.pak.xml.XmlPak;
import org.metroid.xml.dom.Element;

public class PakTool {
	public static void main(String[] args) throws IOException {
		if(args.length != 2) {
			System.out.printf("usage: %s in.xml out.pak\n", PakTool.class.getSimpleName());
			System.exit(1);
		}
		String infile = args[0];
		String outfile = args[1];
		Element root = XmlFile.read(new File(infile));
		Pak pak = XmlPak.read(root);
		pak.write(new FileOutputStream(outfile));
	}
}
