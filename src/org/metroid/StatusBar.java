package org.metroid;

import java.awt.BorderLayout;
import java.awt.Label;
import java.awt.Panel;

public class StatusBar extends Panel {
	private static final long serialVersionUID = -2537590207588596059L;

	private Label text;

	public StatusBar() {
		super(new BorderLayout());
		add(BorderLayout.CENTER, text = new Label());
	}

	public void setText(String text) {
		this.text.setText(text);
	}
}
