package org.metroid;

public interface ResizeListener {
	public void resize(int width, int height);
}
