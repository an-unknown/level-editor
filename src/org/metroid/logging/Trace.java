package org.metroid.logging;

import java.io.PrintStream;
import java.util.logging.Handler;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import java.util.logging.StreamHandler;

public class Trace {
	public static final boolean debug = false;

	static {
		setup();
	}

	private Trace() {
	}

	private static void setup() {
		Logger globalLogger = Logger.getLogger(""); // root logger
		if(debug)
			globalLogger.setLevel(Levels.DEBUG);
		Handler[] handlers = globalLogger.getHandlers();
		for(Handler handler : handlers) {
			globalLogger.removeHandler(handler);
		}
		PrintStream out = System.out;
		PrintStream err = System.err;
		System.setOut(new LoggingStream(Levels.STDOUT));
		System.setErr(new LoggingStream(Levels.STDERR, err));
		StreamHandler handler = new StreamHandler(out, new LogFormatter()) {
			@Override
			public synchronized void publish(LogRecord record) {
				super.publish(record);
				super.flush();
			}
		};
		if(debug)
			handler.setLevel(Levels.DEBUG);
		globalLogger.addHandler(handler);
	}

	public static Logger create(Class<?> tracedClass) {
		return Logger.getLogger(tracedClass.getSimpleName());
	}

	public static Logger create(String name) {
		return Logger.getLogger(name);
	}
}