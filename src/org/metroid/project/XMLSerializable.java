package org.metroid.project;

import java.io.IOException;

import org.metroid.xml.dom.Element;

public interface XMLSerializable {
	public void read(Element root) throws IOException;

	public Element write();
}
