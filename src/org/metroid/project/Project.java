package org.metroid.project;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Logger;

import org.metroid.logging.Trace;
import org.metroid.pak.Pak;
import org.metroid.pak.level.Tile;
import org.metroid.pak.txtr.Texture;
import org.metroid.xml.dom.Element;

public class Project implements XMLSerializable {
	private static final Logger log = Trace.create(Project.class);

	private String name;
	private Textures textures;
	private int tileWidth = 32;
	private int tileHeight = 32;
	private Pak pak;
	private String base;

	private Tile emptyTile = null;

	private static ThreadLocal<Project> currentProject = new ThreadLocal<>();

	public static Project setCurrentProject(Project prj) {
		Project old = currentProject.get();
		currentProject.set(prj);
		return old;
	}

	public static Project getCurrentProject() {
		return currentProject.get();
	}

	public static void process(Project prj, Runnable callback) {
		Project old = setCurrentProject(prj);
		try {
			callback.run();
		} finally {
			setCurrentProject(old);
		}
	}

	public Project(String base) {
		name = "unknown";
		textures = new Textures(base);
		pak = new Pak(this);
		this.base = base;
	}

	public Project(String base, Pak pak) {
		name = "unknown";
		textures = new Textures(base);
		this.pak = pak;
		this.base = base;
	}

	public Project(String base, Element root) throws IOException {
		this(base);
		read(root);
	}

	public Tile getEmptyTile() {
		return emptyTile;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BufferedImage loadTexture(String path) throws IOException {
		return textures.load(path);
	}

	public Texture getTexture(String name) {
		return textures.get(name);
	}

	public String[] getTextureNames() {
		return textures.list();
	}

	public Texture addTexture(String name, String path) throws IOException {
		return textures.add(name, path);
	}

	public Texture addTexture(Texture tex) {
		return textures.add(tex);
	}

	public Texture removeTexture(String name) {
		return textures.remove(name);
	}

	public int getTileWidth() {
		return tileWidth;
	}

	public int getTileHeight() {
		return tileHeight;
	}

	public Pak getPak() {
		return pak;
	}

	@Override
	public void read(Element root) throws IOException {
		if(!root.name.equals("project")) {
			throw new IOException("not a project");
		}
		name = root.getElementsByTagName("name")[0].value;
		textures = new Textures(base, root.getElementsByTagName("textures")[0]);
		Element xmlsize = root.getElementsByTagName("tile-size")[0];
		tileWidth = Integer.parseInt(xmlsize.getAttribute("width"));
		tileHeight = Integer.parseInt(xmlsize.getAttribute("height"));
		Element xmlempty = root.getElementsByTagName("empty-tile")[0];
		Texture emptytex = getTexture(xmlempty.getAttribute("tex"));
		short emptys = Short.parseShort(xmlempty.getAttribute("s"));
		short emptyt = Short.parseShort(xmlempty.getAttribute("t"));
		emptyTile = new Tile(emptys, emptyt, emptytex);
		BufferedImage img = emptyTile.getTexture().getTexture().getSubimage(emptyTile.s * tileWidth,
				emptyTile.t * tileHeight, tileWidth, tileHeight);
		emptyTile.setCache(img);
		pak = new Pak();
		pak.read(this, root.getElementsByTagName("pak")[0]);
	}

	@Override
	public Element write() {
		Element root = new Element("project");
		root.addChild(new Element("name", name));
		root.addChild(textures.write());
		Element xmlsize = new Element("tile-size");
		xmlsize.addAttribute("width", Integer.toString(tileWidth));
		xmlsize.addAttribute("height", Integer.toString(tileHeight));
		root.addChild(xmlsize);
		Element xmlempty = new Element("empty-tile");
		xmlempty.addAttribute("s", Short.toString(emptyTile.s));
		xmlempty.addAttribute("t", Short.toString(emptyTile.t));
		xmlempty.addAttribute("tex", emptyTile.getTexture().getName());
		root.addChild(xmlempty);
		root.addChild(pak.write());
		return root;
	}

	public void write(File file) throws IOException {
		log.info("Writing project to file " + file);
		XMLFile.write(file, write());
	}

	public static Project read(File file) throws IOException {
		String base = file.getParent();
		log.info("Loading file " + file + (base != null ? " (base: " + base + ")" : ""));
		return new Project(base, XMLFile.read(file));
	}

	@Override
	public String toString() {
		return "Project[" + name + "]";
	}
}
