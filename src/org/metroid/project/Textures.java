package org.metroid.project;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.imageio.ImageIO;

import org.metroid.logging.Levels;
import org.metroid.logging.Trace;
import org.metroid.pak.txtr.Texture;
import org.metroid.xml.dom.Element;

public class Textures implements XMLSerializable {
	private static final Logger log = Trace.create(Textures.class);

	private Map<String, Texture> textures;
	private String base;

	public Textures(String base) {
		textures = new HashMap<>();
		this.base = base;
	}

	public Textures(String base, Element root) throws IOException {
		this(base);
		read(root);
	}

	public BufferedImage load(String path) throws IOException {
		String f = base != null ? (base + File.separator + path) : path;
		File file = new File(f);
		log.info("Loading texture from " + path);
		if(!file.exists()) {
			try(InputStream in = getClass().getResourceAsStream("/" + path)) {
				if(in == null) {
					log.warning("File not found: '" + path + "'");
					throw new FileNotFoundException(path);
				}
				return ImageIO.read(in);
			}
		}
		return ImageIO.read(new File(f));
	}

	public Texture add(String name, String path) throws IOException {
		log.info("Adding texture " + name + " (" + path + ")");
		Texture tex = new Texture(load(path));
		tex.setName(name);
		tex.setFileName(path);
		add(tex);
		return tex;
	}

	public Texture add(Texture tex) {
		textures.put(tex.getName(), tex);
		return tex;
	}

	public Texture get(String name) {
		Texture tex = textures.get(name);
		if(tex == null)
			log.log(Levels.WARNING, "Texture '" + name + "' not found");
		return tex;
	}

	public Texture remove(String name) {
		return textures.remove(name);
	}

	public String[] list() {
		if(textures.size() == 0)
			return new String[0];
		return textures.keySet().stream().sorted().toArray(String[]::new);
	}

	@Override
	public void read(Element root) throws IOException {
		textures.clear();
		if(!root.name.equals("textures")) {
			throw new IOException("not a textures element");
		}
		for(Element texture : root.getElementsByTagName("texture")) {
			String name = texture.getAttribute("name");
			String path = texture.value;
			add(name, path);
		}
	}

	@Override
	public Element write() {
		Element root = new Element("textures");
		for(String name : list()) {
			Texture tex = get(name);
			Element e = new Element("texture", tex.getFileName());
			e.addAttribute("name", name);
			root.addChild(e);
		}
		return root;
	}

	@Override
	public String toString() {
		return "Textures[" + Arrays.stream(list()).collect(Collectors.joining(",")) + "]";
	}
}
