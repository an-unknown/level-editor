package org.metroid.project;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Logger;

import org.metroid.logging.Levels;
import org.metroid.logging.Trace;
import org.metroid.xml.dom.Element;
import org.metroid.xml.dom.XMLReader;

public class XMLFile {
	private static final Logger log = Trace.create(XMLFile.class);

	public static Element read(File in) throws IOException {
		if(!in.exists()) {
			try(InputStream fin = XMLFile.class.getResourceAsStream("/" + in.getName())) {
				if(fin == null) {
					log.log(Levels.WARNING, "File not found: " + in.getName());
					throw new FileNotFoundException(in.getName());
				}
				return XMLReader.read(fin);
			}
		}
		try(FileInputStream fin = new FileInputStream(in)) {
			return XMLReader.read(fin);
		}
	}

	public static void write(File out, Element root) throws IOException {
		try(FileOutputStream fout = new FileOutputStream(out)) {
			String xml = root.toString();
			fout.write(xml.getBytes("utf-8"));
		}
	}
}
