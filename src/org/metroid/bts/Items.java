package org.metroid.bts;

public class Items {
	public static final int CHARGE_BEAM = 0;
	public static final int PLASMA_BEAM = 1;
	public static final int SPAZER_BEAM = 2;
	public static final int ICE_BEAM = 3;
	public static final int WAVE_BEAM = 4;
	public static final int LONG_BEAM = 5;
	public static final int F_BEAM = 6;
	public static final int BOMB = 7;
	public static final int MORPH_BALL = 8;
	public static final int SPIDER_BALL = 9;
	public static final int HIGHJUMP_BOOTS = 10;
	public static final int SPRING_BALL = 11;
	public static final int POWER_GRIP = 12;
	public static final int SCREW_ATTACK = 13;
	public static final int SPACE_JUMP = 14;
	public static final int SPEED_BOOSTER = 15;
	public static final int VARIA_SUIT = 16;
	public static final int GRAVITY_SUIT = 17;
	public static final int UNKNOWN_SUIT = 18;
	public static final int XRAY_VISOR = 19;
	public static final int GRAPPLE_BEAM = 20;
	public static final int SEEKER_MISSILE = 21;

	public static final int ENERGY_TANK = 22;
	public static final int RESERVE_TANK = 23;
	public static final int MISSILE = 24;
	public static final int SUPER_MISSILE = 25;
	public static final int POWER_BOMB = 26;
}
