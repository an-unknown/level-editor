package org.metroid.bts;

public class TileType {
	public static final int AIR = 0;
	public static final int SLOPE = 1;
	public static final int SOLID = 2;
	public static final int TREADMILL = 3;
	public static final int DOOR = 4;
	public static final int ELEVATOR = 5;
	public static final int SPIKE = 6;
	public static final int CRUMBLE = 7;
	public static final int DESTRUCTIBLE = 8;
	public static final int ITEM = 9;
	public static final int EVENT = 10;
	public static final int MACHINE = 11;
	public static final int SCROLL_AREA = 12;
	public static final int HIDDEN_PASSAGE = 15;
}
