package org.metroid.bts;

import java.io.Serializable;

import org.metroid.pak.level.Tile;
import org.metroid.pak.level.Tileset;

/*
 * 1b flip_x
 * 1b flip_y
 * 2b environment (0 = normal, 1 = rain, 2 = ice, 3 = hot)
 * 4b major tile type
 * --
 * 16b tile type specific data (subtype, item id, ...)
 * 
 */

public class TileBts implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final int BIT_FLIP_X = 0x800000;
	public static final int BIT_FLIP_Y = 0x400000;

	private int data;
	private Tile tile;

	public TileBts() {
		data = 0;
		tile = null;
	}

	public TileBts(int data) {
		setData(data);
	}

	public TileBts(int data, Tileset tileset) {
		setData(data, tileset);
	}

	private boolean isSet(int bits) {
		return (data & bits) != 0;
	}

	private void setBit(int bit, boolean value) {
		if(value)
			data |= bit;
		else
			data &= ~bit;
	}

	public boolean isFlipX() {
		return isSet(BIT_FLIP_X);
	}

	public void setFlipX(boolean value) {
		setBit(BIT_FLIP_X, value);
	}

	public boolean isFlipY() {
		return isSet(BIT_FLIP_Y);
	}

	public void setFlipY(boolean value) {
		setBit(BIT_FLIP_Y, value);
	}

	public int getTileType() {
		return (data >> 16) & 0x0F;
	}

	public void setTileType(int type) {
		data = (data & 0xFFF0FFFF) | ((type & 0x0F) << 16);
	}

	public int getTileSubtype() {
		return data & 0xFFFF;
	}

	public void setTileSubtype(int type) {
		data = (data & 0xFFFF0000) | (type & 0xFFFF);
	}

	public Tile getTile() {
		return tile;
	}

	public void setTile(Tile tile) {
		this.tile = tile;
	}

	public boolean hasTile() {
		return tile != null;
	}

	public int getData() {
		if(tile != null)
			setTileSubtype(tile.getId());
		return data;
	}

	public void setData(int data) {
		this.data = data;
		this.tile = null;
	}

	public void setData(int data, Tileset tileset) {
		this.data = data;
		this.tile = null;
		if(getTileType() == TileType.HIDDEN_PASSAGE)
			this.tile = tileset.getTile(getTileSubtype());
	}

	@Override
	public String toString() {
		return String.format("BTS[0x%08X;type=%s]", data, getTileType());
	}
}
