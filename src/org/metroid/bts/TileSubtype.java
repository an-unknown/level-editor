package org.metroid.bts;

public class TileSubtype {
	// AIR
	public static final int AIR_NORMAL = 0x0000;
	public static final int AIR_FOOL_XRAY = 0x0001;
	public static final int AIR_SPIDERBALL_HORIZONTAL = 0x0002;
	public static final int AIR_SPIDERBALL_VERTICAL = 0x0003;
	public static final int AIR_SPIDERBALL_Q1 = 0x0004;
	public static final int AIR_SPIDERBALL_Q2 = 0x0005;
	public static final int AIR_SPIDERBALL_Q3 = 0x0006;
	public static final int AIR_SPIDERBALL_Q4 = 0x0007;

	// SLOPE
	// the slope type

	// SOLID
	public static final int SOLID_NORMAL = 0x0000;
	public static final int SOLID_GRAPPLE = 0x0001;
	public static final int SOLID_LADDER = 0x0002;
	public static final int SOLID_LADDER2 = 0x0003;
	public static final int SOLID_RAIL = 0x0004;

	// TREADMILL
	public static final int TREADMILL_CONVEYOR_LEFT = 0x0000;
	public static final int TREADMILL_CONVEYOR_RIGHT = 0x0001;
	public static final int TREADMILL_WATERFALL = 0x0002;
	public static final int TREADMILL_QUICKSAND = 0x0003;
	public static final int TREADMILL_SANDFALL = 0x0004;

	// DOOR
	public static final int DOOR_BLUE = 0x0000;
	public static final int DOOR_RED = 0x0001;
	public static final int DOOR_GREEN = 0x0002;
	public static final int DOOR_YELLOW = 0x0003;
	public static final int DOOR_WHITE = 0x0004;

	// these are flags and can be or'd
	public static final int DOOR_VERTICAL = 0x0000;
	public static final int DOOR_HORIZONTAL = 0x4000;
	public static final int DOOR_LOCKED = 0x8000;

	public static final int DOOR_MASK = 0x00FF;

	// ELEVATOR
	public static final int ELEVATOR_TRACK = 0;
	public static final int ELEVATOR_END = 1;

	// SPIKE
	public static final int SPIKE_NORMAL = 0;
	public static final int SPIKE_HALF_DAMAGE = 1;

	// CRUMBLE
	public static final int CRUMBLE_NORMAL = 0;
	public static final int CRUMBLE_FAST = 1;
	public static final int CRUMBLE_SLOW = 2;

	// flags
	public static final int CRUMBLE_GRAPPLE = 0x4000;
	public static final int CRUMBLE_NEIGHBORS = 0x8000;

	public static final int CRUMBLE_MASK = 0x00FF;

	// DESTRUCTIBLE
	public static final int DESTRUCTIBLE_BEAM = 0x0000;
	public static final int DESTRUCTIBLE_ICE = 0x0001;
	public static final int DESTRUCTIBLE_PLASMA = 0x0002;
	public static final int DESTRUCTIBLE_WAVE = 0x0003;
	public static final int DESTRUCTIBLE_SPAZER = 0x0004;
	public static final int DESTRUCTIBLE_MISSILE = 0x0005;
	public static final int DESTRUCTIBLE_SUPERMISSILE = 0x0006;
	public static final int DESTRUCTIBLE_SPEEDBOOST = 0x0007;
	public static final int DESTRUCTIBLE_SCREW = 0x0008;
	public static final int DESTRUCTIBLE_BOMB = 0x0009;
	public static final int DESTRUCTIBLE_POWERBOMB = 0x000A;

	// additional flags
	public static final int DESTRUCTIBLE_RESPAWN_NEVER = 0x0000;
	public static final int DESTRUCTIBLE_RESPAWN_SLOW = 0x0200;
	public static final int DESTRUCTIBLE_RESPAWN_NORMAL = 0x0400;
	public static final int DESTRUCTIBLE_RESPAWN_FAST = 0x0600;
	public static final int DESTRUCTIBLE_x1 = 0x0800;
	public static final int DESTRUCTIBLE_x2 = 0x1000;
	public static final int DESTRUCTIBLE_x4 = 0x1800;
	public static final int DESTRUCTIBLE_1x = 0x2000;
	public static final int DESTRUCTIBLE_2x = 0x4000;
	public static final int DESTRUCTIBLE_4x = 0x6000;
	public static final int DESTRUCTIBLE_NEIGHBORS = 0x8000;

	public static final int DESTRUCTIBLE_MASK = 0x01FF;

	// ITEM
	public static final int ITEM_TYPE_EQUIPMENT = 0x0000;
	public static final int ITEM_TYPE_MISSILE = 0x4000;
	public static final int ITEM_TYPE_SUPER_MISSILE = 0x8000;
	public static final int ITEM_TYPE_POWER_BOMB = 0xC000;

	public static final int ITEM_MASK = 0x3FFF;

	// EVENT
	// this contains the event id that will be fired

	// MACHINE
	public static final int MACHINE_SAVE_STATION = 0x0000;
	public static final int MACHINE_ENERGY_REGENERATOR = 0x0100;
	public static final int MACHINE_MISSILE_REGENERATOR = 0x0200;
	public static final int MACHINE_REGENERATOR = 0x0300;
	public static final int MACHINE_MAP_TERMINAL = 0x0400;
	public static final int MACHINE_NAV_TERMINAL = 0x0500;
	public static final int MACHINE_SWITCH = 0x0600;
	public static final int MACHINE_BOOSTBALL = 0x0700;
	public static final int MACHINE_ROBOT_CONTROL = 0x0800;
	public static final int MACHINE_TURBINE = 0x0900;
	public static final int MACHINE_BLAST_DOOR = 0x0A00;
	public static final int MACHINE_TUBE = 0x0B00;
	public static final int MACHINE_ENERGY_SINK = 0x0C00;
	public static final int MACHINE_ELECTROMAGNETIC_FIELD = 0x0D00;
	public static final int MACHINE_MAGNET = 0x0E00;
	public static final int MACHINE_GRAVITY_MANIPULATOR = 0x0F00;
	public static final int MACHINE_MORPHBALL_ACCELERATOR = 0x1000;
	public static final int MACHINE_TRAIN = 0x2000;

	// SCROLL_AREA
	// 4b water level
	// 4b tide
	// 4b scroll offset
	// 2b liquid: 0 = nothing, 1 = water, 2 = magma, 3 = acid
	// 1b stick to bottom
	// 1b border
	public static final int SCROLL_AREA_LIQUID_NOTHING = 0x0000;
	public static final int SCROLL_AREA_LIQUID_WATER = 0x1000;
	public static final int SCROLL_AREA_LIQUID_MAGMA = 0x2000;
	public static final int SCROLL_AREA_LIQUID_ACID = 0x3000;
	public static final int SCROLL_AREA_STICK_TO_BOTTOM = 0x4000;
	public static final int SCROLL_AREA_BORDER = 0x8000;

	// HIDDEN_PASSAGE
	// the alternative tile to display

	private static boolean getBit(int data, int bits) {
		return (data & bits) != 0;
	}

	private static int setBit(int data, int bit, boolean value) {
		if(value)
			data |= bit;
		else
			data &= ~bit;
		return data;
	}

	public static boolean getScrollBorder(int data) {
		return getBit(data, SCROLL_AREA_BORDER);
	}

	public static int setScrollBorder(int data, boolean value) {
		return setBit(data, SCROLL_AREA_BORDER, value);
	}
}
