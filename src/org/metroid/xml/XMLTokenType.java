package org.metroid.xml;

public enum XMLTokenType {
	topen, tclose, tsopen, tcloses, copen, cclose, cdataopen, cdataclose, equal,
	quote, squote, string, piopen, piclose, ent, text, space, error;
}
