package org.metroid;

// @formatter:off
public class Endianess {
	public final static int get16bit_BE(byte buffer[]) {
		return(((buffer[0] & 0xFF)<< 8) | (buffer[1] & 0xFF));
	}

	public final static int get16bit_BE(byte buffer[], int offset) {
		return(((buffer[offset +0] & 0xFF)<< 8) | (buffer[offset +1] & 0xFF));
	}

	public final static int get32bit_BE(byte buffer[]) {
		return(
			((buffer[0] & 0xFF) << 24) |
			((buffer[1] & 0xFF) << 16) |
			((buffer[2] & 0xFF) << 8) |
			 (buffer[3] & 0xFF));
	}

	public final static int get32bit_BE(byte buffer[], int offset) {
		return(
			((buffer[offset +0] & 0xFF) << 24) |
			((buffer[offset +1] & 0xFF) << 16) |
			((buffer[offset +2] & 0xFF) << 8) |
			 (buffer[offset +3] & 0xFF));
	}

	public final static long get64bit_BE(byte buffer[]) {
		return(
			((buffer[0] & 0xFF) << 56) |
			((buffer[1] & 0xFF) << 48) |
			((buffer[2] & 0xFF) << 40) |
			((buffer[3] & 0xFF) << 32) |
			((buffer[4] & 0xFF) << 24) |
			((buffer[5] & 0xFF) << 16) |
			((buffer[6] & 0xFF) <<  8) |
			 (buffer[7] & 0xFF));
	}

	public final static int get16bit_LE(byte buffer[]) {
		return((buffer[0] & 0xFF) | ((buffer[1] & 0xFF) << 8));
	}

	public final static int get32bit_LE(byte buffer[]) {
		return(
			 (buffer[0] & 0xFF) |
			((buffer[1] & 0xFF) << 8) |
			((buffer[2] & 0xFF) << 16) |
			((buffer[3] & 0xFF) << 24));
	}

	public final static long get64bit_LE(byte buffer[]) {
		return(
			 (buffer[0] & 0xFF) |
			((buffer[1] & 0xFF) <<  8) |
			((buffer[2] & 0xFF) << 16) |
			((buffer[3] & 0xFF) << 24) |
			((buffer[4] & 0xFF) << 32) |
			((buffer[5] & 0xFF) << 40) |
			((buffer[6] & 0xFF) << 48) |
			((buffer[7] & 0xFF) << 56)
		);
	}

	public final static byte[] set16bit_BE(int num, byte buffer[]) {
		buffer[0] = (byte) ((num >> 8) & 0xFF);
		buffer[1] = (byte) (num & 0xFF);
		return buffer;
	}

	public final static byte[] set32bit_BE(int num, byte buffer[]) {
		buffer[0] = (byte) ((num >> 24) & 0xFF);
		buffer[1] = (byte) ((num >> 16) & 0xFF);
		buffer[2] = (byte) ((num >>  8) & 0xFF);
		buffer[3] = (byte) (num & 0xFF);
		return buffer;
	}

	public final static byte[] set16bit_BE(int num, byte buffer[], int offset) {
		buffer[offset + 0] = (byte) ((num >> 8) & 0xFF);
		buffer[offset + 1] = (byte) (num & 0xFF);
		return buffer;
	}

	public final static byte[] set32bit_BE(int num, byte buffer[], int offset) {
		buffer[offset + 0] = (byte) ((num >> 24) & 0xFF);
		buffer[offset + 1] = (byte) ((num >> 16) & 0xFF);
		buffer[offset + 2] = (byte) ((num >>  8) & 0xFF);
		buffer[offset + 3] = (byte) (num & 0xFF);
		return buffer;
	}

	public final static byte[] set32bit_BE(long num, byte buffer[], int offset) {
		buffer[offset + 0] = (byte) ((num >> 24) & 0xFF);
		buffer[offset + 1] = (byte) ((num >> 16) & 0xFF);
		buffer[offset + 2] = (byte) ((num >>  8) & 0xFF);
		buffer[offset + 3] = (byte) (num & 0xFF);
		return buffer;
	}

	public final static byte[] set32bit_BE(float f, byte buffer[], int offset) {
		int num = Float.floatToRawIntBits(f);
		set32bit_BE(num, buffer, offset);
		return buffer;
	}
}
