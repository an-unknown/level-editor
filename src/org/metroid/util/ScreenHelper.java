package org.metroid.util;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Toolkit;

public class ScreenHelper {
	public static Dimension getScreenSize() {
		return Toolkit.getDefaultToolkit().getScreenSize();
	}

	public static Point getWindowPosition(Dimension size) {
		Dimension screen = getScreenSize();
		int x = (screen.width - size.width) / 2;
		int y = (screen.height - size.height) / 2;
		return new Point(x, y);
	}
}
